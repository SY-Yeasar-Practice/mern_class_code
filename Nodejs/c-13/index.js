//all npm require
var getExpress = require("express");//get the express 
var app = getExpress(); //call the express here
var mongoose = require("mongoose") //get the mongoose
require("dotenv").config()//get the dot env

//dot env file
var port = process.env.PORT || 8080
var url = process.env.MongooseUrl
//creat a server
app.listen(port, () => console.log(`Server is running on ${port}`))

//connect database to mongoose
mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})
.then(()=>{
    console.log("Server is connected to the database....");
})
.catch(err => console.log(err))

//root path
app.get("/",(req,res)=>{
    res.send("<h1>I am from root</h1>")
})

//default path
app.get("*",(req,res)=>{
    res.send("<h1>404 Page not found</h1>")
})
