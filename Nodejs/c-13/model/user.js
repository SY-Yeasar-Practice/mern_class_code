var mongoose = require("mongoose") //get the mongoose

var Schema = mongoose.Schema
var userInfoSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true,
        trim: true
    },
    password:{
        type: String,
        required: true,
        minLength: 6
    }
})
var userModel =  mongoose.model("User", userInfoSchema)
module.exports = userModel;