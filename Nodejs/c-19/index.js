//require file
const getExpress = require("express");
const app = getExpress();
const mongoose = require("mongoose")
require("dotenv").config()
const bodyParser = require("body-parser");

const uploadRoute = require("./src/route/uploadRoute")




//work with dot env
var port = process.env.PORT || 8080;
var mongoURL = process.env.URL

//body parser
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//connect to the database
mongoose.connect(mongoURL, {useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true})
.then(() => {
    console.log("Server is connected to the database");
})
.catch(err => {
    console.log(err);
})

//creat the server
app.listen(port, () => {console.log(`Server is running on ${port}`)})

//root Path
app.get("/", (req, res) => {
    res.send("<h1>I am from Root</h1>")
})

//other route
app.use("/upload", uploadRoute)

//default root
app.get("*", (req,res) => {
    res.status(404).send("<h1>404 Page not found</h1>")
})