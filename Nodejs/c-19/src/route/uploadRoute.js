const getExpress = require("express");
const route = getExpress.Router();
const {uploadController, downloadController} = require("../controller/uploadCont")
const uploadMiddleware = require("../../middleware/uploadMid")

route.post("/single/image", uploadMiddleware.single('img') ,  uploadController)//upload single file or image
route.post("/multiple/image", uploadMiddleware.array('img') ,  uploadController)//upload multuple file or image
route.get("/download/image",   downloadController)//upload multuple file or image

module.exports = route