//require file
const Joi = require("joi");

const addressValidate = Joi.object({
    division: Joi.string().required(),
    district: Joi.string().required(),
    subDistrict: Joi.string().required(),
    zip: Joi.string().required()
})

const userValidation = Joi.object({
    userName: Joi.string().required().alphanum().max(10).min(3),
    password: Joi.string().required().pattern(new RegExp ('^[a-zA-Z0-9]{8,30}$')),
    email: Joi.string().required().regex(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/),
    userType:Joi.string().required(),
    address: addressValidate,
    isDeleted: Joi.boolean(),
    recoveryToken:Joi.string()
})


const passwordValidation = Joi.object({
     newPassword: Joi.string().required().pattern(new RegExp ('^[a-zA-Z0-9]{8,30}$')),
     token: Joi.string()
})

//export part
module.exports = {
    userValidation,
    passwordValidation
}
