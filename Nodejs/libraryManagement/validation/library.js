const Joi = require("joi")

const availableShopObj = Joi.object({
    district: Joi.string().required(),
    shopName: Joi.string().required()
})

const libraryValidator = Joi.object({
    bookName: Joi.string().required(),
    author: Joi.string().required(),
    releaseDate: Joi.date().required(),
    bookImage:Joi.string(),
    bookFile: Joi.string(),
    keyWords: Joi.array().items(Joi.string()),
    availableShop: Joi.array().items(availableShopObj),
    isActivated: Joi.boolean(),
    isDeleted: Joi.boolean()
})


module.exports = libraryValidator;