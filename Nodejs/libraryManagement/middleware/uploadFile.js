const multer = require("multer");

const fileFilter = (req, file, cb) => {
    let exAsArray = file.mimetype.split("/") ;
    let extention = exAsArray[exAsArray.length - 1]
    if(extention == "jpg" || extention == "jpeg"){
        cb(null, true)
    }else{
        cb(new Error("only jpg and jpeg files are allowed"))
    }//
}


const storage = multer.diskStorage({
    destination: (req,file, cb) => {
        cb(null, "./images")
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname)
    }
})

const upload = multer({
    storage,
    // fileFilter //only jpg and jpeg files are allowed
})

//export part
module.exports = {
    upload
}