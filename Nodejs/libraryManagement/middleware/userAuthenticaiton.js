const jwtDecode = require("jwt-decode");

const userAuth = async(req, res, next) => {
    try{
        const {id} = req.params;//get the query id from params of body
        const token = req.header("Authorization")
        const {id:tokeId} = jwtDecode(token)
        if(tokeId == id){
            next()
        }else{
            next("User is not authentic")
        }
    }
    catch(error){
        res.json({
            error
        })
    }
}

module.exports = userAuth