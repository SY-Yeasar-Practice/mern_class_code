const jwt = require("jsonwebtoken");
const user = require("../src/model/user");

const auth = (req, res, next) => {
    const token = req.header("Authorization");//get the jwt input from body header or browser local storage
    if(!token){
        res.status(401).json({
            message: "Authorization Error"
        })
    }
    const securityCode = process.env.TOKEN_SECURE_CODE //get the jwt security code
    const valid = jwt.verify(token, securityCode)//if the jwt is valid it will give a boolean value true
    if(valid){
        req.user = valid
        next()
    }else{
        res.status(401).json({
            message: "Authentication Error"
        })
    }
}
module.exports = {
    auth
}


