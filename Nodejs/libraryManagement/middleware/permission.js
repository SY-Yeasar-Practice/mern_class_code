const json_decode = require("jwt-decode");

const permission = (data) => {
        return (req, res, next) => {
            const token  = req.header("Authorization") //get the token from body input
            const decode = json_decode(token) //decode the token data
            const userType = decode.userType //get the user type
            const queryData = data;//get the data from a route
            const isFind = queryData.find(val => {
                return val == userType
            })//is it available or not just check it 

            if(isFind){
                next()
            }else{
                next("Restricted")
            }
            
    }
}



module.exports = {
    permission
}