//require file
const getExpress = require("express");
const app = getExpress();
const mongoose = require("mongoose")
require("dotenv").config()
const bodyParser = require("body-parser");
const userRoute = require("./src/route/user")
const uploadFileRoute = require("./src/route/uploadFile")
const creatNewBookRoute = require("./src/route/library");
const { rawListeners } = require("./src/model/user");
const { $_super } = require("./validation/library");
const downloadRoute = require("./src/route/download")

//work with bodyParser
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//work with dot env file
const port = process.env.PORT || 8080;
const mongoURL = process.env.URL;

//creat the server
app.listen(port, () => {console.log(`Server is running on ${port}`)})

//connect to the database
mongoose.connect(mongoURL, {useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true})
.then(() => {
    console.log("Server is connected to the database");
})
.catch(err => {
    console.log(err);
})

//root Path
app.get("/", (req, res) => {
    res.send("<h1>I am from Root</h1>")
})

//other route
app.use("/user", userRoute)
app.use("/image", uploadFileRoute)
app.use("/library", creatNewBookRoute)
app.use(downloadRoute)


//default root
app.get("*",(req, res) => {
    res.send("<h1>404 Page not found</h1>")
})


