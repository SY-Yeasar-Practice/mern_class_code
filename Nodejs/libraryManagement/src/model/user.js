const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    userName: String,
    password: String,
    address:{
        division:String,
        district: String,
        subDistrict: String,
        zip: String
    },
    userType:{
        type: String,
        enum : ["seachtudent", "teacher", "librarian"],
        default: "student"
    },
    email: {
        type: String,
        unique:true
    },
    recoveryToken:{
        type : String,
        default: ""
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("user", userSchema)