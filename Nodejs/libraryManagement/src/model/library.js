const mongoose = require("mongoose");


const librarySchema = new mongoose.Schema({
    bookName: String,
    author: String,
    releaseDate: {
        type: Date,
        default: Date.now
    },
    bookImage: String,
    bookFile: String,
    keyWords: [String],
    availableShop: [
        {
            district: String,
            shopName: String
        }
    ],
    isActivated: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("book", librarySchema)