const mongoose = require("mongoose")
const Schema = mongoose.Schema

const muultipleFileUploadSchema = new Schema({
    className: String,
    description: String,
    syllabus: [
        {
            subject: String,
            syllabusFile: {
                type: String,
                default : ""
            },
            title: String
        }
    ]
})

module.exports = mongoose.model("syllabus", muultipleFileUploadSchema)