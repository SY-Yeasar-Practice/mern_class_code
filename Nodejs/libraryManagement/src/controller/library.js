const Book = require("../model/library")
const mongoose = require("mongoose")
const libraryValidator = require("../../validation/library")
const bcrypt = require("bcrypt")

const creatNewBookController = async (req, res) => {
    try{
        const {error} = libraryValidator.validate(req.body)

        if(error){
            console.log(error);
            res.json({
                message: "library Validation error",
                error
            })
        }
        

        const newBook = new Book({
            ...req.body,
            bookFile: req.files.bookFile[0].filename,
            bookImage: req.files.bookImage[0].filename,
        })
        const saveNewBook = await newBook.save()
        if(saveNewBook){
            res.json({
                message:"new book successfully saved",
                saveNewBook
            })
        }
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

const findBookNameWithReleaseDate = async (req, res) => {
    try{
        const queryBook = await Book.find().select("bookName author ").sort({releaseDate:-1})
        if(queryBook){
            res.json({
                queryBook
            })
        }
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

module.exports = {
    creatNewBookController,
    findBookNameWithReleaseDate
}

