const mongoose = require("mongoose");
const User = require("../model/user");
const bcrypt = require("bcrypt")//hash the password
const { userValidation, passwordValidation} = require("../../validation/user") //get the validity of user
const jwt = require("jsonwebtoken");
// const { required } = require("joi");
require("dotenv").config();
const Joi = require("joi");
const { report } = require("../route/user");
const secureCode = process.env.TOKEN_SECURE_CODE //get the token security code from .env
const nodemailer = require("nodemailer");
const { updateOne } = require("../model/user");


//registrationController
const registrationController = async (req, res) => {
    try {
        const {error, value} = userValidation.validate(req.body)
        if(error){
            res.status(400).json({
                message: "validation error",
                error
            })
        }else{
            const { userName, password, userType, email , address, recoveryToken } = req.body //get the input from body
            const hashed = await bcrypt.hash(password, 10)
            const user = new User({
                userName,
                password: hashed,
                userType,
                email,
                address,
                recoveryToken
            })//creat a new user
            const saveData = await user.save();//save that data as a promise
            if (saveData) {
                res.status(201).json({
                    message: "user created succefully",
                    user: saveData
                })
            }
        }
    }
    catch (err) {
        res.json({
            error: err
        })
    }
}

//login controller
const loginController = async (req, res) => {
    let {userName, password} = req.body;//get the data input from body
    let isValidUser = await User.findOne({userName}) // get the valid user or not as a object
    
    //if the user has been found the if state will work otherwise else will work
    if(isValidUser){
        let user = isValidUser //get the user
        let tokenData = {
            userName: user.userName,
            userType: user.userType,
            id: user._id
        }
        let secureCode = process.env.TOKEN_SECURE_CODE //get the token security code from .env
        let token = jwt.sign(tokenData, secureCode, {expiresIn:"10d"})
        let isValidPassword = await bcrypt.compare(password, user.password)
        //if the password is matched then if will work otherwise else will work
        if(isValidPassword){
            res.json({
                message: "Login successfully",
                token
            })//successfully login!!
        }else{
            res.status(406).json({
                message: "Password has been incorrect",
            })
        }//password don't match
    }else{
        res.status(406).json({
            message:"User not found"
        })
    }//user not found
}

//update user controller
const updateUserController = async (req, res) => {
    try{
            const id = req.params.id;
            const {address} = req.body;
            const {division, district, subDistrict, zip} = address
            const user = await User.findByIdAndUpdate (
                {_id: id},
                {
                    $set:req.body
                },
                {multi: true}
            )
            return res.json({
                message: `${user.userName} has been updated`
            })
    }catch(err) {
        res.json({
            err
        })
    }
}

// //password change controller
// const passwordChangeController = async (req,res) => {
//     try{
//         const passwordValidator = Joi.object({
//             password: Joi.string().required().pattern(new RegExp ('^[a-zA-Z0-9]{8,30}$'))
//         })//validator the password
//         const {error} = passwordValidator.validate(req.body)
//         if(error){
//             res.json({
//                 message: "validation error",
//                 error
//             })
//         }else{
//             const {id} = req.params;
//             const {password} = req.body;
//             const hash = await bcrypt.hash(password, 10)
//             await User.findByIdAndUpdate(
//                 {_id : id},
//                 {
//                     $set: {
//                         password: hash
//                     }
//                 }
//             )
//             return res.json({
//                 message: "password has been changed"
//             })
//         }
//     }
//     catch(err){
//         res.json({
//             err
//         })
//     }
// }


//password change controller with old password , new password and retype password
const passwordChangeController = async (req,res) => {
    try{
        /* 
            in body you have to take 3 input like
            {
                oldPassword: .......(min-8),
                newPassword: ........(min-8),
                repeatPassword: ..........

            }
        */
        const passwordValidator = Joi.object({
            newPassword: Joi.string().required().pattern(new RegExp ('^[a-zA-Z0-9]{8,30}$')),
            repeatPassword: Joi.ref('newPassword'),
            oldPassword: Joi.required()
        })//validator the password

        const {error} = passwordValidator.validate(req.body) //get the validation
        if(error){
            res.json({
                message: "validation error",
                error
            })
        }else{

            const {id} = req.params;//get the id from params
            const {oldPassword, newPassword} = req.body; //get the data from body
            const user = await User.findOne(({_id:id})) //get the user here
            const matchTheOldPassword = await bcrypt.compare(oldPassword, user.password) //match the old password with the existing one 
            if(matchTheOldPassword){
                const matchTheNewPasswordWithOldOne = await bcrypt.compare(newPassword, user.password)//check that the new input password is equal to the old one or not
                if(matchTheNewPasswordWithOldOne){

                    res.json({
                        message: "You have input your old password.Please input a different password"
                    })

                }else {

                    const hash = await bcrypt.hash(newPassword, 10) //hash the new input password
                    await User.findByIdAndUpdate(
                        {_id : id},
                        {
                            $set: {
                                password: hash
                            }
                        }
                    )//update the new password
                    return res.json({
                        message: "password has been changed"
                    })
                }
            }else {

                res.json({
                    message: "old password doesn't match"
                })
            }
        }
    }
    catch(err){
        res.send(err)
    }
}


//delete individual user
const deleteUserController = async (req, res) => {
    try{

        const {id} = req.params;
        await User.findByIdAndUpdate(
            {_id: id},
            {
                $set: {
                    isDeleted: true
                }
            }
        )
        return res.json({
            message: "user delete temporary"
        })
    }
    catch(err){
        res.json({
            err
        })
    }
} 

//delete all temporary delete user delete permanently controller
const deleteUserPermanentlyController = async (req, res) => {
    try{
        const user  = await User.find({isDeleted:true})
        user.map(async val => {
            await User.findOneAndDelete(
                {isDeleted: true}
            )
            return res.json({
                message: `all data has been deleted permanently`
            })
        })
        
        
    }
    catch(err){
        err
    }
}

//get all active user
const allActiveUserController = async (req, res) => {
    try{
        const activeUser = await User.find(
            {isDeleted: false}
        )
        if(activeUser){
            res.send(activeUser)
        }else{
            res.send("<h1>no users havebeen found</h1>")
        }
    }
    catch(err){
        res.json({
            err
        })
    }
}

//pagination controller
const paginationController = async (req, res) => {
    try{
        const numberOfItemNeedToShow = 5; //number of item show in the page
        const {page} = req.params //get data from params or url
        const skipData = ((page - 1) * numberOfItemNeedToShow); //get how many items it need to skip
        
        const myUser = await User.find({isDeleted:false}).limit(numberOfItemNeedToShow).skip(skipData)
       
        if(myUser){
            res.json({
                myUser
            })
        }
    }
    catch(error){
        console.log(error);
        res.json({
            error
        })
    }
}

//forgot password
const forgotPasswordController = async (req, res) => {
    try{
        const {email} = req.body;//get the email for detect the user
        const user = await User.findOne(
            {
                email
            }
        )
        if(!user){
            res.status(404).json({
                message: " user not found with this email. "
            })
        }
        const token = jwt.sign(
            {
                _id : user._id
            },
            secureCode,
            {expiresIn:"15m"}
        )//get the token from where we can get these user

        await User.updateOne({recoveryToken: token})
        const emailOfEnv = process.env.EMAIL
        const password = process.env.PASSWORD
        //work of nodemailer start from here
        
        //====step one creat the transporter=====
        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth:{
                user: emailOfEnv,
                pass: password
            }
        })
      
        let mailOption = {
            from: "sadmanishopnil@gmail.com",
            to: user.email,
            subject: "password reset verification", // Subject line
            text: token // send these token
            
        }

        //===== step three send the mail
        await transporter.sendMail(mailOption, (err, data) => {
            if(err){
                res.status(400).json({
                    message: "email send failed"
                })
                console.log(err);
            }
            else{
                res.status(200).json({
                    message: "email has been send successfully"
                })
            }
        })
        
    }
    catch(error){
        console.log(error);
        res.json({
            error
        })
    }
}

//reset password
const resetPasswordController = async (req, res) => {
    try{
        const {token, newPassword} = req.body //take the token and newPassword from the body part
        const {error, value} = passwordValidation.validate(req.body) //did the validation with joi 

        if(error){
            res.json({
                message: "Password validation problem",
                error
            })
        } //new password validation error

        const isVerifiedToken = jwt.verify(token, secureCode) //verify the token

        if(isVerifiedToken){
            const user = await User.findOne({recoveryToken: token})//get the user from data base
            const hashedNewPassword = await bcrypt.hash(newPassword, 10) //hashed the new password
            const isOldPassword = await bcrypt.compare(newPassword, user.password)
            if(isOldPassword){
                res.status(400).json({
                    message: "you have type your old password.Please type a new password"
                })
            }
            const updateNewPassword = await User.updateOne(
                {
                    password: hashedNewPassword,
                    recoveryToken: ""
                }
            ) //update new password into the database
            
            if(updateNewPassword){
                res.json({
                    message: "new password updated succesfully"
                })
            }else{
                res.json({
                    message: "update failed"
                })
            }
        
        }else{
            res.status(203).json({
                message: "token is not authentic or expired"
            })
        }
    }
    catch(error){
        console.log(error);
        res.json({
            error
        })
    }
}

module.exports = {
    registrationController,
    loginController,
    updateUserController,
    deleteUserController,
    passwordChangeController,
    deleteUserPermanentlyController,
    allActiveUserController,
    paginationController,
    forgotPasswordController,
    resetPasswordController
}
