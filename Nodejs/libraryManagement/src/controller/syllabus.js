const Syllabus = require("../model/syllabus")

const syllabusController = async (req, res) => {
    try{
        const {className, subject} = req.params 
        const {filename} = req.file
        const updateData  = await Syllabus.updateOne(
            {
                className: className,
                syllabus:{
                    $elemMatch: {
                        subject: subject,
                    }
                }
            }, //querry
            {
                $set: {
                    "syllabus.$.syllabusFile": filename
                }
            }, //update
            {} //option
        )
        if(updateData){
            res.json({
                message: "syllabus file upload successfully"
            })
        }else{
            res.json({
                message: "syllabus file upload failed"
            })
        }
    }
    catch(err){
        console.log(err);
        res.status(406).json({
            err
        })
    }
}//upload multiple image in a time

module.exports = {
    syllabusController
}