const getExpress = require("express");
const {auth} = require("../../middleware/authentication")
const {permission} = require("../../middleware/permission")
const userAuth = require("../../middleware/userAuthenticaiton")


const {registrationController, 
        loginController,
        updateUserController,
        deleteUserController,
        passwordChangeController,
        deleteUserPermanentlyController,
        allActiveUserController,
        paginationController,
        forgotPasswordController,
        resetPasswordController } = require("../controller/user");


const route = getExpress.Router();

route.post("/registration",registrationController)
route.post("/login",loginController)
route.post("/resetPassword", resetPasswordController)
route.post("/forgotPassword", forgotPasswordController )
route.put("/update/:id", updateUserController)
route.put("/delete/:id", deleteUserController)
route.put("/password/change/:id", auth, userAuth,  passwordChangeController)
route.delete("/delete/permanently",auth, deleteUserPermanentlyController)
route.get("/new", auth, permission(["student"]),allActiveUserController ) //use the permission part middleware
route.get("/pagination/pageNo/:page", paginationController ) //pagination route



//export part
module.exports = route;