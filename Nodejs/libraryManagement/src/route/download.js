const getExpress = require("express");
const {downloadController} = require("../controller/download")

const route = getExpress.Router();

route.get("/download", downloadController)

// export part
module.exports = route