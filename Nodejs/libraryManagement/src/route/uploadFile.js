const getExpress = require("express");
const {upload:imgUploadMiddlware} = require("../../middleware/uploadFile")
const {uploadControllerSingleImage, uploadControllerMultipleImage} = require("../controller/uploadFile")
const {syllabusController} = require("../controller/syllabus")

const route = getExpress.Router();

route.post("/upload/single", imgUploadMiddlware.single("img"), uploadControllerSingleImage )
route.post("/upload/multiple", imgUploadMiddlware.array("img"), uploadControllerMultipleImage )
route.post("/upload/syllabus/:className/:subject", imgUploadMiddlware.single("img"), syllabusController )

module.exports = route;
