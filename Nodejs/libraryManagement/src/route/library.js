const getExpress = require("express");
const {creatNewBookController, findBookNameWithReleaseDate} = require("../controller/library")
const {upload} = require("../../middleware/uploadFile")

const route = getExpress.Router();

route.post("/creat/newBook", upload.fields(
   [
        {
        name: "bookImage",
        maxCount: 1
    },
    {
        name: "bookFile",
        maxCount: 1
    }
   ]
), creatNewBookController)

route.get("/book", findBookNameWithReleaseDate)

module.exports = route;