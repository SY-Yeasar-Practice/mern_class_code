// var http = require('http')
// var myOwnModule = require('./module')

// http.createServer((request , response)=>{
//     response.writeHead(200 , {"Content-type":"text/html"})
//     response.write(`The sum is : ${myOwnModule.sum(5)}
//     The subtration is ${myOwnModule.subs(10)}
//     The multiplication is ${myOwnModule.muliplication(5)}
//     The division is ${+(myOwnModule.div(10).toFixed(2))}`)
//     response.end()
// }).listen(8080)
// console.log('server is running......');

// var http = require('http')
// var url = require('url')
// http.createServer(function(req , res){
//     res.writeHead(200 , {"Content-type":"text/html"})
//     var searchUrl = url.parse(req.url , true).query
//     var outPut = `total page = ${searchUrl.page} and totalContent = ${searchUrl.totalContent}`
//     console.log(outPut);
//     res.write(outPut)
//     res.end
    
// }).listen(8080)
// console.log('Server is running');

// //*import part
// var httpModule = require('http')
// var url = require('url')
// var myOwnModule = require("./module")//my own import module
// httpModule.createServer((request , response)=>{
//     response.writeHead(200 , {"Content-type": "text/html"})
//     var mySearchItem = url.parse(request.url , true).query
//     console.log(mySearchItem);
//     response.write("The name of the student is " + mySearchItem.student)
//     response.end()
// }).listen(8080)
// console.log('Your server is running....');

// var myServer = require('http')
// var myName = require('./c-5')
// var myUrl = require("url")
// myServer.createServer((req , res)=>{
//     res.writeHead(200 , {"Content-type" : "text/html"})
//      var output = myUrl.parse(req.url , true).query
//     // res.write()
//     console.log(output);
//     res.end()

// }).listen(8080)
// console.log("Server is running");

function myPromise(){
    return new Promise((resolve , reject)=>{
        setTimeout(()=>{
            resolve("I am 2 second")
        },2000)
    })
}
function myPromise2(){
    return new Promise((resolve , reject)=>{
        setTimeout(()=>{
            resolve("I am 3 second")
        },3000)
    })
}
function myPromise3(){
    return new Promise((resolve , reject)=>{
        setTimeout(()=>{
            resolve("I am 1 second")
        },1000)
    })
}

// myPromise().then(res=>{
//     console.log(res);
// })
// myPromise2().then(res=>{
//     console.log(res);
// })
// myPromise3().then(res=>{
//     console.log(res);
// // })
// async function myAsync(){
//     // var OneSec = await  myPromise3().then(val => { return val})
//     // var TwoSec = await  myPromise().then(val => { return val})
//     // var ThreeSec = await  myPromise2().then(val => { return val})
//     var OneSec = await  myPromise3()
//     var TwoSec = await  myPromise()
//     var ThreeSec = await  myPromise2()
//     console.log(ThreeSec);
//     console.log(TwoSec);
//     console.log(OneSec);
// }
// myAsync()
//*1
sum =(a , b)=>{
    return a + b
}
console.log(sum(5,6));

//*2
console.log(sum(5,6));
sum =(a , b)=>{
    return a + b
}