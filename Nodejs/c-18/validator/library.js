//require file 
var Joi = require("joi")

var libraryValidator = Joi.object({
    bookName: Joi.string().required().alphanum(),
    author:Joi.string().required(),
    releaseDate: Joi.date().required()
})