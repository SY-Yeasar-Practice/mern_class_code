var mongoose = require("mongoose");

var Schema = mongoose.Schema

var userSchema = new Schema({
    userName: String,
    userType:{
        type: String,
        enum: ["teacher","student","librarian"]
    },
    default: "student",
    email:{
        type: String,
        unique: true
    },
    password:{
        type: String
    }
})

module.exports = mongoose.model("user",userSchema )