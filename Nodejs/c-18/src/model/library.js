var mongoose = require("mongoose");

var Schema = mongoose.Schema

var librarySchema = new Schema({
    bookName: String,
    author: String,
    releaseDate:{
        type: Date,
        default: Date.now
    },
    bookImage: String,
    bookFile: String,
    isActivate:{
        type: Boolean,
        default: false
    },
    isDeleted : {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("library",)