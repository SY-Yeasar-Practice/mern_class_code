//require part
var getExpress = require("express");
var app = getExpress();
var mongoose =  require("mongoose");
var bodyParser = require("body-parser");
require("dotenv").config();

//work with dot env file
var port = process.env.PORT || 8080;
var url = process.env.MONGOOSE_URL;

//creat the server
app.listen(port, () => {console.log(`Server is runnig on ${port}`)})

//work with body parser
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//connect to the database
mongoose.connect(url, {useCreateIndex:true, useNewUrlParser:true, useUnifiedTopology:true})
.then(() => {
    console.log("Server is connected to the database successfully");
})
.catch(err => {
    console.log(err);
})

//root path
app.get("/", (req, res) => {
    res.send("<h1>I am from root</h1>")
})

//post route

//get route


//default path
app.get("*", (req, res) => {
    res.status(404).send("<h1>404 Page not found</h1>")
})