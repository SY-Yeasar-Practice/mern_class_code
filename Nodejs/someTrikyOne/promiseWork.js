//!!difference between asynchronous and synchronous

//*What is output of below code??

const firstFunc  = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("first")
        }, 2000)
    })
}

const secondFunc = () => {
    console.log("Second");
}

const thirdFunc = async () => {
    const first = await  firstFunc()
       secondFunc()
    console.log("Third");
    console.log(first);
 
}

thirdFunc()

