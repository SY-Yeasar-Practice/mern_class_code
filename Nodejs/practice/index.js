const getExpress = require("express");
const app = getExpress();
const bodyParser = require("body-parser");
require("dotenv").config();
const mongoose = require("mongoose")
const productRoute = require("./src/route/product")
const contactRoute = require("./src/route/controller")
const personRoute = require("./src/route/person")
const adminRoute = require("./src/route/adminRegistration")
const loginRoute = require("./src/route/login")
const studentRoute = require("./src/route/student");
const { valueOf } = require("./htm/text");
const uploadRoute = require("./src/route/uploadMultipleFIle")

var bb = require('express-busboy');
bb.extend(app);


//work with dot env file
const port = process.env.PORT || 8080
const mongoUrl = process.env.URL

//body parser part
// app.use(bodyParser.urlencoded({extended:true}))
// app.use(bodyParser.json())

//connect to the server
app.listen(port, () => console.log(`Server is running on ${port}`))

//connect to the database
mongoose.connect(mongoUrl, {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
})
.then(() => {console.log("Server is connected to the database");})
.catch(err => console.log(err))

//root route
app.get("/", (req, res) => {
    res.send("<h1>I am from root</h1>")
})

//others route
app.use("/product", productRoute)
app.use("/user", contactRoute)
app.use("/person", personRoute)
app.use("/admin", adminRoute)
app.use("/user", loginRoute)
app.use("/student", studentRoute)
app.use(uploadRoute)

//default route
app.get("*", (req, res) => {
    res.send("<h1>404 Page not found</h1>")
})


// Try to judge, the ! operator has a higher priority than ==, so actually the operation of ! is also involved here.
