const mongoose = require("mongoose")

const studentSchema = mongoose.Schema({
    userName: String,
    password: String,
    userType:String,
    degree: String
})

module.exports = mongoose.model("teacher", studentSchema)