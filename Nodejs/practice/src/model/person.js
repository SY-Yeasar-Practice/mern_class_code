const mongoose = require("mongoose")

const Schema = mongoose.Schema

const personSchema = new Schema({
    index: Number,
    name: String,
    isActive: {
        type: Boolean,
        default: false
    },
    registered: {
        type: Date,
        default: Date.now
    },
    age: Number,
    gender: String,
    eyeColor: String,
    favoriteFruit: String,
    company: {
        title: String,
        email: String,
        phone: String,
        location: {
            country: String,
            address: String
        }
    },
    tags:[String]
})

module.exports = mongoose.model("person", personSchema)