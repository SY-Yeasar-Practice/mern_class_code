const mongoose = require("mongoose")

const studentSchema = mongoose.Schema({
    userName: String,
    password: String,
    userType:String
})

module.exports = mongoose.model("admin", studentSchema)