const mongoose = require("mongoose")

const studentSchema = mongoose.Schema({
    userName: String,
    password: String,
    userType:String,
    class:String
})

module.exports = mongoose.model("student", studentSchema)