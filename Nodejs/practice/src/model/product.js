const mongoose = require("mongoose");


const Schema = mongoose.Schema

const itemsSchema = new Schema({
    productName: {
        type: String,
        trim: true
    },
    productType: {
        type: String,
        enum:[
            "mobile",
            "computer",
            "laptop"
        ]
    },
    details:{
        productCode: String,
        price: String,
        mrp: {
            type: Date,
            default: Date.now
        },
        exp:{
            type: Date,
            default: Date.now
        }
    },
    shopDetails:[
        {
            shopNo: Number,
            outletNo: Number,
            place: String
        }
    ]
})

//export part
module.exports = mongoose.model("Product", itemsSchema)