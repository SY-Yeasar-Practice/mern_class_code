const { admitNewStudentController } = require("../controller/student")
const getExpress = require("express")

const route = getExpress.Router();

route.post("/admission/newStudent", admitNewStudentController)

//export part
module.exports = route