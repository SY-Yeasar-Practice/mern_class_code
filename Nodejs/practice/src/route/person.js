const { creatPersonController } = require("../controller/person")
const getExpress = require("express")
const check = require("../../middleware/check")

const route = getExpress.Router();

route.post("/create",check,  creatPersonController)

module.exports = route