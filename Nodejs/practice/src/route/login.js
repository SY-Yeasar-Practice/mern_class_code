const {loginController} = require("../controller/login")
const getExpress = require("express");
const route = getExpress.Router()


route.post("/login", loginController)

module.exports = route