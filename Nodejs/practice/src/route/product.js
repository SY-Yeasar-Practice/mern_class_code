const {creatNewProductController, updateDetailsController} = require("../controller/product")
const getExpress = require("express");

const route = getExpress.Router();

route.post("/create", creatNewProductController)
route.post("/update/:id", updateDetailsController)

//export part
module.exports = route