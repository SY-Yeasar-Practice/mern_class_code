const getExpress = require("express");
const {upload:imgUploadMiddlware} = require("../../middleware/upload")
const {syllabusController} = require("../controller/uploadMultipleFile")

const route = getExpress.Router();

route.post("/upload/single", imgUploadMiddlware.single("img"), syllabusController )
// route.post("/upload/multiple", imgUploadMiddlware.array("img"), uploadControllerMultipleImage )

module.exports = route;
