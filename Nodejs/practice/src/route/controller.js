const {contactController} = require("../controller/contact")
const getExpress = require("express");
const route = getExpress.Router()

route.post("/contact/message", contactController)

//export part
module.exports = route