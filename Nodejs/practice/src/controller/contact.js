const nodemailer = require("nodemailer")
const data = require("../../htm/text")

const user = process.env.USER
const pass = process.env.PASSWORD

const contactController = async (req, res) => {
    try{
        const {name, subject, message, email} = req.body

        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth:{
                user,
                pass
            }
        })
        const to = "nishatnikks50@gmail.com"
        const mailOption = {
            from: email,
            to,
            subject,
            // html: `<p>Name: ${name}</p> <h2>Message:</h2> <span style="font-weight:bold; color:black">${message}</span> `
            html: data
        }

        await transporter.sendMail(mailOption, (err, data) => {
            if(err){
                console.log(err);
                res.json({
                    message: "Send failed",
                    err
                })
            }else{
                res.json({
                    message: "Mail has been send successfully"
                })
                console.log(data);
            }
        })

    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

//export part
module.exports = {
    contactController
}