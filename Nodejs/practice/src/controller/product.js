const Product = require("../model/product")//get the product model


//creat a new product
const creatNewProductController = async (req, res) => {
    try{
        const {productName, productType, details, shopDetails} = req.body;
        const newProduct = new Product({
            productName,
            productType,
            details,
            shopDetails
        })

        const isSave = await newProduct.save()
        if(isSave){
            const data = isSave
            res.status(201).json({
                message:"new product created successfully",
                data
            })
        }
    }
    catch(error){
        console.log(error);
        res.json({
            error
        })
    }
}

//update the details part ///query data from a nested object
updateDetailsController = async (req, res) => {
    try{
        const {details} = req.body
        const {id} = req.params
        const {productCode, price} = details
        const isUpdate = await Product.updateOne(
            {
                _id: id
            },
            {
                $set:{
                    "details.productCode" : productCode,
                    "details.price" : price
                }
            }
        )
        if(isUpdate){
            res.json({
                message: "product has updated succefully"
            })
        }
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

//export part
module.exports = {
    creatNewProductController,
    updateDetailsController
}