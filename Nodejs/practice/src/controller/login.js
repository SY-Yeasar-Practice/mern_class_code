const Admin = require("../model/admin")
const Student = require("../model/student")
const Teacher = require("../model/teacher")
const jwt = require("jsonwebtoken")
require("dotenv").config

const SecurityCode = process.env.SECURITY

//login controller
const loginController = async (req, res) => {
    try{
        const {userName, password, userType} = req.body
        //admin
        if(userType == "admin"){
            const admin = await Admin.findOne({userName})
            if(admin){
                if(password == admin.password){
                const data = {
                    id: admin._id,
                    userType: admin.userType
                }
                const token = jwt.sign(data, SecurityCode, {expiresIn:"30m"})
                    res.json({
                        message: "Login Successfully as a admin",
                        token
                    })
                }
                else{
                    res.json({
                        message:"Password wrong"
                    })
                }
            }else{
                res.json({
                    message: "User Not found"
                })
            }
        }
        //teacher
        else if(userType == "teacher"){
            const teacher = await Teacher.findOne({userName})
            if(teacher){
                if(password == teacher.password){
                const data = {
                    id: teacher._id,
                    userType: teacher.userType
                }
                const token = jwt.sign(data, SecurityCode, {expiresIn:"30m"})
                    res.json({
                        message: "Login Successfully as a teacher",
                        token
                    })
                }
                else{
                    res.json({
                        message:"Password wrong"
                    })
                }
            }else{
                res.json({
                    message: "User Not found"
                })
            }
        }
        //student
        else if(userType == "student"){
            const student = await Student.findOne({userName})
            if(student){
                if(password == student.password){
                const data = {
                    id: student._id,
                    userType: student.userType
                }
                const token = jwt.sign(data, SecurityCode, {expiresIn:"30m"})
                    res.json({
                        message: "Login Successfully as a student",
                        token
                    })
                }
                else{
                    res.json({
                        message:"Password wrong"
                    })
                }
            }else{
                res.json({
                    message: "User Not found"
                })
            }
        }
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

module.exports = {
    loginController
}