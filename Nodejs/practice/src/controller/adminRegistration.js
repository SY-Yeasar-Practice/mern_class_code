const Admin = require("../model/admin")


const adminRegistrationController = async (req, res) => {
    try{
        const admin = new Admin(req.body)
        const isSave = await admin.save()
        if(isSave){
            res.json({
                message:"Admin created succefully",
                isSave
            })
        }
      
        
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

//export part
module.exports = {
    adminRegistrationController
}