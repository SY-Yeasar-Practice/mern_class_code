const mongoose = require("mongoose");
const Person = require("../model/person")

const creatPersonController = async (req, res) => {
    try{
        const person = new Person(req.body)
        const isSave = await person.save()
        if(isSave){
            res.json({
                message: "person has been save successfully",
                isSave
            })
        }
    }
    catch(err){
        res.json({
            err
        })
    }
}

//export part
module.exports = {
    creatPersonController
}