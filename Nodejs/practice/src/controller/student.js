const Student = require("../model/student")

//admit a new student
const admitNewStudentController = async (req, res) => {
    try{
        const student = new Student(req.body)
        const isSave = await student.save()
        if(isSave){
            res.json({
                message:"Student created succefully",
                isSave
            })
        }
        
    }
    catch(err){
        console.log(err);
        res.json({
            err
        })
    }
}

//export part
module.exports = {
    admitNewStudentController
}