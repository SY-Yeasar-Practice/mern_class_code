//module require 
var getExpress = require("express");
var app = getExpress();
var mongoose = require("mongoose")
require("dotenv").config();
var bodyParser = require("body-parser")
var postRoute = require("./route/post")


//body-parser part
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//dot env part
var port = process.env.PORT;
var mongoUrl = process.env.URL;

//creat the server
app.listen(port , ()=>{console.log(`Server is running on ${port}`)})

//connect to the data base 
mongoose.connect(mongoUrl,{useNewUrlParser: true, useUnifiedTopology: true })
.then(()=>{
    console.log("Server is connected to database");
})
.catch(err => {
    console.log(err);
})

//root path
app.get("/" , (req,res) => {
    res.send("<h1>I am from root file</h1>")
})

//post path
app.use("/student" , postRoute)


//default path
app.get("/" , (req,res) => {
    res.status(404).send("<h1>404 Page not found/h1>")
})