//required module
var studentLogin = require("../model/login")
var getExpress = require("express")
var mongoose = require("mongoose")
var bcrypt = require("bcrypt")

//make the login controller one
var studentLoginController = (req , res) => {
    var {name,password,email,updated,class:{className,mark}} = req.body

    var res = className
    bcrypt.hash(password, 10, (err , hasData) => {
        if(err){
            res.json({
                message: "There have some issue in the hash password",
                err
            })
        }
    
        var student = new studentLogin({
            name,
            password: hasData,
            email,
            updated,
            className,
            mark
        })
        
        student.save()
        .then((data)=>{
            res.json({
                message:"Student data successfully saved",
                data: data
            })
        })
        .catch(err=>{
            res.json({
                err
            })
        })
    })
}

module.exports = {
    studentLoginController
}
