//require module part
var {studentLoginController} = require("../controller/login")
var getExpress = require("express")

//creat the route
var route = getExpress.Router()

route.post("/login" ,studentLoginController )

//export part
module.exports = route;