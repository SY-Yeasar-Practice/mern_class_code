//module require 
var mongoose = require("mongoose")
var validator = require("validator")


var classSchema = new mongoose.Schema({
    className: {
        require:[true, "class name is required"],
        type: String
    },
    mark:{
        required: [true, "Mark is required"],
        type: Number
    }
})


var loginModel = new mongoose.Schema({
    name: {
        required: [true, "name is required"],
        type: String
    },
    password: {
        type: String,
        required: [true, "password is required"],
        minLength: 8
    },
    email: {
        type: String,
        required: [true, "email is required"],
        trim: true,
        unique: [true, "email exist"],
        validate: {
            validator: validator.isEmail,
            message: "Email is invalid"
        }
    },
    class:[classSchema],
    updated: {
        type: Date,
        default: Date.now,
        required: [true, "Update required"]
    }
})

module.exports = mongoose.model("login", loginModel)

