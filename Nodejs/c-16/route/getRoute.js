var {allUserController, uniqeUserController, someDataController} = require("../controller/userController")
var getExpress = require("express"); 


var route = getExpress.Router()

route.get("/allUser", allUserController)
route.get("/show/details" , someDataController)
route.get("/:userName/details" , uniqeUserController)


module.exports = route;