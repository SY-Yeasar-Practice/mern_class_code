var mongoose = require("mongoose");
require("dotenv").config()
var database = process.env.DATA_BASE

var userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: [true, "User name is required"],
        unique: true
    },
    password:{
        type: String,
        required: [true, "password is required"],
        min: 8
    },
    name:{
        type: String,
        required: [true, "name is required"]
    },
    age:{
        type: String,
        required: [true, "age is required"]
    },
    email:{
        type: String,
        unique: true,
        required: [true, "email is required"]
    },
    birthDate: {
        type: Date
    },
    update:{
        type: Date,
        required: true,
        default: Date.now
    }
})

var userModel = mongoose.model("student", userSchema ) //creat the model

//export part
module.exports = userModel;