//module import part
var getExpress = require("express");
var app = getExpress();
require("dotenv").config();
var mongoose = require("mongoose")
var bodyPartser = require("body-parser");
var connected = require("./databaseConnection")
var postRoute = require("./route/postRoute")

//work of body parser
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json)

//work with dot env
var port = process.env.PORT || 8080;


connected()

//creat the server 
app.listen(port, () => console.log(`Server is running on ${port}`))

//root route
app.get("/" , (req, res) => {
    res.send("<h1>I am from root</h1>")
})

//get route
app.use("/user", postRoute )
//post route

//default route
app.get("*",(req, res) => {
    res.status(404).send("404 Page not found")
})



