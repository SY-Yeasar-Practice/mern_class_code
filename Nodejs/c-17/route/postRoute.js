var mongoose = require("mongoose");
var {registrationController, userLoginController} = require("../controller/user")
var getExpress = require("express");

var route = getExpress.Router()

route.post("/registration" , registrationController )
route.post("/login", userLoginController)


//export part
module.exports = route