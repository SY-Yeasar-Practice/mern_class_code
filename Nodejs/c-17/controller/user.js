//module import part 
var getExpress = require("express");
var app = getExpress();
var mongoose = require("mongoose");
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
var userModel = require("../model/userModel")
require("dotenv").config()

var key = process.env.SECRET_KEY

var registrationController = async (req, res) => {
    try{
        var {userName, password, name, age, email} = req.body;
        var hashPassword = await bcrypt.hash(password, 10)
        var newUser = new userModel({
            userName,
            password: hashPassword,
            name,
            age,
            email
        })

        var saveData = await newUser.save()
        if(saveData){
            res.json({
                message: "new user created",
                user: saveData
            })
        }
    }
    catch(err){
        res.json({
            err
        })
    }
}

//login controller
var userLoginController = async (req, res) => {
   try{
        var {userName, password} = req.body;//get the data from body
        var isValidUser = await userModel.findOne({userName})//return a promise is valid user or  not
        if(isValidUser){
        var User = isValidUser
        var isValidPassword = await bcrypt.compare(password , User.password)//return a promise is valid password or  not
        var paswordMatch = isValidPassword //store the boolean value here
        var sendData = {
            userName: User.userName,
            email: User.email,
            age: User.age
        }
        var token = jwt.sign(sendData, key, {expiresIn: "1m"})
        if(paswordMatch){
            res.json({
                message: "login successfully",
                dataToken : token
            })
        }else{
            res.json({
                message: "password doesn't match please try again"
            })
        }
        }else{
            res.json({
                message: "User not found"
            })
        }
   }
   catch(err){
       res.json({
           err
       })
   }
}

//export part
module.exports = {
    registrationController,
    userLoginController
}