//module import part
require("dotenv").config();
var mongoose = require("mongoose")

//work with dot env
var url = process.env.URL

var connection = async () => {
   try{
        var connect = await mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true , useCreateIndex: true})
        if(connect){
            console.log("Server is connected with database");
        }
   }
   catch(err){
       console.log(err);
   }
}

module.exports = connection