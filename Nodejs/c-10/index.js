var getExpress = require("express");
var app = getExpress();
var port = 3030;
var {rout:getTheRout} = require("./route") //get the rout from here
app.listen(port,() => console.log(`Server is running in ${port}`))
// ex.use(res)
app.use(getTheRout);

app.get("/", (req , res) => {
    res.send("<h1>This is a rout file</h1>")
})

app.get("*", (req , res) => {
    res.send("<h1>Page not found</h1>")
})
