var student = [
    {
        name: "Shah Sadmaney Yeasar",
        id: 1730034,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Md. Shah Newaz",
        id: 1730587,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Nowshin Tasnim",
        id: 1700258,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Md Samuel Islam",
        id: 1830034,
        dept: "EEE",
        season: "2018-2019",
        expectedGraduationYeas: "2024"
    },
    {
        name: "Md. Zahid Hasan",
        id: 1730234,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Ahsanul Haque",
        id: 1735034,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Puja Bhoumik",
        id: 1731034,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "NJ Antora",
        id: 1730035,
        dept: "CSE",
        season: "2017-2018",
        expectedGraduationYeas: "2023"
    },
    {
        name: "Linkon",
        id: 1720034,
        dept: "EEE",
        season: "2019-2020",
        expectedGraduationYeas: "2025"
    },
    {
        name: "Md Sabuj",
        id: 1730032,
        dept: "CSE",
        season: "2018-2019",
        expectedGraduationYeas: "2023"
    }
]

module.exports = {student};