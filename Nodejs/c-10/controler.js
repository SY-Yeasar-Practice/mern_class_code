var {student:studentData} = require("./data") //export student data here
var getExpress = require("express") // get the express;
var app = getExpress();

var studentDetailsWithDept = (req , res) => {
    var {dept , session} = req.params; //get the param data
    var searchData = studentData.filter(val => {
        return val.season == session && val.dept == dept;
    })//filter the data
    setTimeout(()=>{
        res.send(searchData)
    },1500)

}   // /home/student/:dept/:session

var studedentDetails = (req , res) => {
    var {id} = req.params; //get the param data
    var searchData = studentData.filter(val => {
        return val.id == id;
    })//filter the data
    
    setTimeout(()=>{
        res.send(searchData)
    },1500)
}  //  /home/student/:id


var allstudent = (req , res) => {

    var dataArray = [];
    var searchData = studentData.map(val => {
        dataArray.push(val)
    })//filter the data
    
    setTimeout(()=>{
        res.send(dataArray)
    },1500)
} //  /home/student

module.exports = {studedentDetails , studentDetailsWithDept , allstudent};//export part