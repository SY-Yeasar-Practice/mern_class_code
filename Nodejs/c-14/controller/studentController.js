//requires module
var loginModel = require("../model/login")
var bcrypt = require("bcrypt")

//loginController
var loginController = (req , res) => {
    // var {name , userName, password, phoneNumber} = req.body;//get the data from front end part
    var {name,password} = req.body
    
    bcrypt.hash(password , 10 , (error , hash) => {

        if(error){
            res.json({
                message: "There have a problem in hash the password",
                error
            })
        }
        var student = new loginModel({
            name:name,
            password: hash
        })//get the data from front end and save it in the database according to their requirement

        student.save()
        .then(result => {
            res.status(201).json({
                message: "Saved successfully",
                loginInfo: result
            })
        })
        .catch(err => {
            res.json({
                err
            })
    })
        
    })

   
}

module.exports = {
    loginController
}