//requires module
var getExpress = require("express")
var {loginController} = require("../controller/studentController")

//make a route
var route = getExpress.Router()

route.post("/login" ,loginController )

module.exports = route