//required module
var getExpress = require("express")
var app = getExpress();
var mongoose = require("mongoose");
require("dotenv").config();
var bodyParser = require('body-parser')
var route = require("./route/postRout")

//work of body parser
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

//dot env file
var port = process.env.PORT;
var url = process.env.URL;


// //connect to mongo db database
mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})
.then(()=>{
    console.log("Server is connected to the database....");
})
.catch(err => console.log(err))


//creat a server
app.listen(port, ()=> console.log(`server is running on ${port}`))

//root path
app.get("/", (req,res) => {
    res.send("<h1>This is a root file</h1>")
})

//our more route 
app.use(route)

//default path
app.get("*", (req,res) => {
    res.status(404).send("<h>404 Page not found</h>")
})