//require the module
var getExpress = require("express");
var app = getExpress();
var mongoose = require("mongoose");
require("dotenv").config();
var bodyParser = require("body-parser");

//work with dot env file
var port = process.env.PORT || 8080 ;
var URL = process.env.MongoURL;

//work with body-parser middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//creat the server 
app.listen(port, () => {
    console.log(`Server is running on ${port}`);
})

//connect with data base using mongoose
mongoose.connect(URL,{useNewUrlParser: true , useCreateIndex:true , useUnifiedTopology: true })

mongoose.connection.once("open", ()=>{
    console.log("Data base is connected succefully");
}).on("error" , (error)=>{
    console.log("Connection Error: error",error);
})


