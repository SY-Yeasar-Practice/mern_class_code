
var getExpress = require("express");
var app = getExpress();
var port = 8080;
 
app.listen(port, () => console.log(`Server is running in ${port}`))//creat the server
 
var student = [
    {
        name: "Rahim",
        id: 1,
        eyeColor: "black",
        age: 18
    },
    {
        name: "Karim",
        id: 2,
        eyeColor: "black",
        age: 19
    },
    {
        name: "Fahim",
        id: 3,
        eyeColor: "black",
        age: 20
    },
    {
        name: "Ridom",
        id: 4,
        eyeColor: "black",
        age: 14
    },
    {
        name: "Rakib",
        id: 5,
        eyeColor: "brown",
        age: 14
    },
]//this is my data
 
//   all information from only one route 
 
app.get('/studentinfo', (req,res)=> {
    const { eyeColor, age } = req.query; 
    var getTheStudent;
    if(eyeColor && age) {
       getTheStudent = student.filter(val => val.eyeColor == eyeColor && val.age > age)
    }
    else if(age) {
        getTheStudent = student.filter(val => {
            if (val.age <= age) {
                return val.eyeColor = "black";
            }
        })
    }
    else {
        getTheStudent = student.map(ele => {
            return {
                myName: ele.name,
                id: ele.id
            }
        })
    }
 
    setTimeout(()=> {
        res.send(getTheStudent)
    },2000)
})