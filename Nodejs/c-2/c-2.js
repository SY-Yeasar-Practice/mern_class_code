// var myObj = {
//     name:{
//         name : 'shopnil',
//         roll : 25
//     },
//     age : 25
// }
// var {name} = myObj
// var {name , roll} = name
// console.log(name , roll);

// const arr = [{
//         name : "tata",
//           years:"2020",
//           price:"10000"
//         },
//         {
//         name : "tata",
//           years:"2010",
//           price:"20000"
//         },
//         {
//         name : "yamaha",
//           years:"1990",
//           price:"30000"
//         },
// ]
// var another = {
//     name : "HYNDAI",
//     years:"1990",
//     price:"30000"
// }
// var res = arr.fill(another , 0 , 2)
// console.log(arr);

// var student = {
//   name:'shopnil',
//   roll: 55,
//   age : 25
// }
// var key = Object.keys(student)
// var value = Object.values(student)
// var all = Object.entries(student)
// console.log(key);
// console.log(value);
// console.log(all);


//*Q: find the total mark
var mark = {
  bangla : 80 , 
  english : 55 ,
  math : 85
}

//*by using for in loop
// var totalMark= 0
// for(key in mark){
//   totalMark += mark[key]
// }
// // console.log(totalMark);

// //*by using reduce method
// var res  = Object.values(mark)
// var total = res.reduce((update , current) => {
//    return update += current
// },0)

// console.log(total);

//*find the length of this object
// var mark = {
//   bangla : 80 , 
//   english : 55 ,
//   math : 85
// }

// function length(obj){
//   // var myObj = obj
//   // var length = Object.keys(myObj).length
//   // return length

//   var myObj = obj;
//   var empty = []
//   for(key in myObj){
//     empty.push(myObj[key])
//   }
//   return empty.length
// }

// var res = length(mark)
// console.log(res);

//*Find that is it a empty array or not
// var mark = {
//   bangla : 80 , 
//   english : 55 ,
//   math : 85
// }

// isEmpty = (obj) =>{
//   var myObj = obj 
//   var length = Object.keys(myObj)
//   var res
//   (length == 0) ? res = true : res = false
//   return res
// }
// console.log(isEmpty(mark));

// isEmpty = (obj) =>{
//   var myObj = obj 
//   var lengthArray = []
//   for(key in myObj){
//     return false
//   }
//   return true
// }
// console.log(isEmpty(mark));


//*Find the total marks when there have another key
// var mark = {
//   bangla : 80 , 
//   english : 55 ,
//   math : 85,
//   isPassed : true ,
//   position : "first"
// }
// var total = 0
// for(key in mark){
//   if(typeof mark[key] == 'number' ) total += mark[key]
// }
// console.log(total);

//*Two array never equal
// var a = [1,2,3]
// var b = [1,2,3]
// console.log(JSON.stringify(a) == JSON.stringify(b));
// console.log(JSON.stringify(a));


// var a = [
//   {
//     age : 5
//   },
//   {
//     age : 5
//   },
//   {
//     age : 5
//   }
// ]

// var totalAge = a.reduce((a,b) =>{
//   return a += b.age
// },0)
// console.log(totalAge);
// var a = [ 1 , 2]
// var res = a.reduce((a , b) => {
//   return a + b
// })

// console.log(res)
