import React from 'react'

const AnotherComp = ({data, handler}) => {
    return (
        <div>
             <form action="">

                    <label htmlFor="exampleFormControlTextarea1">I am from Child Component</label>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name = "data" value = {data} onChange = {handler}></textarea>
                    </div> 

                    {/* <label for="exampleFormControlTextarea1">Instant Input</label>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" value = {data}></textarea>
                    </div>  */}
                </form>
        </div>
    )
}

export default AnotherComp
