import React, { Component } from 'react'
import AnotherComp from '../AnotherInputComp/AnotherComp'

class InstantInput extends Component {
    constructor() {
        super()
        this.state = {
            data: "",
            parent: React.createRef()
        }
    }

    //inputHandler
    inputHandler = (e) => {
        this.setState({
            data: e.target.value
        })
    }

    submit = (e) => {
        e.preventDefault()
        let res = this.state.parent.current.value
        console.log(res);
    }

    render() {
        return (
            <div>
                {/* <form action="">

                    <label for="exampleFormControlTextarea1">Write Some Thing</label>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name = "data" value = {this.state.data} onChange = {this.inputHandler}></textarea>
                    </div> 

                    <label for="exampleFormControlTextarea1">Instant Input</label>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" value = {this.state.data}></textarea>
                    </div> 
                </form> */}
                <AnotherComp data={this.state.data} handler={this.inputHandler} />
                <label htmlFor="exampleFormControlTextarea1">I am from Parent Component</label>
                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" value={this.state.data} ref = {this.state.parent} name="parent"></textarea>
                </div>
                <button onClick={this.submit}>Click Me</button>
            </div>
        )
    }
}

export default InstantInput
