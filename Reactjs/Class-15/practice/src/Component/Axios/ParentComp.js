import React, { Component } from 'react'
import axios from 'axios'

class ParentComp extends Component {
    constructor () {
        super()
        this.state = {
            data : []
        }
    }
    componentDidMount() {
        axios.get(
            'http://localhost:3030/user/student/view/all/One'
            // 'http://api.openweathermap.org/data/2.5/weather?q=Pune&appid=7bf49d27c9852e49fa1ac3584eb9b8bb'
        )
        .then(data =>{
            this.setState({
                data: data.data.findStudent
            })
            console.log(data);
        })
        .catch(err => console.log(err))
        // this.setState ({
        //     data:
        // })
    }
    render() {
        const {data} = this.state
        return (
            <div>
                {
                    data.length > 0 ?
                    data.map((data, ind) => {
                        const {name} = data.personalInfo
                        return(
                            <h1 key = {ind} >Welcome {name.FirstName} {name.LastName}</h1>
                        )
                    })
                    :
                    <h1>Loading....</h1>
                }
            </div>
        )
    }
}

export default ParentComp