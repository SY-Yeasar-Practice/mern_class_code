import React, { Component } from 'react'

class SumComp extends Component {
    constructor () {
        super()
        this.state = {
            firstInput: 0,
            lastInput: 0,
            sum: 0
        }
        this.resultRef = React.createRef()
    }

    // inputHandler
    inputHandler = (e) => {
        e.preventDefault()

        this.setState({
            [e.target.name]: e.target.value
        })
        
    }
    render() {
        const {firstInput, lastInput} = this.state
        const sum = +firstInput + +lastInput
        return (
            <div>
                <label htmlFor="firstInput">First Input </label>
                <input type="text" name="firstInput" id="" onChange = {this.inputHandler} /> <br />

                <label htmlFor="lastInput">Last Input </label>
                <input type="text" name="lastInput" id="" onChange = {this.inputHandler} /> <br />

                <label htmlFor="sum">Sum </label>
                <input type="text" name="sum" id="" value = {sum} ref = {this.resultRef}/> <br />
            </div>
        )
    }
}

export default SumComp
