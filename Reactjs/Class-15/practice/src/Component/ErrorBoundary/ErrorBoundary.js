import React, { Component } from 'react'

class ErrorBoundary extends Component {
    constructor () {
        super()
        this.state = {
            hasError: false
        }
    }
    static getDerivedStateFromError (error) {
        return {
            hasError: true
        }
    }
    render() {
        if(this.state.hasError){
            return (
                <h1>Some this error in this component</h1>
            )
        }else{
            return this.props.children
        }
    }
}

export default  ErrorBoundary