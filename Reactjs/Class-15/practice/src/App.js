import logo from './logo.svg';
import './App.css';
import ParentComp from './Component/Axios/ParentComp';
import SumComp from './Component/DynamicSum/SumComp';
import ErrorBoundary from './Component/ErrorBoundary/ErrorBoundary';

function App() {
  return (
    <div className="">
      <ErrorBoundary>
         <ParentComp/>
      </ErrorBoundary>  

      <ErrorBoundary>
          <SumComp/>
      </ErrorBoundary>
    </div>
  );
}

export default App;
