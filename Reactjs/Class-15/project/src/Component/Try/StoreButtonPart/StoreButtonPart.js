import React, { Component } from 'react'

class StoreButtonPart extends Component {
    constructor (props) {
        super()
        this.state = {
            data : props.buttonData
        }
    }
    componentDidMount() {
        console.log(this.state.data);
    }
    render() {
        return (
            <div>
               {
                   this.props.buttonData.map((data, ind) => {
                        return (
                            <div  key = {ind} style = {{ marginRight:"3%", marginBottom:"3%", marginTop:"3%"}} className = {`d-inline btn btn-secondary`} >
                                <a href="#" style = {{textDecoration: "none"}} className = {`text-warning `}>{data} <i class="fas fa-times"></i></a>
                            </div>
                        )
                   })
               }
               
            </div>
        )
    }
}

export default StoreButtonPart
