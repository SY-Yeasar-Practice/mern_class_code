import React, { Component } from 'react'
import StoreButtonPart from '../StoreButtonPart/StoreButtonPart'
import OptionPart from '../OptionPart/OptionPart'
import Data from '../Data/Data.js'

class Main extends Component {
    constructor () {
        super()
        this.state = {
            optionData : Data
        }
    }
    render() {
        return (
            <div>
                {/* header  */}
                <div style = {{textAlign : "center"}}>
                    <h2>Next Topper</h2>
                    <h3>React UI Assignment</h3>
                    <h3>Total Mark: 50</h3>
                </div>
                
                <div className = {`container`}>
                    {/* down option part  */}
                    <div className = {`mt-3`}>
                        <OptionPart data = {this.state.optionData} ref = {this.state.optionRef}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Main