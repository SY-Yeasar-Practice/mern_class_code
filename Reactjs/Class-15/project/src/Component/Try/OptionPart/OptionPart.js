import React, { Component } from 'react'
import StoreButtonPart from '../StoreButtonPart/StoreButtonPart'

class OptionPart extends Component {
    constructor () {
        super()
        this.state = {
            optionValue : "",
            submitData: []
        }
    }

    optionChangeHandler = (e) => {
        this.setState({
            optionValue: e.target.value
        })
    }

    submitHandler = (e) => {
        e.preventDefault()
        this.setState ({
            submitData: [...this.state.submitData, this.state.optionValue]
        })
    }
    
    render() {
        return (
            <React.Fragment>
                 {/* upper button part  */}
                    <div className = {`mt-3 mb-3`}>
                        <StoreButtonPart buttonData = {this.state.submitData}/>
                    </div>

                <select class="form-select" aria-label="Default select example" onChange = {this.optionChangeHandler} >
                     {
                        this.props.data.map ((data, ind) =>{
                            return (
                                <option key = {data.itemNo} selected = {data.itemValue == 'tag 2'} value = {data.itemValue}>{data.itemValue}</option>
                            )
                        })
                    }
                </select>
                <button className = {`mt-2`} onClick = {this.submitHandler} >Submit</button>

            </React.Fragment>
        )
    }
}

export default OptionPart
