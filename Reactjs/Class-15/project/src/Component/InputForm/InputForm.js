import React, { Component } from 'react'
import Loader from './Loader'
import axios from 'axios'


class InputForm extends Component {
    constructor () {
        super()
        this.state = {
            divisionData: "",
            districtData: "",
            formData : {
                division: "",
                district: ""
            }
        }
    }

    //event handler part 

    // divisionHadler
    divisionHadler = (e) => {
        e.preventDefault()
        this.setState({
            formData: {
                [e.target.name]: e.target.value
            }
        })
        const findDivisionData = this.state.divisionData.data.find(data => data.name == e.target.value) //find the division from the state value
        const {id} = findDivisionData //get the division id 

    }

    
    componentDidMount () {
        axios.get('https://api.digitalpathshalabd.com/district/all')
        .then(data => {
            this.setState({
                districtData: data
            })
        })
        .catch(err => {
            console.log(err);
        })

        axios.get('https://api.digitalpathshalabd.com/division/admin/all')
        .then(data => {
            this.setState ({
                divisionData: data
            })
        })
        .catch(err => {
            console.log(err);
        })
    }
    render() {
        const {districtData, divisionData} = this.state
        console.log( this.state.formData.division);
        return (
            <div>
                {
                    this.state.divisionData.length == 0
                    ?
                    <Loader/>
                    :
                    <div class="form-group">
                        <label htmlFor="exampleFormControlSelect1">Division</label>
                        <select className="form-control" id="exampleFormControlSelect1" onChange = {this.divisionHadler} name = "division">
                            {/* itterate the division data  */}
                            {
                                divisionData.data.map(data => {
                                    const {name, id} = data //get the data from division api
                                    const valueOfDivision = {
                                        name,
                                        id
                                    } //sent the data as a value
                                    return(
                                        <option value= {name} key = {id} >{name}</option>
                                    )
                                })
                            }
                        </select>
                        
                        {/* District part  */}
                        <label for="exampleFormControlSelect1">Example select</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>{this.state.formData.district == "" ? "" : this.state.formData.division.data}</option>
                        </select>

                    </div>
                }
            </div>
        )
    }
}

export default InputForm