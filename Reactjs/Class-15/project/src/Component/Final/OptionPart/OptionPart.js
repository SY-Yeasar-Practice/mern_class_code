import React, { Component } from 'react'


class OptionPart extends Component {
    constructor() {
        super()
        this.state = {
            optionValue: "",
            submitData: []
        }
    }

    optionChangeHandler = (e) => {
        this.setState({
            optionValue: e.target.value
        })
    }

    submitHandler = (e) => {
        e.preventDefault()
        if (this.state.optionValue) {
            this.setState({
                submitData: [...this.state.submitData, this.state.optionValue]
            })
        }
    }

    deleteHandler = (e, ind) => {
        e.preventDefault()
        const data = this.state.submitData
        data.splice(ind, 1)
        this.setState({
            submitData: data
        })
        console.log(`remain data:${data}`);
    }

    render() {
        return (
            <React.Fragment>

                {/* upper button part  */}
                <div className={`mb-3`}>
                    {
                        this.state.submitData.map((data, ind) => {
                            return (
                                <div key={ind} style={{ marginRight: "3%", marginBottom: "3%", }} className={`d-inline btn btn-secondary`} >
                                    <a href="#" style={{ textDecoration: "none" }} className={`text-warning `} >{data} <i class="fas fa-times" onClick={(e) => this.deleteHandler(e, ind)} ></i></a>
                                </div>
                            )
                        })
                    }
                </div>

                {/* option part  */}
                <div>
                    <select class="form-select" aria-label="Default select example" onChange={this.optionChangeHandler} value={this.state.optionValue} >
                        {
                            this.props.data.map((data) => {
                                return (
                                    <option key={data.itemNo} selected={data.itemValue == 'tag 2'} value={data.itemValue}>{data.itemValue}</option>
                                )
                            })
                        }
                    </select>
                </div>

                {/* submit button  */}
                <button className={`mt-2`} onClick={this.submitHandler} >Submit</button>
            </React.Fragment>
        )
    }
}

export default OptionPart
