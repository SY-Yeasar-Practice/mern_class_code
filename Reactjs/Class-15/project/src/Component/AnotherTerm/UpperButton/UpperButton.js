import React, { Component } from 'react'

class UpperButton extends Component {
    render() {
        const { submitData, deleteController } = this.props
        return (
            <React.Fragment>
                <div className={`row`}>
                    {
                        submitData.map((data, ind) => {
                            return (
                                <div key={ind} style={{ marginRight: "3%", marginBottom: "3%", }} className={`d-inline-block btn btn-secondary col-1`} >
                                    <a href="#" style={{ textDecoration: "none" }} className={`text-warning `} >{data} <i class="fas fa-times" onClick={(e) => deleteController(e, ind)} ></i></a>
                                </div>
                            )
                        })
                    }
                </div>
            </React.Fragment>
        )
    }
}

export default UpperButton