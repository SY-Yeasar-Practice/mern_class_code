import React, { Component } from 'react'
import UpperButton from '../UpperButton/UpperButton'

class OptionPart extends Component {
    constructor() {
        super()
        this.state = {
            optionValue: "",
            submitData: []
        }
    }

    optionChangeHandler = (e) => {
        this.setState({
            optionValue: e.target.value
        })
    }

    submitHandler = (e) => {
        e.preventDefault()
        if (this.state.optionValue) {
            this.setState({
                submitData: [...this.state.submitData, this.state.optionValue]
            })
        }
    }

    deleteHandler = (e, ind) => {
        e.preventDefault()
        const data = this.state.submitData
        data.splice(ind, 1)
        this.setState({
            submitData: data
        })
    }

    render() {
        return (
            <React.Fragment>

                {/* upper button part */}
                <div className={`mb-3`}>
                    <UpperButton submitData={this.state.submitData} deleteController={this.deleteHandler} />
                </div>

                {/* option part  */}
                <div>
                    <select class="form-select" aria-label="Default select example" onChange={this.optionChangeHandler} value={this.state.optionValue} >
                        {
                            this.props.data.map((data) => {
                                return (
                                    <option key={data.itemNo} selected={data.itemValue == 'tag 2'} value={data.itemValue}>{data.itemValue}</option>
                                )
                            })
                        }
                    </select>
                </div>

                {/* submit button  */}
                <button className={`mt-2`} onClick={this.submitHandler} >Submit</button>

            </React.Fragment>
        )
    }
}

export default OptionPart
