import React from 'react'

class MyClassComp extends React.Component {
    constructor(){
        super() 
        this.state = {
            studentInfo : [
                {
                    name: "Sadmaney Yeasar",
                    age: 25
                },
                {
                    name: "Nafisa Tabassum",
                    age: 22
                },
                {
                    name: "Naimur Nishat",
                    age: 35
                },
                {
                    name: "Sidrat",
                    age: 15
                },
            ]
        }
    }

    render(){
        const {studentInfo} = this.state //get the data from state value
        return (
            studentInfo.map(allInfo => {
               return (
                    <h1>My name is {allInfo.name} and I am {allInfo.age} years old</h1>
               )
            })
        )
    }
}

export default MyClassComp