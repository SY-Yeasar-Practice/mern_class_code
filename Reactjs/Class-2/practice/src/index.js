import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Hello} from './component/Jsx/JSX';
import FuncProp from './component/FunctionalProps/FuncProp';
import ClassComp from './component/ClassComp/ClassComp';

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <Hello/> */}
    <FuncProp name = "shopnil" age = {25} workIn = "student" relationShip = "single">
      <h2>This is children</h2>
    </FuncProp>
    <FuncProp name = "sneha" age = {25} workIn = "student" relationShip = "single"></FuncProp>
    <FuncProp name = "nishat" age = {25} workIn = "student" relationShip = "single"></FuncProp>
    <ClassComp name = "Naimur Nishat" age = {25} workIn = "student" relationShip = "in a relationship"/>
    <ClassComp  name = "Nafisa Tabassum" age = {25} workIn = "student" relationShip = "in a relationship">
      <h1>Hello</h1>
    </ClassComp>
    <ClassComp name = "Nishat pagla" age = {25} workIn = "student" relationShip = "in a relationship"/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
