import React, {Component} from 'react'

class ClassComp extends Component {
    constructor(){
        super()
        this.state = {
            name: "shopnil",
            age: 25
        }
        this.info = "I am from state"
    }
    render(){
        const {name, age, children, workIn, relationShip} = this.props //get the data from props
        let {name:stateName, age:stateAge} = this.state
        const {info} = this
        return(
            <div>
                {/* <h1>Hello My name is {stateName.toUpperCase()}, I am {stateAge} years old, I am {relationShip}, I am a {workIn}</h1>
            <div>{children }</div> <h1>{info}</h1> */}
            <h1>My name is {stateName}</h1>
            </div>
        )
    }
}

export default ClassComp