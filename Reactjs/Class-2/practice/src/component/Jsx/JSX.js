import React, {Component} from 'react'
import jsx from './jsx.css'

const Hello = () => {
    return (
        
        <div className = "hello">
            <h1>I am Hello One and i am from functional component</h1>
            <h2>I am Hello Two and i am from functional component</h2>
        </div> //this is jsx code which is convert to vanila javascrtipt  in the background


       
        // React.createElement(
        //     "div",
        //     {className:"hello"},
        //     React.createElement(
        //         "h1", null, "I am Hello One and i am from functional component"
        //     ),
        //     React.createElement(
        //         "h2", null, "I am Hello One and i am from functional component"
        //     ),
        //     React.createElement(
        //         "h3", null, "I am Hello One and i am from functional component"
        //     )
        // )   //this code is happen in the background the jsx is in upper one
    )
}


export  {
    Hello
}