import React from 'react'

// //type one to get props
// const FuncProp = (props) => {
//     const {name, age, relationShip, workIn, children} = props //get the value from props
//     return(
//         <div>
//             <h1>Hello My name is {name.toUpperCase()}, I am {age} years old, I am {relationShip}, I am a {workIn}</h1>
//         </div>
//     )
// }

//type two to get props   !!preferable one
const FuncProp = ({name, age, relationShip, workIn, children}) => {
    return(
        <div>
            <h1>Hello My name is {name.toUpperCase()}, I am {age} years old, I am {relationShip}, I am a {workIn}</h1>
            <div>{children }</div>
        </div>
    )
}

//type three to get props
// const FuncProp = (props) => {
//     return(
//         <div>
//             <h1>Hello My name is {props.name.toUpperCase()}, I am {props.age} years old, I am {props.relationShip}, I am a {props.workIn}</h1>
//             {props.children}
//         </div>
//     )
// }

export default FuncProp