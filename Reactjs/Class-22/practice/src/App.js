import logo from './logo.svg';
import './App.css';
// import Parent from './Component/UseReducer/Parent/Parent';
import {Provider} from 'react-redux'
import store from './Component/UseRedux/Global/Store';
import Parent from './Component/UseRedux/Comp/Parent/Parent';

function App() {
  return (
    <div className="">
      <Provider store = {store}>
          <Parent/>
      </Provider>
      {/* <Parent/> */}
    </div>
  );
}

export default App;
