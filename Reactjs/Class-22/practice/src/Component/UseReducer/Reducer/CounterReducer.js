const initialState = {
    count: 0
}
const reducer = (state, action) => {
    switch (action.type) {
        case "Increment": {
            return {
                ...state,
                count: state.count + action.payload
            }
        }
        case "Decrement": {
            const value = state.count - action.payload
            if(value > 0) {
                return {
                    ...state,
                    count: state.count - action.payload
                }
            }
        }
        case "Reset": {
            return {
                ...state,
                count: 0
            }
        }
        default: {
            return state

        }
    }
}

export  {
    reducer,
    initialState
}