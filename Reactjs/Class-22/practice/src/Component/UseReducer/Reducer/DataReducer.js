const initial = {
    isLoading: true,
    isError: false,
    errorMessage: "",
    data: "",
    comment: ""
}


const DataReducer = (state, action) => {
    switch (action.type) {
        case "Success": {
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payLoad
            }
        }
        case 'Failed' : {
            return {
                ...state,
                isLoading: true,
                isError: true,
                errorMessage: "Some things Error",
                data: ""
            }
        }
        case 'GetComment' : {
            return {
                ...state,
                comment: action.payLoad,
            }
        }
        case 'GetCommentFailed': {
            return {
                ...state,
                comment: ""
            }
        }
        default : {
            return state
        }
    }
}

export {
    DataReducer,
    initial
}