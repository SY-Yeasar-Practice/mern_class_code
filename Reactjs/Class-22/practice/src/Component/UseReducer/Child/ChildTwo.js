import React, { useContext } from 'react'
import { counterDecrement, counterIncrement, counterReset } from '../Action/CounterAction'
import { CounterContext } from '../Context/CounterContext'
import ChildThree from './ChildThree'

const ChildTwo = () => {
    const ProviderData = useContext(CounterContext)
    const {count, data} = ProviderData
    const {countValue, dispatch:countDispatch} = count
    const {dataValue, dispatch} = data
    const {isLoading, isError, errorMessage, data:fetchData, comment} = dataValue 
    console.log(`I am render from child two`);
    return (
        <div>
            <h1>Hello I am from Child Teo</h1>
            <button onClick={(e) => countDispatch(counterIncrement(5))} >Increment</button>
            <button onClick={(e) => countDispatch(counterDecrement(5))} >Decrement</button>
            <button onClick={(e) => countDispatch(counterReset())} >Increment</button>
            <h1>{fetchData.title} <mark>from Child two</mark></h1>
            <ChildThree/>
        </div>
    )
}

export default ChildTwo