import React, { useContext, useEffect, useState } from 'react'
import { counterDecrement, counterIncrement, counterReset } from '../Action/CounterAction'
import { Failed, GetComment, GetCommentFailed, Success } from '../Action/DataAction'
import { CounterContext } from '../Context/CounterContext'
import axios from 'axios'

const ChildThree = () => {
    const ProviderData = useContext(CounterContext)
    const {count, data} = ProviderData
    const {countValue, dispatch:countDispatch} = count
    const {dataValue, dispatch:dataFetchDispatch} = data
    const {isLoading, isError, errorMessage, data:fetchData, comment} = dataValue
    const [commentCounter, setCommentCounter] = useState(0)
    console.log(`I am render from child three`);
    useEffect(() => {
        const fetchFunc = async () => {
            try{
                const getData = await axios.get("https://jsonplaceholder.typicode.com/posts/15")
                const {data, status} = getData
                if(data) {
                    dataFetchDispatch(Success(data))
                }else {
                    dataFetchDispatch( Failed() )
                }
            }catch{
                dataFetchDispatch( Failed() )
            }
        }
        fetchFunc()
        return  () => {

        }
    }, [])

    // clickHandler
    const clickHandler = async () => {
        try{
            if(commentCounter < 0) {
                setCommentCounter(0)
            }
            setCommentCounter(commentCounter + 1)
        }catch(err) {
            console.log(err);
        }
    }

    console.log(ProviderData);

    useEffect(() => {
        const getComment = async () => {
            try{
                const getComment = await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${commentCounter}`)
                // console.log("hello");
                dataFetchDispatch(GetComment(getComment.data))
            }catch(err){
                dataFetchDispatch(GetCommentFailed())
            }
        }
        getComment()
    }, [commentCounter])

    return (
        <div>
            {
                !isLoading
                ?
                <>
                    <h1>Hello I am from Child Three</h1>
                    <button onClick={(e) => countDispatch(counterIncrement(5))} >Increment</button>
                    <button onClick={(e) => countDispatch(counterDecrement(5))} >Decrement</button>
                    <button onClick={(e) => countDispatch(counterReset())} >Increment</button>
                    <h1>{fetchData.title}</h1>
                    <button onClick = {(e) => clickHandler(e)}>CLick Me to see the comment </button>
                    {
                        commentCounter > 0 &&
                        <div>
                            <h1>Comment post id: {commentCounter}</h1>
                            {
                                comment.map(data => {
                                    return (
                                        <>
                                            <h2 key = {data.id}>Id:{data.id} =====>> {data.body} </h2>
                                        </>
                                    )
                                })
                            }
                        </div>
                    }
                    
                </>
                :
                <h1>Loading</h1>
            }
        </div>
    )
}

export default ChildThree