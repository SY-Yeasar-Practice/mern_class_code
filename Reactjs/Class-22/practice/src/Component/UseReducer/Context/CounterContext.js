import React from 'react'

const CounterContext = React.createContext()
const CounterProvider = CounterContext.Provider

export {
    CounterContext,
    CounterProvider
}