import React , {useReducer} from 'react'
import { counterDecrement, counterIncrement, counterReset } from '../Action/CounterAction'
import ChildOne from '../Child/ChildOne'
import { CounterProvider } from '../Context/CounterContext'
import { reducer , initialState } from '../Reducer/CounterReducer'
import { DataReducer , initial} from '../Reducer/DataReducer'


const Parent = () => {
    const  [newCount, counterDispatch] = useReducer(reducer, initialState)
    const [newData, dataDispatch] = useReducer(DataReducer, initial)
    const sentData = {
        count: {
            countValue: newCount,
            dispatch: counterDispatch
        },
        data: {
            dataValue: newData,
            dispatch: dataDispatch
        }
    } //set the data
    console.log(`I am render from Parent`);
    const {count, data} = sentData
    const {countValue, dispatch:countDispatch} = count
    const {count:newCountValue} = countValue
    const {dataValue, dispatch: dispatchData} = data
    const {isLoading,isError, errorMessage, data:fetchData} = dataValue
    return (
        <div>
            <CounterProvider value = {sentData}>
                <h1>Count: {newCountValue}</h1>
                <ChildOne/>
            </CounterProvider>
        </div>
    )
}

export default Parent
