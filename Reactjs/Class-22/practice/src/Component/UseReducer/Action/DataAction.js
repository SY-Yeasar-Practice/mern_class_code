const Success = (data) => {
    return {
        type: "Success",
        payLoad: data
    }
}

const Failed = (data) => {
    return {
        type: "Failed",
        payLoad: {}
    }
}

const GetComment = (data) => {
    return {
        type: "GetComment",
        payLoad: data
    }
}

const GetCommentFailed = () => {
    return {
        type: "GetCommentFailed"
    }
}

export {
    Success,
    Failed,
    GetComment,
    GetCommentFailed
}