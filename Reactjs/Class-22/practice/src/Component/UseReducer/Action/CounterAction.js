const counterIncrement = (data) => {
    return {
        type: "Increment",
        payload: data
    }
}
const counterDecrement = (data) => {
    return {
        type: "Decrement",
        payload: data
    }
}
const counterReset = () => {
    return {
        type: "Reset",
        payload: ""
    }
}

export { 
    counterIncrement,
    counterDecrement,
    counterReset
}