const incrementTwo = (data) => {
    return{
        type: "Increment",
        payLoad: data
    }
}
const decrementTwo = (data) => {
    return{
        type: "Decrement",
        payLoad: data
    }
}
const resetTwo = () => {
    return{
        type: "Reset"
    }
}

export {
    resetTwo,
    incrementTwo,
    decrementTwo
}