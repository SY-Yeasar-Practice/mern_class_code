const incrementAction = (data) => {
    return{
        type: "Increment",
        payLoad: data
    }
} 
const decrementAction = (data) => {
    return{
        type: "Decrement",
        payLoad: data
    }
} 

const resetAction = () => {
    return {
        type: "Reset"
    }
}
export {
    incrementAction,
    decrementAction,
    resetAction
}
