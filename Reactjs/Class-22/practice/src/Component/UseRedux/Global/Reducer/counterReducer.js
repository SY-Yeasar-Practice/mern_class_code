const initialState = {
    count: 0
}

const countReducer = (state = initialState, action) => {
    switch (action.type) {
        case "Increment" : {
            return {
                count: state.count + action.payLoad
            }
        }
        case "Decrement" : {
            if(state.count > 0) {
                return {
                    count: state.count - action.payLoad
                }
            }
        }
        case "Reset" : {
            return {
                count: 0
            }
        }
        default : return state
    }
}

export default countReducer