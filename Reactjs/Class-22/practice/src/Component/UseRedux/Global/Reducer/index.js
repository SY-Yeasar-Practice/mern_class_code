import countReducer from "./counterReducer";
import {combineReducers} from 'redux'
import counterTwoReducer from "./counterTwoReducer";

const rootReducer = combineReducers({
    countReducer,
    counterTwoReducer
})

export default rootReducer