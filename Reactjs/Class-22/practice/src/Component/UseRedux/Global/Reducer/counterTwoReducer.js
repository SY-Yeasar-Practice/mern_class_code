const initialState = {
    countTwo: 0
}
const counterTwoReducer = (state = initialState, action) => {
    switch (action.type) {
        case "Increment": {
            return{
                countTwo: state.countTwo + action.payLoad
                // countTwo: 100
            }
        }
        case "Decrement": {
            return{
                countTwo: state.countTwo - action.payLoad
                // countTwo: 15
            }
        }
        case "Reset" : {
            return{
                countTwo: 0
            }
        }
        default :{
            return state
        }
    }
}

export default counterTwoReducer