import { useSelector } from "react-redux"
import ChildOne from "../Child/ChildOne"

const Parent = () => {
    const firstCounter = useSelector((state) => state.countReducer) //get the data of first counter 
    const secondCounter = useSelector((state) => state.counterTwoReducer) //get the data of second counter
    return (
        <div>
            <h1>Count: {firstCounter.count}</h1>
            <h1>Count from Child two: {secondCounter.countTwo}</h1>
            <ChildOne/>
        </div>
    )
}

export default Parent
