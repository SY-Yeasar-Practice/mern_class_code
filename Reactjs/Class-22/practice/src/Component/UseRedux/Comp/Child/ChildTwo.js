import React from 'react'
import { useDispatch } from 'react-redux'
import ChildThree from './ChildThree'
import { decrementTwo, incrementTwo, resetTwo} from '../../Global/Action/counterTwoAction'

const ChildTwo = () => {
    const dispatch = useDispatch()
    return (
        <div>
            <h1>I am from Child Two</h1>
            <button onClick={(e) => dispatch(incrementTwo(1)) }>Increment</button>
            <button onClick={(e) => dispatch(decrementTwo(1)) } >Decrement</button>
            <button onClick={(e) => dispatch(resetTwo()) } >Reset</button>
            <h2>=======================================================</h2>
            <br/>
            <br/>
            <ChildThree/>
        </div>
    )
}

export default ChildTwo