import React from 'react'
import { useDispatch } from 'react-redux'
import { decrementAction, incrementAction, resetAction } from '../../Global/Action/counterAction'

const ChildThree = () => {
    const dispatch = useDispatch()
    return (
        <div>
            <button onClick={(e) =>dispatch(incrementAction(5))}>Increment</button>
            <button onClick={(e) => dispatch(decrementAction(5))}>Decrement</button>
            <button onClick={(e) => dispatch(resetAction())}>Reset</button>
        </div>
    )
}

export default ChildThree