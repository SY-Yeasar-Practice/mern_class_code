import logo from './logo.svg';
import './App.css';
import Home from './component/Home';
import OnOfComp from './OnOfComp';
import Input from './component/Input';

const App = () => {
  return (
    <div >
      <OnOfComp/>
      {/* <Home/> */}
      <Input/>
    </div>
  );
}

export default App;
