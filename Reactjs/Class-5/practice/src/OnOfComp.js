import React,{Component} from 'react'
import Button from './component/Button'

class OnOfComp extends Component {
    constructor(){
        super()
        this.state = {
            isOn: true
        }
    }
    OnOfHandler = () => {
        this.setState({
            isOn: !this.state.isOn
        })
        console.log(!this.state.isOn);
    }
    render() {
        return (
            <div>
                <h3>I am from On Of Component</h3>
                <Button OnOfHandler = {this.OnOfHandler} OnOfValue = {this.state.isOn}/>
            </div>
        )
    }
}

export default OnOfComp