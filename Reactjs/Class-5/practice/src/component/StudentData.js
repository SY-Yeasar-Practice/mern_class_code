import React from 'react'
import Table from './Table'
import "./css/data.css"
import {studentInfoThree, studentInfo, studentInfoTwo} from "./dataInput"

const DataTable = () => {
    return (
        <div style = {{display: "flex", justifyContent: "space-between", flexFlow: "row wrap"}}>   
             <div>
                 <Table data = {studentInfo}/>
                <br />
                <Table data = {studentInfoTwo}/>
                <br />
                <Table data = {studentInfoThree}/>
             </div>

             <div>
                 <Table data = {studentInfo}/>
                <br />
                <Table data = {studentInfoTwo}/>
                <br />
                <Table data = {studentInfoThree}/>
             </div>

             <div>
                 <Table data = {studentInfo}/>
                <br />
                <Table data = {studentInfoTwo}/>
                <br />
                <Table data = {studentInfoThree}/>
             </div>
        </div>
    )
}

const StudentData = () => {
    return(
        <div>
            {
                studentInfo.map((data, index) => {
                    const myClass = data.age <= 18 ? "less" : "greater"
                    return <div key = {index}>
                        <h1 >My name is {data.name}.I am <span className ={myClass} style = {{padding : "8px", borderRadius: "100%"}}>{data.age}</span> years old.I am a {data.nationality}.</h1>
                    </div>
                })
            }
        </div>
    )
}

export  {
    StudentData,
    DataTable
}
