import React from 'react'


function Button({OnOfHandler, OnOfValue}) {
    return (
        <div>
            <button onClick = {OnOfHandler}>{!!OnOfValue ? "On" : "Off"}</button>
        </div>
    )
}


export default Button

