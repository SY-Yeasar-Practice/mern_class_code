import React , {Component} from 'react'
import ClassButton from './ClassButton'
import "./css/data.css"

class ClassComp extends Component {
    constructor(){
        super()
        this.state = {
            count : 0,
            count2: 0
        }
    }

    counterHandler = () => {
        this.state.count < 5 && this.setState({
            count: this.state.count + 1
        })
    }
    counterHandlerTwo = () => {
        this.state.count2 < 10 && this.setState({
            count2: this.state.count2 + 1
        })
    }
    decreseCounterHandler = () => {
        this.state.count > 0  && this.setState({
            count: this.state.count - 1
        })
    }
    decreseCounterHandlerTwo = () => {
        this.state.count2 > 0  && this.setState({
            count2: this.state.count2 - 1
        })
    }

    render(){
        const countClass = (this.state.count % 2 == 0 ) ? "even" : "odd" 
        const countClassTwo = (this.state.count2 > 5 ) ? "danger" : "odd" 
        return(
            <div>
                <h1>Count: <span className = {countClass}>{this.state.count}</span></h1>
                <ClassButton countIncease = {this.counterHandler} countDecrease = {this.decreseCounterHandler} />
                <br />
                <br />
                <h1>Count: <span className = {countClassTwo}>{this.state.count2}</span></h1>
                <ClassButton countIncease = {this.counterHandlerTwo} countDecrease = {this.decreseCounterHandlerTwo} />
            </div>
        )
    }
}

//export part
export default ClassComp