import React from 'react'
import ClassComp from './ClassComp'
import {DataTable, StudentData} from './StudentData'
import Table from './Table'

const  Home = () => {
    return (
        <div style = {{display: "flex" , flexFlow: "column wrap"}}>
            <DataTable/>
            <StudentData/>
            <ClassComp/>
        </div>
    )
}

export default Home
