import React, {Component} from "react" 

class Input extends Component {
    constructor(){
        super()
        this.state = {
            form: 0
        }
    }

    createForm  = () => {
        console.log("hello");
        let a =  <div>
                    <label htmlFor="first_name">FirstName</label>
                    <input type="text" />
                    <br />
                    <label htmlFor="last_name">Last Name</label>
                    <input type="text" />
                    <br />
                    <label htmlFor="dateOfBirth">BirthDate</label>
                    <input type="date" />
                </div>
        this.setState({
            form: a
        })
    }

    render(){
        return(
            <div>
                <button onClick = {this.createForm}>Click Me</button>
            </div>
        )
    }
}

export default Input