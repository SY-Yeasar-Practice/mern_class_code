import React from 'react'
import "./css/Table.css"
import "./css/data.css"

const Table = ({data}) => {

    return (
        <div >
            <table style = {{border: "2px solid black", borderCollapse: "collapse", textAlign: "center"}} >
                <thead >
                    <tr >
                        <th style = {{border: "2px solid black"}} >Student Name</th>
                        <th style = {{border: "2px solid black"}}>Student Age</th>
                        <th style = {{border: "2px solid black"}}>Student Nationatlity</th>
                    </tr>
                </thead>
                <tbody>
                     {
                        data.map((tableData, index) => {
                            // const myClass = (tableData.age) <= 18 ? "less" : "greater"
                            let myClass
                            if(tableData.age <= 18 && tableData.nationality !== "Bangladeshi") myClass = "less"
                            else myClass = "greater"
                            return  <tr key = {index}>
                                        <td style = {{border: "2px solid black"}}>{tableData.name}</td>
                                        <td className = { myClass } style = {{border: "2px solid black"}}>{tableData.age}</td>
                                        <td style = {{border: "2px solid black"}}>{tableData.nationality}</td>
                                    </tr>
                                })
                     }
                </tbody>
            </table>
        </div>
    )
}


// const Table = ({elementOne, elementTwo, elementThree}) => {
//     return (
//         <div>
//            <table>
//                <thead>
//                    <tr>
//                        <th>Student Name</th>
//                        <th>Student Age</th>
//                        <th>Student Nationality</th>
//                    </tr>
//                </thead>
//                <tbody>
//                    <tr>
//                        <td>{elementOne}</td>
//                        <td>{elementTwo}</td>
//                        <td>{elementThree}</td>
//                    </tr>
//                </tbody>
//            </table>
//         </div>
//     )
// }

export default Table

