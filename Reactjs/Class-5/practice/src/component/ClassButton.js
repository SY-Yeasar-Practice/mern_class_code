import React from 'react'

const ClassButton = ({countIncease, countDecrease}) => {
    return(
        <div>
            <button onClick = {countIncease} >Increase One</button>
            <button onClick = {countDecrease} >Decrese One</button>
        </div>
    )
}

//export part
export default ClassButton