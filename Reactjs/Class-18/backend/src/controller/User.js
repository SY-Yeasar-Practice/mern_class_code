const User = require("../model/User")
const Order = require("../model/Order")
const bcrypt = require("bcrypt")
const {singleImageUploader} = require("../../utils/uploadSingleFile")
const jwt = require("jsonwebtoken")
const jwtSecurityCode = process.env.JWT_Code //get the jwt secured code
const fs = require("fs")

//create user
const registrationUserController = async (req, res) => {
    try{
        const {password}  = req.body //get the data from body
        const {profileImage} = req.body.personalInfo
        const acceptedFormat = ["jpg", "jpeg", "png"]
        //upload the image 
        const {file, size} = profileImage //get the data from profileImage
        const editFileData = {
            base64: file,
            size
        } //change the property name accroding to the function requerment
        const {fileAddStatus, extensionValidation, fileUrl} = singleImageUploader(editFileData, acceptedFormat)
        if(extensionValidation){
            if(fileAddStatus){
                const imageLink = fileUrl //store the file url
                const hashedPassword = await bcrypt.hash(password, 10) //hashed the password
                if(hashedPassword){
                    const createUser = new User({
                        ...req.body,
                        password: hashedPassword,
                        "personalInfo.profileImage": imageLink
                    }) //create the user
                    const saveUser = await createUser.save()
                    if(saveUser){
                        res.status(201).json({
                            message: "User has been created",
                            saveUser
                        })
                    }else{
                        res.status(400).json({
                            message: "User not created"
                        })
                    }
                }else{
                    res.status(406).json({
                        message: "Password Hashing problem"
                    })
                }
            }else{
                res.status(406).json({
                    message: "file add failed"
                })
            }
        }else{
            res.status(406).json({
                message: "only jpg jpeg and png are allowed "
            })
        }
    }catch(err) {
        res.status(400).json({
            err
        })
        console.log(err);
    }
}

//login controller 
const loginController = async (req, res) => {
    try{
        const {email, password:inputPassword} = req.body //get the data from body
        const isValidUser = await User.findOne({"personalInfo.email": email}) //find the user
        if(isValidUser){
            const validUser = isValidUser
            const {password:dataBasePassword} = validUser
            const isValidPassword = await bcrypt.compare(inputPassword, dataBasePassword) //check is it a valid password or not
            if(isValidPassword) {
                const {_id, usertype, email} = validUser   
                const data = {
                    id: _id,
                    usertype,
                    email
                }
                const token = jwt.sign(data, jwtSecurityCode) //create the token
                res.status(202).json({
                    message: "Login successfully",
                    token
                })
            }else{
                res.status(404).json({
                    message: "Password not match"
                })
            }

        }else{
            res.status(404).json({
                message: "User not found"
            })
        }

    }catch(err){
        console.log(err);
        res.status(400).json({
            err
        })
    }
}

//see won profile
const showOwnProfileController = async (req, res) => {
    try{
        const token = req.header("Authorization") //get the token from header
        console.log(token);
        const tokenData = await jwt.decode(token, jwtSecurityCode) //get the token data
        console.log(token);
        const {id} = tokenData //get the id from token
        const isValidUser = await User.findOne({_id: id})
        if(isValidUser) {
            const {firstName, lastName} = isValidUser.personalInfo
            res.status(200).json({
                message: `${firstName} ${lastName} found`,
                isValidUser
            })
        }else{
            res.status(406).json({
                message: "User not found"
            })
        }
    }catch(err) {
        res.status(406).json({
            err
        })
        console.log(err);
    }
}

//create a order 
const createOrderSchema = async (req, res) => {
    try{
        const token = req.header('Authorization') //get the token from header
        const tokenData = await jwt.verify(token, jwtSecurityCode) //get the data from token data
        const {id} = tokenData
        console.log(id);
        const createOrder = new Order({
            ...req.body,
            user: id
        })
        const saveOrder = await createOrder.save()
        if(saveOrder) {
            res.status(201).json({
                message: "Order hasbeen create",
                order: saveOrder
            })
        }else{
            res.status(400).json({
                message: "Order saved failed"
            })
        }
    }catch(err) {
        console.log(err);
    }
}

//find the user from order
const findUserFromOrderController = async (req, res) => {
    try{
        const {orderId} = req.body
        const findOrder = await Order.findOne({orderId}).populate("user", "personalInfo.profileImage")
        console.log(findOrder);
    }catch(err)  {
        res.status(400).json({
            err
        })
        console.log(err);
    }
}

//update profile picture
const updateProfilePictureController = async (req, res) => {
    try{
        const token = req.header("Authorization")
        const data = await jwt.verify(token, jwtSecurityCode) //get the token data
        const {id} = data //get the id from token
        const {base64, size} = req.body.profileImage //get the image file from the
        const file = {base64, size}
        if(file) {
            const uploadTheImage = singleImageUploader(file, ["jpg", "jpeg", "png"]) 
            const {fileAddStatus, extensionValidation, fileUrl} = uploadTheImage
            if(fileAddStatus && extensionValidation) {
                const findUser = await User.findOne({_id: id }) //get the user by token
                if(findUser) {
                    const user = findUser //store the use here
                    const {profileImage:oldProfileImage} = user.personalInfo //get the previous image
                    const newProfileImage = fileUrl //get the new image link
                    const updateUserProfile = await User.updateOne(
                        {
                            _id: id
                        }, //querry
                        {
                            $set: {
                                "personalInfo.profileImage": newProfileImage
                            }
                        }, //update
                        {multi:true} //option
                    )
                    if(updateUserProfile.nModified !== 0) {
                        res.status(202).json({
                            message: "Profile Picture has been changed",
                            updateStatus: true
                        })

                        // delete current picture
                        const imageFile = oldProfileImage.split("/")
                        const imageName = imageFile[imageFile.length - 1] //get the image name
                        const route = `E:/Topper/MERN/Class/mern_class_code/Reactjs/Class-18/backend/public/${imageName}`
                        fs.unlink(
                            route,
                            (err) => {
                                if(err) {

                                    console.log(err);
                                    throw err
                                }else{
                                    console.log(`Image has been deleted`);
                                }
                            }
                        ) //delete the file 
                    }else {
                        res.status(400).json({
                            message: "Profile Upload failed",
                            updateStatus: false
                        })
                    }
                }else{
                    res.status(404).json({
                        message: "User not found",
                        updateStatus: false
                    })
                }
            }
        }else{
            res.status(404).json({
                message: "Image file not found"
            })
        }
         
    }catch(err) {
        console.log(err);
    }
}

//export part
module.exports = {
    registrationUserController,
    loginController,
    showOwnProfileController,
    createOrderSchema,
    findUserFromOrderController,
    updateProfilePictureController
}
