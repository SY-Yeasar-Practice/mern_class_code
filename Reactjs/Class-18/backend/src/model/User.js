const mongoose = require("mongoose")
const Schema = mongoose.Schema

const userSchema = new Schema({
    personalInfo: {
        firstName: String,
        lastName: String,
        dateOfBirth: {
            type: Date,
            default: Date.now
        },
        sex: {
            type: String,
            enum: ["male", "female", "other"]
        },
        profileImage: {
            type: String,
            default:""
        },
        email: String,
        description: {
            type: String,
            default: ""
        }
    },
    userId: String,
    password: String,
    userType: String
})

module.exports = mongoose.model("User", userSchema)