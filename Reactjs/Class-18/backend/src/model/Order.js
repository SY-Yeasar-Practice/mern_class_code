const mongoose = require('mongoose');
const User = require('./User')
const Schema = mongoose.Schema

const OrderSchema = new Schema({
    orderId: String,
    orderDate: {
        type: Date,
        default: Date.now
    },
    user:{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

module.exports = mongoose.model("Order", OrderSchema)