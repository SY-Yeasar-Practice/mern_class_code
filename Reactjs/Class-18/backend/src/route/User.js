const express = require("express")
const route = express.Router()
const auth = require("../../middleware/authentication")
const {registrationUserController,
    loginController,
    showOwnProfileController,
    createOrderSchema,
    findUserFromOrderController,
    updateProfilePictureController} = require("../controller/User")

route.post(`/create`, registrationUserController )
route.post(`/login`, loginController )
route.get(`/show/profile`, showOwnProfileController )
route.post('/create/order', createOrderSchema)
route.post('/find/user/oder/id', findUserFromOrderController)
route.post('/update/profile', updateProfilePictureController)



//export part
module.exports = route