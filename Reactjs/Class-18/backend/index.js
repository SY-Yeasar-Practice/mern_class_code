const express = require("express")
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config()
const userRoute = require("./src/route/User")

//get the dot env file
const port = process.env.PORT || 8080  //get the port
const mongoUrl = process.env.mongoUrl //get  the mongo url

//create the server
app.listen(port, () => console.log(`Server is running on ${port} `))

//connect to the database
mongoose.connect(mongoUrl, {useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true})
.then(() => {
    console.log(`Server is connected to the database`);
})
.catch(err => {
    console.log(err);
})

//middleware part
app.use(express.json({limit: "250mb"}))
app.use(express.urlencoded({extended: true, limit: "250mb"}))
app.use(cors()) //set the cors for valid a http request
app.use(express.static('public')) //set the static folder

//root page
app.get("/", (req,res) => {
    res.send("<h1>Hello I am from root</h1>")
})

//other root
app.use("/user", userRoute)

app.get("*", (req, res) => {
    res.send("<h1>404 Page not found</h1>")
})
