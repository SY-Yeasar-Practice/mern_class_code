const jwt = require("jsonwebtoken")
const jwtCode = process.env.JWT_Code //get the secrete code of jwt

const authMiddleware = (req, res, next) => {
    const token = req.header("Authorization") //get the data from header
    if(token) {
        const isValidToken = jwt.verify(token, jwtCode) //check that is it a valid token on not
        if(isValidToken) {
            const valid = isValidToken
            req.user = valid 
            next()
        }else{
            res.status(401).json({
                message: "Unauthorized user"
            })
        }
    }else{
        res.status(401).json({
            message: "Unauthorized user"
        })
    }
}

module.exports = authMiddleware