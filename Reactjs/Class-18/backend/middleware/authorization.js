const jwt = require("jsonwebtoken")
const securityCode = process.env.JWT_Code //get the jwt security code

const authorizationMiddleware = (providedRole) => {
    const role = providedRole //store the provided data
    return (req, res) => {
        const token = req.header("Authorization") //get the token from header
        if(!token){
            res.status(401).json({
                message: "Unauthorized User"
            })
        }else{
            const tokenData = jwt.verify(token, securityCode) //decode the token data
            const {userType} = tokenData //get the data from token
            const isPermitted = role.includes(userType) //it will give a boolean value
            if(isPermitted) {
                next()
            }else{
                res.status(403).json({
                    message: "Restricted not permitted"
                })
            }
        }
    }
} 

module.exports = authorizationMiddleware