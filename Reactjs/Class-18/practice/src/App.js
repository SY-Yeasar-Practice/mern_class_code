import logo from './logo.svg';
import './App.css';
// import CompOne from './Component/Main/CompOne';
// import Registration from './Component/Registration/Registration';
import Home from './Component/Home/Home';

function App() {
  return (
    <div className="container">
      {/* <CompOne/> */}
      {/* <Registration/> */}
      <Home/>
    </div>
  );
}

export default App;
