import React, { Component } from 'react'
import DefaultImage from '../assert/default.png'
import uploadStyle from './SingleImageUpload.module.css'


class SingleImageUploader extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            preview: DefaultImage
        }
    }
    
    // imageChangeHandler  
    imageChangeHandler = (e) => {
        e.preventDefault()
        const data = e.target.files[0] //store the data
        if(data) {
            const reader = new FileReader()
            reader.onloadend = () => {
                this.setState({
                    preview: reader.result //store the base 64 data
                },
                () => {
                    this.props.imageHandle(data) //pass the data as a props
                })
            }
            reader.readAsDataURL(data)
        }else{
            this.setState({
                preview: this.state.preview
            })
        }
        this.props.imageHandle(e.target.files[0])
    }

    // imageDeleteHandler 
    imageDeleteHandler = (e) => {
        e.preventDefault()
        this.setState({
            preview: DefaultImage
        })
    }

    render() {
        return (
            <React.Fragment>
                <div className="mb-3">
                    <div className="mb-3">
                        <input className="form-control" type="file" id="formFile" onChange = {(e) => this.imageChangeHandler(e)}/>
                    </div>
                    {/* privew part  */}
                    <div className = {`${uploadStyle.imageWrap}`}>
                        <img src= {this.state.preview} alt="image" style = {{height: "90px", width: "90px"}}/>
                        <i className= {`fas fa-times ${uploadStyle.crossButton} `} onClick = {(e) => this.imageDeleteHandler(e)} ></i>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}


export default  SingleImageUploader
