import React, { Component } from 'react'
import Registration from '../Registration/Registration'
import axios from 'axios'
import option from '../../config/option'
import HomePage from '../HomePage/HomePage'

class Login extends Component {
    constructor () {
        super()
        this.state = {
            registration: false,
            email: "",
            password: "",
            isLoggedIn: false,
            token: ""
        }
    }

    //register handler
    register = (e) => {
        e.preventDefault()
        const {registration} = this.state //get the data from state
        if(!!registration){
            this.setState({
                registration: false
            })
        }else{
            this.setState({
                registration: true
            })
        }
    }

    
    // loginController handler
    loginController = (e) => {
        e.preventDefault() //for ignore the auto load
        const {email, password} = this.state //get the data from state
        const data = {
            email,
            password
        }

        axios.post(`http://localhost:3030/user/login`, data, option)
        .then(response => {
            // console.log({responseToken : response.data.token});
            const {token} = response.data //get the token from response
            this.setState({
                token
            })
            localStorage.setItem("loginToken", token )
            this.setState({
                isLoggedIn: true,
            })
            // console.log(option);
            // this.setState({
            //     isLoggedIn: true,
            // })
        })
        .catch(err => {
            console.log(err);
        })

    }
    // loginInputHandler 
    loginInputHandler = (e) => {
        e.preventDefault() 
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    componentDidUpdate() {
        // console.log(`I am from component did update`);
        // if(this.state.token) {
        //     localStorage.removeItem("loginToken")
        //     localStorage.setItem("loginToken", this.state.token )
        //     // this.render()
        // }
        // console.log(this.state.token);
        // console.log(option);
        if(this.state.token && this.state.isLoggedIn) {
            this.render()
        }
    }
    render() {
        // console.log("I am renderd");
        return (
            <div className = {`mb-4`}>
                {
                    !!this.state.registration ?
                    <Registration/>
                    :
                    <>
                        {
                            !!this.state.isLoggedIn 
                            ?
                            <HomePage/>
                            :
                            <>
                                <h1>Login</h1>
                                <form>
                                    <div className="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name = "email" value = {this.state.email} onChange = {(e) => this.loginInputHandler(e)} />
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" value = {this.state.password} onChange = {(e) => this.loginInputHandler(e)}/>
                                    </div>
                                    <a href="#">Forgot password </a>
                                    <a href="#" onClick = {(e) => this.register(e)}>Register</a>
                                    <br />
                                    <button type="submit" className="btn btn-primary" onClick = {(e) => this.loginController(e)}>Login</button>
                                </form>
                            </>
                        }
                    </>
                }
                {
                    !!this.state.registration && <button className = {`mt-3 text-center btn-primary`} onClick = {(e) => this.register(e)}>Back</button>
                }
                
            </div>
        )
    }
}

export default  Login