import React, { Component } from 'react'
import SingleImageUploader from '../../utils/SingleImageUploader'
import axios from 'axios'

class Registration extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            firstName: "",
            lastName: "",
            dateOfBirth: "",
            userId: "",
            password: "",
            imageData: "",
            sex: "",
            email: "",
            userType: "",
            isSubmit : false
        }
    }
    
    // formChangeHandle
    formChangeHandle = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    
    //submit handler 
    submitHandler = (e) => {
        e.preventDefault()
        const {firstName, lastName, sex, dateOfBirth, imageData, email, userId, password, userType} = this.state //get the data from state store data
        const sentData = {
            personalInfo: {
                firstName,
                lastName,
                dateOfBirth,
                sex,
                profileImage: imageData,
                email

            },
            userId,
            password,
            userType
        }
        console.log(sentData);
       
        //sent the data into server
        axios.post("http://localhost:3030/user/create", sentData)
        .then(response => {
            console.log(response.status)
             if(response.status == 201) {
                 this.setState({
                    isSubmit: true
                })
             }
        })
        .catch(err => {
            console.log(err)
        })
    }

    // imageUpload 
    imageUpload = (file) =>  {
        const myFile = file 
        const {size} = myFile
        const reader = new FileReader() 
        reader.onloadend = () => {
            this.setState({
                imageData:{
                    file: reader.result,
                    size
                }
            })
        }
        reader.readAsDataURL(myFile)
    }

    handler = (e) => {
        e.preventDefault()
        this.setState({
            random : 5
        })
    }
    render() {
        // console.log(`I am rerender`);
        console.log(this.state.userType);
        return (
            <div>
               <h3>Registration form</h3>
               {/* form part  */}
               <div>
                    <form>
                        {/* first name  */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">First Name</label>
                            <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value = {this.state.firstName} name = "firstName" onChange = {(e) => this.formChangeHandle(e)} placeholder = "first name"/>
                        </div>

                        {/* last name  */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Last Name</label>
                            <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" 
                            value = {this.state.lastName} 
                            name = "lastName" 
                            onChange = {(e) => this.formChangeHandle(e)} 
                            placeholder = "last name"/>
                        </div>

                        {/* user id  */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">User Id</label>
                            <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value = {this.state.userId} name = "userId" onChange = {(e) => this.formChangeHandle(e)} placeholder = "user Id"/>
                        </div>

                        {/* user type  */}
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">User Type</label>
                            <select  class="form-control" id="exampleFormControlSelect2" name = "userType" onChange = {(e) => this.formChangeHandle(e)}>
                                <option 
                                value = "student"
                                >Student</option>

                                <option 
                                value = "teacher" 
                                
                                >Teacher</option>
                                
                                <option 
                                value = "admin" 
                                
                                >Admin</option>
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Email</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value = {this.state.email} name = "email" onChange = {(e) => this.formChangeHandle(e)} placeholder = "email"/>
                        </div>

                        {/* password  */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1" value = {this.state.password} placeholder = "password" name = "password" onChange = {(e) => this.formChangeHandle(e)}/>
                        </div>

                        {/* sex  */}
                        <label htmlFor="sex">Sex</label>
                        <div class="form-check mb-3">
                            {/* male  */}
                            <input class="form-check-input" type="radio" name="sex" id="flexRadioDefault1" value = "male" onChange = {(e) => this.formChangeHandle(e)}/>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Male
                            </label>
                            
                            <br />

                            {/* female  */}
                             <input class="form-check-input" type="radio" name="sex" id="flexRadioDefault2" value = "female" onChange = {(e) => this.formChangeHandle(e)}   />
                            <label class="form-check-label" for="flexRadioDefault2">
                                Female
                            </label>

                        </div>

                        {/* date of birth  */}
                        <div className = "mb-3">
                            <label for="date">Date of Birth</label> <br />
                            <input type="date" id="date"  onChange = {(e) => this.formChangeHandle(e)} name = "dateOfBirth" value = {this.state.dateOfBirth}/>
                        </div>
                        
                        {/* upload image  */}
                        <div className = "mb-3">
                            <label htmlFor="profileImage">Profile Pic</label>
                            <SingleImageUploader imageHandle = {this.imageUpload}/>
                        </div>
                        
                        <button type="submit" className="btn btn-primary" onClick = {(e) => this.submitHandler(e)}>{this.state.isSubmit ? "Submitted" : "Submit"}</button>
                    </form>
               </div>
            </div>
        )
    }
}


export default Registration