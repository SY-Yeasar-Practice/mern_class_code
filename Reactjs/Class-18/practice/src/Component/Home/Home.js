import React, { Component } from 'react'
import Registration from '../Registration/Registration.js'
import Login from '../Login/Login'
import option from '../../config/option.js'
import HomePage from '../HomePage/HomePage.js'

class Home extends Component {
    constructor() {
        super()
        this.state = {
            isLoggedIn: false
        }
    }
    componentDidMount() {
        if(option.headers.Authorization) {
            this.setState({
                isLoggedIn: true
            })
        }
    }

    // logoutHandler 
    // logoutHandler = (e) => {
    //     e.preventDefault()
    //     localStorage.removeItem("loginToken")
    //     this.setState({
    //         isLoggedIn: false
    //     })
    // }
    render() {
        // console.log(this.state.loginRef);
        // console.log(this.state.isLoggedIn);
        return (
            <div>
                {/* {
                    !!this.state.isLoggedIn && <button onClick = {(e) => this.logoutHandler(e)} className = {`btn btn-primary`}>Logout</button>
                } */}
                {
                    // !!this.state.isLoggedIn
                    // ?
                    // <HomePage/>
                    // :
                    <Login />
                }
            </div>
        )
    }
}

export default Home
