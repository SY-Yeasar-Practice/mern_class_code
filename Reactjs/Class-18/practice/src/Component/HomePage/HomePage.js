import axios from 'axios'
import React, { Component } from 'react'
import option from '../../config/option'
import {UserProvider} from '../../Context/UserContext'
import Profile from '../Profile/Profile'


class HomePage extends Component {
    constructor() {
        super()
        this.state = {
            profileData: "",
            loggedOut: false
        }
    }
    componentDidMount() {
        // console.log({optionOfComponentDidMountOfHomePage: option});
        console.log(`Option from component did mount`);
        console.log(option);
        axios.get(`http://localhost:3030/user/show/profile`, option)
        .then(response => {
            const {data} = response //ge the data from response
            this.setState({
                profileData: data
            })
        })
        .catch(err => {
            console.log(err);
        }) 
    }
    componentDidUpdate() {
        // console.log({optionOfComponentDidMountOfHomePage: option});
        if(!this.state.isLoggedIn){
            this.render()
        }
    }
    
    // logoutHandler
    logoutHandler = (e) => {
        e.preventDefault();
        localStorage.removeItem("loginToken")
        this.setState({
            profileData: "",
            loggedOut: true
        })
    }

    render() {
        // console.log({optionOfRenderInHomePage: option})
        console.log(`Option of render `);
        console.log(option);
        return (
            <React.Fragment>
                {
                    !!this.state.profileData
                    ?
                    <UserProvider value = {this.state.profileData}>
                        <Profile/>
                        <button onClick = {(e) => this.logoutHandler(e)}>Logout</button>
                    </UserProvider>
                    :
                    <h1>Loading.....</h1>
                }
            </React.Fragment>
        )
    }
}

export default HomePage