import React, { Component } from 'react'
import { UserConsumer } from '../../Context/UserContext'

class Profile extends Component {
    // logoutHandler 
    // logoutHandler = (e) => {
    //     e.preventDefault()
    //     localStorage.removeItem("loginToken")
    // }

    render() {
        return (
            <React.Fragment>
                <UserConsumer>
                    {
                        (userValue) => {
                            const {firstName, lastName} = userValue.isValidUser.personalInfo //get the use data
                            return (
                                <>
                                    <h1>{`Welcome ${firstName} ${lastName}`}</h1>
                                    {/* <button onClick = {(e) => this.logoutHandler(e)}>Logout</button> */}
                                </>
                            )
                        }
                    }
                </UserConsumer>
            </React.Fragment>
        )
    }
}

export default Profile
