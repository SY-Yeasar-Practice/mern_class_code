import React, { Component } from 'react'
import { DataProvider } from '../Context/dataContext'
import CompTwo from './CompTwo'


class CompOne extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count: 0,
             name: "shopnil"
        }
    }
    
    increaserHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        const data = {
            ...this.state,
            countHandler : this.increaserHandler
        }
        return (
            <div>
               <DataProvider value = {data}>
                    <CompTwo/>
               </DataProvider>
            </div>
        )
    }
}

export default CompOne
