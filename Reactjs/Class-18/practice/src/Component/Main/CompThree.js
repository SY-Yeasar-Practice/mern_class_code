import React, { Component } from 'react'
import { DataConsumer } from '../Context/dataContext'

class CompThree extends Component {
    render() {
        return (
            <div>
               <DataConsumer>
                   {
                       (data) =>{
                           console.log(data)
                           return(
                                <>
                                    <h1>Count {data.count}</h1>
                                    <button onClick = {(e) => data.countHandler(e)}>Click me</button>
                                </>
                           )
                       }
                   }
               </DataConsumer>
            </div>
        )
    }
}

export default CompThree
