
let token = localStorage.getItem("loginToken") //get the token from login part
let option = {
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-type": "Application/json",
        "Authorization": token
    }
}
export default option