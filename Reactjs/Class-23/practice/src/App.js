import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle'
import {BrowserRouter as Router , Redirect, Route, Switch} from 'react-router-dom'
import LoginPage from './Component/Work/LoginPage/LoginPage';
import HomePage from './Component/Work/HomePage/HomePage';
import PageNotFound from './Component/Work/PageNotFound/PageNotFound';
import NavBar from './Component/Work/NavBar/NavBar';
import {useSelector} from 'react-redux'
import Profile from './Component/Work/Profile/Profile';

function App() {
  const {isLoggedIn} = useSelector(state => state.loginReducer)
  const {} = useSelector(state => state.userReducer)
   return (
    <div className="container">
      <Router>
        <NavBar/>
        <Switch>
          {/* <Route  exact path = {`/`} component = {LoginPage}/> */}
          <Route exact path = {`/`} component = {LoginPage} >
              {
                isLoggedIn
                ? 
                <Redirect to = {`/home`} />
                :
                <LoginPage/>
              }
          </Route>
          <Route exact path = {`/login`}  component = {LoginPage} >
            {
              !!isLoggedIn
              &&
              <Redirect to = {`/profile`}/>
            }
          </Route>
          <Route exact path = {`/home`} component = {HomePage} />
          <Route exact path = {`/profile`} component = {Profile}>
            {
              !isLoggedIn
              &&
              <Redirect to = {`/login`}/>
              
            }
          </Route>
          <Route component = {PageNotFound}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
