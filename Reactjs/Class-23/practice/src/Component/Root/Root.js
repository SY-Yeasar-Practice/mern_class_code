import React from 'react'
import App from '../../App'
import store from '../Global/store'
import { Provider } from 'react-redux'
import RootTwo from './rootTwo'



const Root = () => {
    const storeData = store //store the store here
    return (
        <div>
            <Provider store = {storeData}>
                <RootTwo/>
            </Provider>
        </div>
    )
}

export default Root
