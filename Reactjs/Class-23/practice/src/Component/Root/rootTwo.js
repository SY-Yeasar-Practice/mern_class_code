import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import App from '../../App'
import { checkedLoginFailed, checkedLoginSuccess } from '../Global/action/loginAction'
import store from '../Global/store'



const RootTwo = () => {
    const {isLoggedIn, isChecked, header} = useSelector(state => state.loginReducer) //get the login reducer function state valueEqual
    const dispatch = useDispatch()

    useEffect(() => {
        const token = localStorage.getItem('auth-token')
        console.log(token);
        if(token) {
            dispatch(checkedLoginSuccess(token)) //if there have some user then it will happen
        }else {
            dispatch(checkedLoginFailed())
        }
    }, []) //to check that is there have e loggedIn user or not 


    return (
        <div>
            {
                isLoggedIn || isChecked
                ?
                <App/>
                
                :
                <h1>Loading.........</h1>
            }
        </div>
    )
}

export default RootTwo
