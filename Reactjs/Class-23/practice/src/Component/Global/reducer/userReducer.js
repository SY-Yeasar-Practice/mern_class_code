const initialState = {
    userData: "",
    hasError: false,
    isLoading: true,
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GetUserSuccess" : {
            return {
                ...state,
                userData: action.payload,
                hasError: false,
                isLoading: false
            }
        }
        case "FailedToGetUser": {
            return {
                ...state,
                hasError: true,
                isLoading: true,
                userData: ""
            }
        }

        default :{
            return state
        }
    }
}

export default userReducer