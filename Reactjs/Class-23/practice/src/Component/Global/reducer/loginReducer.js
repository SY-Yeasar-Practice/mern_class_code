const initialState = {
    isLoggedIn : false,
    header: "",
    isChecked: false,
    isError: false,
    loginAttempt : false,
    isLoggedOut: false
}
const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'loginSuccess': {
            const setHeader = {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "Authorization": action.payload //get the token
                }
            }
            return{
                ...state,
                isLoggedIn: true,
                isChecked: true,
                header: setHeader,
                isError: false,
                loginAttempt: true
            } // return a new state
        }
        case 'loginFailed' : {
            return {
                ...state,
                isChecked: true,
                isLoggedIn: false,
                header: "",
                isError: true,
                loginAttempt: true
            }
        }

        case "CheckedLoginSuccess" : {
            const setHeader = {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "Authorization": action.payload //get the token
                }
            }
            return {
                ...state,
                isLoggedIn: true,
                isChecked: true,
                header: setHeader
            }
        }
        case "CheckLoginFailed" : {
            return {
                ...state,
                isLoggedIn: false,
                isChecked: true,
                header: ""
            }
        }
        case "LoginAttemptSuccess" : {
            return {
                ...state,
                loginAttempt: true
            }
        }
        case "LoginAttemptFailed": {
            return {
                ...state,
                loginAttempt: false
            }
        }
        case "LoggedOut" : {
            return {
                ...state,
                header: "",
                isLoggedIn: false,
                isLoggedOut: true
            }
        }
        default: {
            return state
        }
    }
}

export default loginReducer