import {combineReducers} from 'redux'
import loginReducer from './loginReducer'
import userReducer from './userReducer'

//create a root combine reducer 

const rootReducer = combineReducers({
    loginReducer,
    userReducer
})

export default rootReducer