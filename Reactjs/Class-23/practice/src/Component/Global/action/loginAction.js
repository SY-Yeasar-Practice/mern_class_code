const loginSuccessAction = (token) => {
    return {
        type: "loginSuccess",
        payload: token
    }
}
const loginFailedAction = () => {
    return {
        type: "loginFailed"
    }
} 

const checkedLoginSuccess = (token) => {
    return {
        type: "CheckedLoginSuccess",
        payload: token
    }
}

const checkedLoginFailed = () => {
    return {
        type: "CheckLoginFailed"
    }
}

const loggedOutAction = () => {
    return {
        type: "LoggedOut"
    }
}
// const loginAttemptSuccess = () => {
//     return {
//         type: "LoginAttemptSuccess"
//     }
// }

// const loginAttemptFailed = () => {
//     return {
//         type: "LoginAttemptFailed"
//     }
// }
export {
    loginSuccessAction,
    loginFailedAction,
    checkedLoginSuccess,
    checkedLoginFailed,
    // loginAttemptSuccess,
    // loginAttemptFailed
    loggedOutAction
}