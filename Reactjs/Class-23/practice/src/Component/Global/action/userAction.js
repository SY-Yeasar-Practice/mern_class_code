const getUserAction = (data) => {
    return {
        type: "GetUserSuccess",
        payload: data
    }
}

const failedToGetUser = () => {
    return {
        type: "FailedToGetUser"
    }
}
export {
    getUserAction,
    failedToGetUser
}