import React, {useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, Redirect, useHistory } from 'react-router-dom'
import { loggedOutAction } from '../../Global/action/loginAction'

const NavBar = () => {
    const [isLoading, setIsLoading] = useState(true)
    const {isLoggedIn} = useSelector(state => state.loginReducer) //get the login info from login reduce
    const {userData} = useSelector(state => state.userReducer) //get the loggedIn user Data
    // console.log(userData);
    console.log(isLoading);
    if(!isLoading) {
       var {firstName, lastName} = userData.personalInfo //get the personal info of the logged
       console.log(firstName , lastName );
    }
    const dispatch =  useDispatch()
    // loggedOutHandler
    const loggedOutHandler = async(e) => {
        e.preventDefault()
        try{
            localStorage.removeItem('auth-token')
            dispatch(loggedOutAction())
        }catch(err){
            console.log("Logged out error", err);
        }
    }   

    useEffect(() => {
        console.log(`I am rendered`);
        if(userData) {
            setIsLoading(false)
        }
    },[userData])
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    {/* logo */}
                    <Link to = {`/`} className="navbar-brand">Navbar</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                        <Link to = {`/home`} className="nav-link active" aria-current="page">Home</Link>
                        </li>
                        <li className="nav-item">
                        <Link to = {`/profile`} className="nav-link">Profile</Link>
                        </li>
                    </ul>
                    <span className="navbar-text">
                        {
                            !!isLoggedIn 
                            ?
                            <>
                                {
                                    !!isLoading ? "" : <span className="me-3">WellCome {firstName} {lastName}</span>
                                }
                                <span className = 'btn btn-primary' onClick={(e) => loggedOutHandler(e)}>LoggedOut</span>
                            </>
                            :
                            <Link to = {`/login`}>
                            <p className = 'btn btn-primary'>Login</p>
                            </Link>
                        }
                    </span>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default NavBar
