import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'
import { failedToGetUser, getUserAction } from '../../Global/action/userAction'
import ProfileStyle from './Profile.module.css'

const Profile = () => {
    const [isLoading, setIsLoading] = useState(false)
    const {userData} = useSelector(state => state.userReducer) //get the user data
    // console.log(userData)
    if(!!isLoading) {
        var {firstName, lastName, profileImage} = userData.personalInfo
    }
    // console.log(firstName, lastName);
    const {header} = useSelector(state => state.loginReducer)
    const dispatch = useDispatch()
    useEffect(() => {
        const getData = async() => {
            try{
                const userData = await axios.get(`http://localhost:3030/user/show/profile`, header)
                const {status, data} = userData
                if(status == 200) {
                    const {isValidUser:user} = data //store the user data
                    dispatch(getUserAction(user))
                    setIsLoading(true)
                }
            }catch(err) {
                dispatch(failedToGetUser())
            }
        }
        getData()
    }, []) //to get the user data
    return (
        <div>
            {
                isLoading
                ?
                <>
                    <h1>Hello My name is {firstName} {lastName}</h1>
                    <img src={`${profileImage}`} alt="profileImage" className = {`${ProfileStyle.profileImage}`} />
                </>
                :
                <h1>Loading......</h1>
            }
        </div>
    )
}

export default Profile
