import React, { useState } from 'react'
import axios from 'axios'
import {useDispatch, useSelector} from 'react-redux'
import { loginFailedAction, loginSuccessAction } from '../../Global/action/loginAction'

const LoginPage = () => {
    const [message, setMessage] = useState("")
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    })
    const dispatch = useDispatch()
    const {isError, isLoggedIn, loginAttempt} = useSelector(state => state.loginReducer) //get the login reducer data
    // submitHandler
    const submitHandler = async(e) => {
        try{
            e.preventDefault()
            const sentData = formData //get the form data here
            const isLoggedIn = await axios.post('http://localhost:3030/user/login', sentData ) //sent the request
            const {status, data} = isLoggedIn
            if(status == 202) {
                const {token, message} = data
                localStorage.setItem('auth-token', token) //set the token in to local storage
                dispatch(loginSuccessAction(token))
                setMessage(message)
            }
            
        }catch(err) {
            setMessage("Login Falied")
            dispatch(loginFailedAction())
        }
    }
    // console.log({isError,loginAttempt});
    return (
        <div>
            <form>
                {/* email address */}
                <div className="mb-3">
                    <label for="exampleInputEmail1" className="form-label">Email address</label>
                    <input 
                    type="email" 
                    className="form-control" 
                    id="exampleInputEmail1" 
                    aria-describedby="emailHelp" 
                    onChange = {(e) => setFormData({...formData, email: e.target.value })}
                    value = {formData.email}/>
                </div>

                {/* password */}
                <div className="mb-3">
                    <label for="exampleInputPassword1" className="form-label">Password</label>
                    <input 
                    type="password" 
                    className="form-control" 
                    id="exampleInputPassword1"
                    onChange = {(e) => setFormData({...formData,password: e.target.value })}
                    value = {formData.password}/>
                </div>
                
                {/* submit */}
                <button type="submit" 
                className="btn btn-primary"
                onClick = {(e) => submitHandler(e)}>Submit</button>
                {
                    loginAttempt ?
                    !isError 
                        ?
                        <p>{message}</p>
                        :
                        <p>Login Failed</p>
                    :
                    ""
                }
            </form>
        </div>
    )
}

export default LoginPage

