import React, {Component} from 'react'

class Binding extends Component{
    constructor(){
        super()
        this.state = {
            count: 0
        }

        // {/* //get the handling (this) technique Three 3 */}
        // this.increaseHandling = this.increaseHandling.bind(this)
    }

    // {/* get the handling (this) technique one two and three 1,2,3 */}
    // increaseHandling(){
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    
    // {/* get the handling (this) technique Four 4*/}
    increaseHandling = () => {
        this.setState({
            count : this.state.count + 1
        })
    }

    render(){
        const {count} = this.state
        return (
            <div>
                <h1>Count: {count}</h1>
 
                {/* get the handling (this) technique one 1 */}
                {/* <button onClick = {this.increaseHandling.bind(this)}>Increase One</button>  */}

                {/* //get the handling (this) technique Two 2 */}
                {/* <button onClick = {() => {this.increaseHandling()}}>Increase One</button>  */}

                {/* //get the handling (this) technique Three 3  preferable one*/}
                {/* <button onClick = {this.increaseHandling}>Increase One</button> */}

                 {/* //get the handling (this) technique Four 4  easy one*/ }
                 <button onClick = {this.increaseHandling}>Increase One</button>

            </div>
        )
    }
}

export default Binding