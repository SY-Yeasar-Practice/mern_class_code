import React, { Component } from 'react'
let i = 0
class Random extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             elementIndex : 0
        }
    }
    
    hello = () => {
        let arr = ["1st", "2nd", "3rd", "4rth", "5th", "6th"]
        console.log(arr[i]);
        this.setState({
            elementIndex: arr[i]
        })
        if(i > arr.length - 1){
            this.setState ({
                elementIndex: "Array is full"
            })
        }
        i++
    }
    render() {
        const {introduction, message} = this.props
        return (
            <div>
                <h1>Array Element: {this.state.elementIndex}</h1>
                <button onClick = {this.hello}>Click me</button>
                {/* <button onClick = {this.props.introduction}>Another</button> */}
                <button onClick= {introduction}>Hello</button>
                <h1>{message}</h1>
            </div>
        )
    }
}

export default Random