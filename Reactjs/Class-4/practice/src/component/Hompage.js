import React, { Component } from 'react'
import Random from './Random'

class Hompage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             message : 'Hello'
        }
    }
    
    introduction = () => {
        console.log("I am from Homepage component");
        this.setState({
            message: "I am from Homepage component"
        })
    }
    render() {
        return (
            <div>
                <Random  introduction = {this.introduction} message = {this.state.message}/>
            </div>
        )
    }
}

export default Hompage
