import React, { Component } from 'react'
import Homepage from './HomePage'
import Hompage from './Hompage'
import LogOut from './LogOut'
import Random from './Random'


class Parent extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count : 0,
             isLoggedIn: false
        }
    }
    increase = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    decrease = () => {
        if(this.state.count > 0){
            this.setState({
                count: this.state.count - 1
            })
        }else{
            return
        }
    }
    check = () => {
         const element = this.state.isLoggedIn ? <Homepage/> : <LogOut/>
         return element
    }
    render() {
        return (
             <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick = {this.check}>Click me</button>
                <Random/>
                <Hompage/>
             </div>
        )
    }
}

export default Parent

