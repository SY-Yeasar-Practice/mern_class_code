import React, { Component } from 'react'
import axios from 'axios'

class Form extends Component {
    constructor() {
        super()
        this.state = {
            division: [],
            district: [],
            subDistrict: [],
            postalCode: [],
            divisionSelectData : '',
            districtSelectData: '',
            subDistrictSelectData: '',
            postalCodeSelectData: ''
        }
    }

    // division handler 
    divisionHandler = (e) => {
        e.preventDefault()
        // console.log(e.target.value);
        const myCurrentDivisionId = e.target.value
        const findTheDivision = this.state.division.data.find(data => data.id == myCurrentDivisionId)
        const divisionName = findTheDivision.name //get the division name
        const id = myCurrentDivisionId //store the division id
        this.setState({
            divisionSelectData: divisionName
        })
        //get the district
        axios.get(`https://api.digitalpathshalabd.com/district/by-division/${id}`) //set the district data according to division
        .then(data => {
            this.setState({
                district: data
            })
        })
        .catch(err => {
            console.log(err);
        })
        
    }

    //district handler 
    districtHandler = (e) => {
        e.preventDefault()
        const myCurrentDistrictId = e.target.value
        const findTheDistrict = this.state.district.data.find(data => data.id == myCurrentDistrictId)
        const districtName = findTheDistrict.name //get the district name
        const id = myCurrentDistrictId //store the district id
        this.setState({
            districtSelectData: districtName
        })
        //get the sub District by district
        axios.get(`https://api.digitalpathshalabd.com/upazila/by-district/${id}`) //set the sub district data according to district
        .then(data => {
            this.setState({
                subDistrict: data
            })
        })
        .catch(err => {
            console.log(err);
        })

        //get the postal code by district
        axios.get(`https://api.digitalpathshalabd.com/postcode/by-district/${id}`)
        .then(data => {
            this.setState({
                postalCode: data
            })
        })
        .catch(err => {
            console.log(err);
        })
        
    }

    // postalCodeHandler 
    postalCodeHandler = (e) => {
        e.preventDefault()
        this.setState({
            postalCodeSelectData: e.target.value
        })
    }

    // submitHandler
    submitHandler = (e) => {
        e.preventDefault()
        const {districtSelectData, divisionSelectData, postalCodeSelectData} = this.state
        const data = {
            division: divisionSelectData,
            district: districtSelectData,
            postalCode: postalCodeSelectData
        }
        console.log(data);
    }

    componentDidMount() {
        axios.get('https://api.digitalpathshalabd.com/division/admin/all')
        .then(data => {
            this.setState({
                division: data
            })
        })
        .catch (err => {
            console.log(err);
        })
        
    }
    render() {
        return (
            <React.Fragment>
                    {
                        this.state.division.length == 0 ?
                        'Loading...': 
                        <React.Fragment>
                            <div class="form-group container">
                                {/* division  */}
                                <label for="division">Division</label>
                                <select class="form-control"  onChange = {(e) => this.divisionHandler(e)} >
                                        {
                                            this.state.division.data.map(data => {
                                                return(
                                                    <option value={data.id} key = {data.id}>{data.name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                    
                                    {/* district  */}
                                    <label for="district">District</label>
                                    <select class="form-control" id="exampleFormControlSelect1" onChange = {(e) => this.districtHandler(e)}>
                                        {
                                            this.state.district.length == 0 
                                            ?
                                            <option value=""></option>
                                            :
                                            this.state.district.data.map(data => {
                                                return(
                                                    <option value="" value = {data.id}>{data.name}</option>
                                                )
                                            })
                                        }
                                    </select>

                                     {/* sub district  */}
                                    <label for="subDistrict">Sub District</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        {
                                            this.state.subDistrict.length == 0 
                                            ?
                                            <option value=""></option>
                                            :
                                            this.state.subDistrict.data.map(data => {
                                                return(
                                                    <option value = {data.id}>{data.name}</option>
                                                )
                                            })
                                        }
                                    </select>

                                     {/* Postal Code  */}
                                    <label for="postalCode">Postal Code</label>
                                    <select class="form-control" id="exampleFormControlSelect1" onChange = {(e) => this.postalCodeHandler(e)}>
                                        {
                                            this.state.postalCode.length == 0 
                                            ?
                                            <option value=""></option>
                                            :
                                            this.state.postalCode.data.map(data => {
                                                return(
                                                    <option value = {data.postalCode}>{data.postCode}</option>
                                                )
                                            })
                                        }
                                    </select>
                                    <br />
                                    {/* button  */}
                                    <button type="submit" class="btn btn-primary" onClick = {(e) => this.submitHandler(e)}>Submit</button>
                            </div>
                        </React.Fragment>
                    }                    
            </React.Fragment>
        )
    }
}
export default Form