import logo from './logo.svg';
import './App.css';
// import InputAdmin from './Component/Form/InputAdmin';
import ImageUpload from './Component/ImageUpload/ImageUpload';

function App() {
  return (
    <div className="container mt-5">
      {/* <InputAdmin/> */}
      <ImageUpload/>
    </div>
  );
}

export default App;
