import React, { Component } from 'react'
import axios from 'axios'

class InputAdmin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            confirmPassword: '',
            dateOfBirth: '',
            permanentAddress: '',
            currentAddress: '',
            sex: '',
            proImg: null,
            degreeName: '',
            result: '',
            session: '',
            passingYear: '',
            mobileNo: '',
            userId: ''
        }
    }

    // changle handler 
    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    //image upload part
    imageUploadHandler = (e) => {
        this.setState ({
                [e.target.name] : e.target.files[0]
            })
    }
    
    //submit handler 
    submitHandler = (e) => {
        e.preventDefault()
        const {firstName, 
            lastName, 
            email, 
            password, 
            confirmPassword,
            dateOfBirth,
            permanentAddress,
            currentAddress,
            sex,
            proImg,
            degreeName,
            result,
            session,
            passingYear,
            mobileNo,
            userId} = this.state //get the data from state 
            
            const imageData = new FormData()
            imageData.append('proImg', proImg)

            const data = {
                proImg,
                personalInfo: {
                    name :{
                        FirstName: firstName,
                        LastName: lastName,
                    },
                    email,
                    dateOfBirth,
                    contact: {
                        permanentAddress,
                        currentAddress,
                        mobileNo
                    },
                    sex
                },
                academicInfo: {
                    degree: [
                        {
                            degreeName,
                            result,
                            season: session,
                            passingYears: passingYear
                        }
                    ]
                },
                password,
                retypePassword: confirmPassword,
                userId
            }
            const postApi = "http://localhost:3030/user/admin/register"
            axios.post(postApi, data)
            .then(data => {
                console.log(data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    render() {
        return (
            <React.Fragment>
                <form>
                    {/* first Name  */}
                    <div className="form-group">
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" className="form-control" id="firstName" aria-describedby="emailHelp" placeholder="Enter FirstName" name = "firstName"  onChange = {(e) => this.changeHandler(e)}></input>
                    </div>

                    {/* Last name  */}
                    <div className="form-group">
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" className="form-control" id="lastName" aria-describedby="emailHelp" placeholder="Enter LastName" name = "lastName" onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>

                    {/* Email  */}
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email" name = "email"  onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>
                    
                    {/* >User Id  */}
                    <div className="form-group">
                        <label htmlFor="lastName">User Id</label>
                        <input type="text" className="form-control" id="userId" aria-describedby="emailHelp" placeholder=">User Id" name = "userId" onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>

                     {/* Passwrod  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" onChange = {(e) => this.changeHandler(e)}></input>
                    </div>

                    {/* retype Password  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Confirm Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Confirm Password" name = "confirmPassword" onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>
                    
                    {/* date of birth  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Date of Birth</label>
                        <input type="date" className="form-control" id="exampleInputPassword1"  name = "dateOfBirth" onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>

                    {/* permanentAddress  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Permanent Address</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Permanent Address" name = "permanentAddress" onChange = {(e) => this.changeHandler(e)}  ></input>
                    </div>

                    {/* current address  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Current Address</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Current Address"  onChange = {(e) => this.changeHandler(e)} name = "currentAddress" ></input>
                    </div>

                    {/* mobile no  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Mobile no:</label>
                        <input type="tel" className="form-control" id="exampleInputPassword1" placeholder = "Mobile no"  onChange = {(e) => this.changeHandler(e)} name = "mobileNo" ></input>
                    </div>

                    {/* sex part  */}
                    <label htmlFor="Sex">Sex</label>
                    <div class="form-check" onChange = {(e) => this.changeHandler(e)} >
                        <input class="form-check-input" type="radio" name="sex" id="exampleRadios1" value="male"  ></input>
                        <label class="form-check-label" for="exampleRadios1">
                            Male
                        </label>

                        <br />
                        <input class="form-check-input" type="radio" name="sex" id="exampleRadios2" value="female" ></input>
                        <label class="form-check-label" for="exampleRadios2">
                            Female
                        </label>
                    </div>
                    

                    {/* upload profile image part  */}
                     <div class="form-group">
                        <label for="exampleFormControlFile1">Upload Profile Image</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name = "proImg" onChange = {(e) => {this.imageUploadHandler(e)}} ></input>
                    </div>
                    
                    {/* degree part  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Degree Name</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Degree Name" onChange = {(e) => this.changeHandler(e)} name = "degreeName" ></input>
                    </div>
                    
                    {/* result  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Result</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Result" onChange = {(e) => this.changeHandler(e)} name = "result" ></input>
                    </div>

                    {/* season  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Session</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Session"  onChange = {(e) => this.changeHandler(e)} name = "session" ></input>
                    </div>

                    {/* passingYears  */}
                     <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Passing Year</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder = "Passing Year" name = "passingYear" onChange = {(e) => this.changeHandler(e)} ></input>
                    </div>

                    <button type="submit" className="btn btn-primary" onClick = {(e) => this.submitHandler(e)}>Submit</button>
                </form>
            </React.Fragment>
        )
    }
}

export default InputAdmin