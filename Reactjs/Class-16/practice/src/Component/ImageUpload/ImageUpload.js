import React, { Component } from 'react'

class ImageUpload extends Component {
    constructor() {
        super()
        this.state = {
            image: ''
        }
    }

    //imageChangeHandler
    imageChangeHandler = (e) => {
        console.log(`file to upload ${e.target.files[0]}`);
        const file = e.target.files[0] 
        if(file){
            const reader = new FileReader()
            reader.onload = this.handelReaderLoaded.bind(this)
            reader.readAsBinaryString(file)

        }
    }

    handelReaderLoaded = (readerEvent) => {
        let binaryString = readerEvent.target.result
        this.setState({
            image: btoa(binaryString)
        })
    }
    render() {
        console.log(this.state.image);
        return (
            <React.Fragment>
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Upload file</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" onChange = {(e) =>this.imageChangeHandler(e)} name = "image"></input>
                    </div>
                </form>
            </React.Fragment>
        )
    }
}

export default ImageUpload
