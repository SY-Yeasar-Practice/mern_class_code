const express = require("express")
const app = express()
const mongoose = require("mongoose")
require('dotenv').config() 

//dot env file 
const PORT = process.env.PORT || 8080
const MongoUrl = process.env.URL

app.use(express.json({limit: "250mb"}))
app.use(express.urlencoded({extended: true}))

//create a server
app.listen(PORT, () => console.log(`server is running on ${PORT}`))

//connet to the database
mongoose.connect(MongoUrl,{
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => console.log(`Server is conneted to the database`))
.catch(err => console.log(err))



