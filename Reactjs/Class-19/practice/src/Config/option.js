let token = localStorage.getItem('auth-token')
let options = {
    headers:{
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "Authorization": token
    }
}

export default options