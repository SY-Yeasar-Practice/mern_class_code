import React, { Component } from 'react'
import { LoginDataConsumer } from '../../../Context/LoginData'
import ProfileStyle from './Profile.module.css'

class Profile extends Component {
    render() {
        return (
            <div>
                <LoginDataConsumer>
                    {
                        (loggedInData) => {
                            const myData = loggedInData //store the loggedInData
                            const {firstName, lastName, profileImage} = myData.personalInfo //distracture  data from provided one
                            console.log(profileImage);
                            return(
                                <>
                                    <h1>Well come {firstName} {lastName}</h1>
                                    <img src={profileImage} alt="" className= {`${ProfileStyle.proPic}`} />
                                </>
                            )
                        }
                    }
                </LoginDataConsumer>
            </div>
        )
    }
}

export default Profile