import React, { Component } from 'react'
import option from '../../../Config/option'
import LoginForm from '../Login/LoginForm'
import axios from 'axios'
import { LoginDataProvider } from '../../../Context/LoginData'
import Profile from '../Profile/Profile'

class HomePage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isLogeOut: false,
             userData: "" //store the profile user data here
        }
    }
    // logOutHandler
    logOutHandler = async (e) => {
        e.preventDefault()
        localStorage.removeItem("auth-token")
        this.setState({
            isLogeOut: true
        }) //just change the status of state and remove the token from localStorage
    }
    async componentDidMount() {
        const localStorageToken = localStorage.getItem("auth-token") //get the token from local storage
        const header = {
            headers:{
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Authorization": localStorageToken
            }
        } //set the header for sent the token in to the api
        const getUseData = await axios.get("http://localhost:3030/user/show/profile", header )
        if(getUseData.status == 200) {
                const {isValidUser:myUser} = getUseData.data
                this.setState({
                    userData: myUser
                })
            }else{
                this.setState({
                    userData: ""
                })
            }

    }
    render() {
        const {isLogeOut,userData} = this.state
        return (
            <div>
                {
                    //is you logged out then it will bring you to login form
                    isLogeOut
                    ?
                    <LoginForm/>
                    :
                    <>
                        {
                            //if got the user data then execute here
                            userData
                            ?
                            <LoginDataProvider value = {this.state.userData}>
                                <>
                                    <Profile/>  
                                    <button onClick = {(e) => this.logOutHandler(e)}>Logout</button>
                                </>
                            </LoginDataProvider> //provide all logged in user data
                            :
                            <h1>Loading</h1>
                        }
                    </>
                }
            </div>
        )
    }
}
export default  HomePage
