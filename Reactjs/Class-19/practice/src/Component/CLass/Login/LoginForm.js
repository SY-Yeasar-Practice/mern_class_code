import React, { Component } from 'react'
import option from '../../../Config/option'
import HomePage from '../HomePage/HomePage'
import axios from 'axios'


class LoginForm extends Component {
    constructor(props) {
        super()
        this.state = {
            isLoggedIn: false,
            formData : {
                email: "",
                password: ""
            },
        }
    }

    // loginInputHandler
    loginInputHandler = (e) => {
        e.preventDefault()
        this.setState({
            formData : {
                ...this.state.formData,
                [e.target.name]: e.target.value
            }
        })
    }

  
    loginController = async (e) => {
        e.preventDefault()
        const {email, password} = this.state.formData //get the data from form data
        const data = {
            email,
            password
        } //set the data according to the api
        const isLoggedIn = await axios.post("http://localhost:3030/user/login", data) //get the login response
        const {status} = isLoggedIn //get the data from api
        const {token, message} = isLoggedIn.data //get the data from api
        if(status == 202 && message == "Login successfully") { //if the status is true then it will exicute and set the login status false to true otherwise it will set the log in status to false
            localStorage.setItem("auth-token", token)
            // console.log(`token of login response ${token}`);
            this.setState({
                isLoggedIn: true
            })
        }else{
            this.setState({
                isLoggedIn: false
            })
        }
    }
    async componentDidMount() { //if you again load the page it will check that is it logged in or not if there have token in local storage it will login other wise show the login form
        const token = localStorage.getItem("auth-token")
        console.log(token);
        if(token){
                this.setState({
                    isLoggedIn: true
                })
        }
    }
    render() {
        const {isLoggedIn} = this.state //get the login value from state
        return (
            <div>
               {
                   //if you are logged in then it will show your profile
                    isLoggedIn 
                    ?
                    <HomePage/>
                    : 
                    <>
                        <h1>Login</h1>
                        <form>
                            <div className="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name = "email" value = {this.state.email} onChange = {(e) => this.loginInputHandler(e)} />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" value = {this.state.password} onChange = {(e) => this.loginInputHandler(e)}/>
                            </div>
                            <a href="#">Forgot password </a>
                            <a href="#" onClick = {(e) => this.register(e)}>Register</a>
                            <br />
                            <button type="submit" className="btn btn-primary" onClick = {(e) => this.loginController(e)}>Login</button>
                        </form>
                    </>
               }
            </div>
        )
    }
}


export default  LoginForm