import React, {useState} from 'react'
import Comp from '../Comp/Comp'

const CompTwo = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    return (
        <div>
            {
                isLoggedIn
                ?
                <Comp/>
                :
                <h1>Loading...</h1>
            }
            <button onClick={(e) => setIsLoggedIn(true)}>Click me </button>
        </div>
    )
}

export default CompTwo
