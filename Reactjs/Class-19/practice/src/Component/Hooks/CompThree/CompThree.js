import React, {useEffect, useState} from 'react'

const CompThree = () => {
    const [count, setCount] = useState(0)
    const mouserPosition = async(e) => {
        setCount(e.clientX)
    }
    useEffect(() => {
        // console.log(`I am updated`);
        setInterval(() => {
            setCount(count + 1)
            console.log(count);
        }, 2000)
    },[])
    return (
        <div>
            <h1>Count: {count}</h1>
            {/* <button onClick={(e) => setCount(count + 1)}>Click me</button> */}
        </div>
    )
}


export default CompThree
