import React, {useState, useEffect} from 'react'
import CompTwo from '../CompTwo/CompTwo'

const Comp = () => {
    const [formData, setformData] = useState(
        {
            firstName: "",
            lastName: "",
            email: "",
            password: ""
        }
    )
    // console.log(formData)
    const submitHandler = (e) => {
        e.preventDefault() //to ignore the auto load during click
        const {firstName, lastName, email, password} = formData //get the data from form data
        console.log(formData)
    }

    useEffect (async () => {
        console.log(`I am from component did mount`);
    }, [formData.firstName])
    return (
        <div>
            <form>
                {/* firsName */}
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="firstName" name = "firstName" value = {formData.firstName} onChange = {(e) => setformData({...formData, firstName: e.target.value})}/>
                </div>

                {/* last Name */}
                 <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="lastName" name = "lastName" value = {formData.lastName} onChange = {(e) => setformData({...formData, lastName: e.target.value})}/>
                </div>

                {/* email  */}
                 <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" name = "email" value = {formData.email} onChange = {(e) => setformData({...formData, email: e.target.value})}/>
                </div>

                {/* password  */}
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" value = {formData.password} onChange = {(e) => setformData({...formData, password: e.target.value})}/>
                </div>
                <button type="submit" class="btn btn-primary" onClick = {(e) => submitHandler(e)}>Submit</button>
            </form>
        </div>
    )
}

export default Comp