import React, {createContext} from 'react'

const LoginDataContext = createContext()
const LoginDataProvider = LoginDataContext.Provider
const LoginDataConsumer = LoginDataContext.Consumer

export {
    LoginDataProvider,
    LoginDataConsumer
}