import logo from './logo.svg';
import './App.css';
import {List, ListTwo} from './component/file/List';
import OBJ from './component/file/obj';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import ListModule from "../src/component/file/cssModuleTwo/ListModule"
function App() {
  return (
    <div>
      <List/>
      <OBJ/>
      <OBJ/>
      <ListModule/>
    </div>
  );
}

export default App;
