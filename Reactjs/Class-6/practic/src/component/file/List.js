import {studentInfo, studentInfoTwo , studentInfoThree} from "../file/Data"
import "../style/list.css"
import Element from "./cssModuleOne/Element"

const List = ({intro}) => {
    return(
        <div>
            {
                studentInfo.map((data, ind) => {
                    let nameClass = (ind %2 == 0) ? "odd": "even"
                    return (
                        <div  key = {ind} className = {`cover`} >
                            <h1 className = {nameClass}>{data.name}</h1>
                            <h2 className = {nameClass}>{data.nationality}</h2>
                        </div>
                    )
                })
            }
            <Element/>
        </div>
    )
}
const ListTwo = ({intro, name}) => {
    console.log(name);
    return(
        <div>
            <h1 className = "">My name is {intro()} </h1>
        </div>
    )
}
export {
    List,
    ListTwo
}