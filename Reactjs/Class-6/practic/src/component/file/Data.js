const studentInfoThree = [
    {
        name: "Tamim",
        age: 5,
        nationality: "Bangaldeshi"
    },
    {
        name: "Sifat",
        age: 28,
        nationality: "Bangaldeshi"
    },
    {
        name: "Mahin",
        age: 14,
        nationality: "Bangaldeshi"
    },
    {
        name: "Hasan",
        age: 19,
        nationality: "Bangaldeshi"
    }
]

const studentInfo = [
    {
        name: "Shopnil",
        age: 25,
        nationality: "Bangaldeshi"
    },
    {
        name: "Sidrat",
        age: 18,
        nationality: "Bangaldeshi"
    },
    {
        name: "Nafisa",
        age: 14,
        nationality: "Indian"
    },
    {
        name: "Niks",
        age: 25,
        nationality: "Bangaldeshi"
    },
]

const studentInfoTwo = [
    {
        name: "Sakib",
        age: 25,
        nationality: "Bangaldeshi"
    },
    {
        name: "Rifat",
        age: 18,
        nationality: "Indian"
    },
    {
        name: "Zahin",
        age: 24,
        nationality: "Bangaldeshi"
    },
    {
        name: "Niks",
        age: 25,
        nationality: "Bangaldeshi"
    }
]

export {
    studentInfoThree,
    studentInfo,
    studentInfoTwo
}