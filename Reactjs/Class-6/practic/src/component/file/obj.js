import React , {Component} from 'react'
import {List, ListTwo} from './List'

class OBJ extends Component {
    constructor(){
        super()
        this.state = {
            count : 0
        }
        this.name = "shopnil"
        this.introductionMine = () => {
            console.log(`My name is ${this.name}`);
        }
    }
    introduction = () => {
         return this.name
    }
    render(){
        return(
            <div>
                <ListTwo intro = {this.introduction} name = {"shopnil"}/>
                <button className = {`btn btn-danger`}>Hello</button>
                <h1>My name is {this.introductionMine}</h1>
            </div>
        )
    }
}

export default OBJ