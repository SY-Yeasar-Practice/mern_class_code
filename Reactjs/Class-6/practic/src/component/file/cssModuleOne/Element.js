import React from 'react'
import style from './Element.module.css'

Element = () => {
    return (
        <div className = {`bg-success ${style.containerBackground}`}>
            <h1 className = {`${style.text} ${style.background}  `}>Hi I am from Element Component</h1>
            <h1>hello</h1>
        </div>
    )
}

export default Element

