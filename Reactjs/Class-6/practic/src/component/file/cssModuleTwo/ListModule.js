import {studentInfo, studentInfoTwo , studentInfoThree} from "../Data.js"
import "../../style/list.css"
import style from "./ListModule.module.css"

const ListModule = ({intro}) => {
    return(
        <div >
           <div>
                {
                studentInfo.map((data, ind) => {
                    let nameClass = (ind %2 == 0) ? "odd": "even"
                    let mainDivClass = (ind % 2 == 0 ? "bgOdd" : "bgEven")
                    return (
                        <div  key = {ind} className = {`${style[mainDivClass]} `} >
                            <h1 className = {nameClass}>{data.name}</h1>
                            <h2 className = {nameClass}>{data.nationality}</h2>
                        </div>
                    )
                })
            }
           </div>
        </div>
    )
}

export default ListModule