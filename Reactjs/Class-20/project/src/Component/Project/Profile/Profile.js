import React, {useState, useEffect} from 'react'
import LoginForm from '../LoginForm/LoginForm'
import axios from 'axios'
import UpdateProPic from '../UpdateProPic/UpdateProPic'

const Profile = () => {
    const [userData, setUserData] = useState("") //for get the user data
    const [isLoggedOut, setLoggedOut] = useState(false) //set the logged out status
    // console.log(userData.personalInfo);
    if(userData) {
        var {firstName, lastName, profileImage} = userData.personalInfo //get the personal info 
    }
    
    //get the data
    useEffect(async () => {
        const token = localStorage.getItem('auth-token') //get the token from local storageSize
        const option = {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-type": "application/json",
                "Authorization": token
            }
        }
        // console.log(option)
        const response = await axios.get("http://localhost:3030/user/show/profile", option)
        // console.log(response)
        if(response) {
            const {status} = response //get the response status
            if(status == 200) {
                const {message,isValidUser } = response.data  //get the data from response
                const user = isValidUser
                setUserData(user) //set the user data
            }else{
                setUserData(false) //set the user data
            }
        }

    }, [])

    // loggedOutHandler
    const loggedOutHandler = (e) => {
        e.preventDefault()
        localStorage.removeItem('auth-token') //remove the token from local storage
        setLoggedOut(true)
    }
    return(
        !!isLoggedOut
        ?
        <LoginForm/>
        :
        !userData 
            ?
            <h1>Loading</h1>
            :
            <>
                <h1>Hello I am {firstName} {lastName}</h1>
                <h2>Profile Image:</h2>
                <img src={`${profileImage}`} alt="" style= {{height: "250px", width: "250px", display: "block"}} />
                <br />
                <button onClick = {(e) => loggedOutHandler(e)}>Logged out</button>
                <div className = {`mt-4`}>
                    <UpdateProPic/>
                </div>
            </>

    )
}

export default Profile
