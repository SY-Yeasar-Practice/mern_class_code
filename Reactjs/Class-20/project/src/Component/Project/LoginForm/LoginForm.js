import React, {useState, useEffect} from 'react'
import RegistrationForm from '../RegistrationForm.js/RegistrationForm'
import Profile from '../Profile/Profile'
import axios from 'axios'
import LoginFormStyle from './LoginForm.module.css'

const LoginForm = (props) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [isRegistered, setIsRegistered] = useState(false)
    const [loginData, setLoginData ] = useState({
        email: "",
        password: ""
    }) //state of login data
    const [response, setResponse] = useState(true)
    
    // submitHandler
    const submitHandler = async (e) => {
        try{
            e.preventDefault()
            const {email, password} = loginData //get the data from login
            const data = {
                email,
                password
            } //set the data as database
            const sendData = await axios.post('http://localhost:3030/user/login', data) //get the response from server
            if(sendData.status == 202 && sendData.data.message == 'Login successfully') {
                setIsLoggedIn(true)
                const {token} = sendData.data //get the token from responsesAreSame
                localStorage.setItem('auth-token', token)
            }
        }catch(err){
            console.log(`Submit handler of login error: ${err}`);
            setIsLoggedIn(false)
            setResponse(false)
            console.log("hello");
        }
    }
    // make logged in
    useEffect(() => {
        const token = localStorage.getItem('auth-token');
        if(token) {
            setIsLoggedIn(true)
        }
    }, [])
    // console.log(loginData);
    return (
        <React.Fragment>
            {
                isLoggedIn
                ?
               <Profile/>
                :
                isRegistered 
                    ?
                    <RegistrationForm/>
                    :
                    <form>
                        <div className="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" 
                            className="form-control" 
                            id="exampleInputEmail1" 
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                            name = "email"
                            value = {loginData.email}
                            onChange = {(e) => setLoginData({...loginData, email: e.target.value})}/>
                        </div>
                        <div className="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" 
                            className="form-control" 
                            id="exampleInputPassword1" 
                            placeholder="Password"
                            value = {loginData.password}
                            onChange = {(e) => setLoginData({...loginData, password: e.target.value})}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary mr-2" onClick={(e) => submitHandler(e)}>Submit</button>                    
                        <button type="button" className="btn btn-primary" onClick ={(e) => setIsRegistered(true)}  >Register</button>   
                        <p className = {response ? "" : LoginFormStyle.form} >{response ? "" : "Login failed!!"}</p>                 
                </form>
            }
        </React.Fragment>
    )
}


export default LoginForm
