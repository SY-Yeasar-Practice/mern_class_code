import React from 'react'
import SingleImageUpload from '../utils/SingleImageUpload'

function UpdateProPic() {
    return (
        <React.Fragment>
            <SingleImageUpload/>
        </React.Fragment>
    )
}

export default UpdateProPic
