import React , {useState, useEffect}from 'react'
import defaultImage from '../../../assert/default.png'
import style from './SingleImageUpload.module.css'

const SingleImageUpload = ({imageData}) => {
    const [defaultPhoto, setProfileImage] = useState(defaultImage)

    // imageHandler
    const imageHandler = async (e) => {
        try{
            e.preventDefault() 
            const data = e.target.files[0]
            if(data) {
                const reader = new FileReader()
                reader.onloadend = () => {
                    setProfileImage(reader.result)
                }
                reader.readAsDataURL(data)
                imageData(data)
            }
            
        }catch(err) {
            console.log(err);
        }
    }
    return (
        <React.Fragment>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile01" onChange={(e) => imageHandler(e)}/>
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                </div>
            </div>
            <div className = {`${style.photoWrap}`}>
                <img src= {defaultPhoto} alt="Profile Image" className = {`${style.image}`}/>
                <i className = {`fas fa-times ${style.cross}`} onClick = {(e) => setProfileImage(defaultImage)} ></i>
            </div>
        </React.Fragment>
    )
}

export default SingleImageUpload