import React, {useState, userEffect} from "react"
import SingleImageUpload from "../utils/SingleImageUpload"
import axios from 'axios'
import LoginForm from "../LoginForm/LoginForm"

const RegistrationForm = () => {
    const [registerForm, setRegisterForm] = useState({
        firstName: "",
        lastName: "",
        email: "",
        profileImage: "",
        sex: "male",
        dateOfBirth: "",
        password: "",
        userType: "admin",
        dateOfBirth: "",
        userId: "",
    })
    const [isSubmit, setSubmit] = useState(false)
    // console.log(registerForm);
    // imageUploadHandler
    const imageUploadHandler = async (data) => {
        try{
            const myData = data //get the data
            const {size} = myData
            if(myData) {
                const reader = new FileReader()
                reader.onloadend = () => {
                    setRegisterForm({
                        ...registerForm,
                        profileImage: {
                            file: reader.result,
                            size
                        }
                    }) //set the state value with base 64 data
                }
                reader.readAsDataURL(myData)
            }
        }catch(err){
            console.log(err);
        }
    }

    // submitHandler
    const submitHandler = async (e) => {
        try {
            e.preventDefault()
            const {firstName, lastName, email, profileImage, sex, dateOfBirth, userType, userId, password} = registerForm //get the data from register form
            // console.log(registerForm);
            const data = {
                personalInfo: {
                    firstName,
                    lastName,
                    email,
                    sex,
                    dateOfBirth,
                    profileImage
                },
                password,
                userType,
                userId
            }
            const sendData = await axios.post('http://localhost:3030/user/create', data)
            if(sendData.status == 201) {
                setSubmit(true)
            }else{
                setSubmit(false)
            }
             
        }catch(err){
            console.log("Submit error ", err);
        }
    }
    // sexHandler
    const sexHandler = (e) => {
        setRegisterForm({...registerForm, sex: e.target.value})
        console.log(e.target.value);
    }
    return (
        <React.Fragment>
            {
                isSubmit 
                ?
                <LoginForm/>
                :
                <form>
                    {/* firstName */}
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" 
                        class="form-control" 
                        id="exampleInputEmail1" 
                        aria-describedby="emailHelp" 
                        placeholder="First Name" 
                        value = {registerForm.firstName} 
                        onChange = {(e) => setRegisterForm({...registerForm, firstName: e.target.value})}/>
                    </div>
                    
                    {/* lastName */}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Last Name</label>
                        <input type="text" 
                        class="form-control" 
                        id="exampleInputPassword1" 
                        placeholder="Last Name"
                        value = {registerForm.lastName}
                        onChange = {(e) => setRegisterForm({...registerForm, lastName: e.target.value})}/>
                    </div>

                    {/* user type  */}
                    <div class="form-group">
                        <label for="exampleFormControlSelect2">User Type</label>
                        <select  class="form-control" 
                        id="exampleFormControlSelect2"
                        name = "userType" 
                        onChange = {(e) => setRegisterForm({...registerForm, userType: e.target.value})}
                        >
                            <option value = "student"
                            selected = {registerForm.userType == "student" }
                            >Student</option>

                            <option value = "teacher" 
                            selected = {registerForm.userType == "teacher" }
                            >Teacher</option>

                            <option value = "admin" 
                            selected = {registerForm.userType == "admin" }
                            >Admin</option>
                        </select>
                    </div>

                    {/* userId */}
                    <div class="form-group">
                        <label for="exampleInputEmail1">User Id </label>
                        <input type="text" 
                        class="form-control" 
                        id="exampleInputEmail1" 
                        aria-describedby="emailHelp" 
                        placeholder="User Id" 
                        value = {registerForm.userId} 
                        onChange = {(e) => setRegisterForm({...registerForm, userId: e.target.value})}/>
                    </div>

                    {/* dateOfBirth */}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Date of Birth</label>
                        <input type="date" 
                        class="form-control" 
                        id="exampleInputPassword1" 
                        value = {registerForm.dateOfBirth}
                        onChange = {(e) => setRegisterForm({...registerForm, dateOfBirth: e.target.value})}/>
                    </div>

                    {/* email */}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <input type="email" 
                        class="form-control" 
                        id="exampleInputPassword1" 
                        placeholder="Email"
                        value = {registerForm.email}
                        onChange = {(e) => setRegisterForm({...registerForm, email: e.target.value})}/>
                    </div>

                    {/* password */}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" 
                        class="form-control" 
                        id="exampleInputPassword1" 
                        placeholder="Password"
                        value = {registerForm.password}
                        onChange = {(e) => setRegisterForm({...registerForm, password: e.target.value})}/>
                    </div>

                    {/* sex */}
                    <label htmlFor="sex">Sex</label>
                    <div class="form-check">
                        <input class="form-check-input" 
                        type="radio" 
                        name="sex" 
                        id="flexRadioDefault1" 
                        value = "male"
                        checked = {registerForm.sex == "male"}
                        onChange = {(e) => sexHandler(e)} />
                        <label class="form-check-label" for="flexRadioDefault1">
                            Male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" 
                        type="radio" 
                        name="sex" 
                        id="flexRadioDefault2" 
                        checked = {registerForm.sex == "female"}
                        value = "female"
                        onChange = {(e) => sexHandler(e)} />
                        <label class="form-check-label" for="flexRadioDefault2">
                            Female
                        </label>
                    </div>
                    <SingleImageUpload imageData = {(data) => imageUploadHandler(data)}/>
                    <br />
                    <br />
                    <button type="submit" class="btn btn-primary" onClick = {(e) => submitHandler(e)}>Submit</button>
                </form>
            }
        </React.Fragment>
    )
}

export default RegistrationForm
