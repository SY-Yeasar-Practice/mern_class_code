import logo from './logo.svg';
import './App.css';
import LoginForm from './Component/Project/LoginForm/LoginForm';

function App() {
  return (
    <div className="container">
      <LoginForm/>
    </div>
  );
}

export default App;
