import React, { PureComponent } from 'react'
import ParentStyleSheet from "./style/style.module.css"

class Child extends PureComponent {
    // shouldComponentUpdate (nextProps){
    //     if(nextProps.count == this.props.count){
    //         return false
    //     }else{
    //         return true
    //     }
    // }
    render() {
        const {count, classNameOfDot, min, sec, hour} = this.props
        console.log("I am renderd from child");
        return (
            <>
                <div>
                    <h1>Count: {count}</h1>
                    <h1>Hello</h1>
                </div>
                <>
                    <h1>Current Time: 
                            <span>{hour}<span className = {`${ParentStyleSheet[classNameOfDot]}`}>:</span></span>
                            <span>{min}<span className = {`${ParentStyleSheet[classNameOfDot]}`} >:</span></span>
                            <span>{sec}</span>
                    </h1>
                </>
            </>
        )
    }
}

export default Child