import React, { PureComponent } from 'react'
import Child from './Child'
import ParentStyleSheet from "./style/style.module.css"


class Parent extends PureComponent {
    constructor(){
        super()
        this.state = {
            count: 0,
            hour: new Date().toLocaleString().split(",")[1].split(":")[0],
            min: new Date().toLocaleString().split(",")[1].split(":")[1],
            sec: new Date().toLocaleString().split(",")[1].split(":")[2],
            dot: true
        }
    }
    componentDidMount (){
        setInterval(() => {
            this.setState({
                count: new Date().toLocaleString().split(",")[1].split(":")[0],
                hour: new Date().toLocaleString().split(",")[1].split(":")[0],
                min: new Date().toLocaleString().split(",")[1].split(":")[1],
                sec: new Date().toLocaleString().split(",")[1].split(":")[2],
            })
        },1000)
        setInterval(() => {
            if(this.state.dot){
                this.setState({
                    dot: false
                })
            }else if(!this.state.dot ){
                this.setState({
                    dot: true
                })
            }
        }, 500)
    }
    // shouldComponentUpdate(nextProps, nextState){
        
    //     if(nextState.count == this.state.count){
    //         return false
    //     }else{
    //         return true
    //     }
    // }
    render() {
        const dotClassName = this.state.dot == true ? "on" : "off"
        console.log("renderd");
        return (
            <>
                {
                    this.state.count > 0 ? <h1>Count:{this.state.count}</h1> : <h1>Loading......</h1>
                }  
                <Child count = {this.state.count} classNameOfDot = {dotClassName} hour = {this.state.hour} sec = {this.state.sec} min = {this.state.min}/> 
                <>
                    <h1>Time: 
                        <span>{this.state.hour}<span className = {`${ParentStyleSheet[dotClassName]}`}>:</span></span>
                        <span>{this.state.min}<span className = {`${ParentStyleSheet[dotClassName]}`} >:</span></span>
                        <span>{this.state.sec}</span>
                    </h1>
                </>
            </>
        )
    }
}

export default Parent
