import logo from './logo.svg';
import './App.css';
import Parent from './comp/cycle/Parent';

function App() {
  return (
    <div className="">
      <Parent/>
    </div>
  );
}

export default App;
