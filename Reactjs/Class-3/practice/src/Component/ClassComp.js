import React, {Component} from 'react'

class ClassComp extends Component {
    constructor(){
        super()
        this.state = {
            count : 0
        } //this is the state value
    }

    indcrementFive(){
        const {count} = this.state
        this.setState({
            count: count + 5
        })
    } //increment value
    decrementFive(){
        const {count} = this.state
        if(count >= 5 ){
            this.setState({
                count: count - 5
            })
        }else{
            return
        }
    }

    incrementOne (){
        this.setState(previuosStateValue => ({
            count: previuosStateValue.count + 1
        }))
    }

    decrementOne (){
        if(this.state.count > 0){
            this.setState(previuosStateValue => ({
                count: previuosStateValue.count  -1
            }))
        }else{
            return
        }
    }
    
    indcrementFiveWithPreviousStateValue (){
        this.incrementOne()
        this.incrementOne()
        this.incrementOne()
        this.incrementOne()
        this.incrementOne()
    }
    decrementFiveWithPreviousStateValue (){
        this.decrementOne()
        this.decrementOne()
        this.decrementOne()
        this.decrementOne()
        this.decrementOne()
    }
    
    render(){
        const {count} = this.state
        return(
            <div>
                <h1>Count: {count}</h1>
                {/* <button onClick = {this.count.bind(this)}>Increment One</button> */}
                <button onClick = {this.indcrementFive.bind(this)}>Increment Five</button>
                <button onClick = {this.decrementFive.bind(this)}>Decrement Five</button>
                <button onClick = {this.indcrementFiveWithPreviousStateValue.bind(this)}>Increment Five by Previous state Value</button>
                <button onClick = {this.decrementFiveWithPreviousStateValue.bind(this)}>Decrement Five by Previous state Value</button>
            </div>
        )
    }
}


export default ClassComp