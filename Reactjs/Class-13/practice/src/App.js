import logo from './logo.svg';
import './App.css';
import ClickCounter from './component/ClickCounter/ClickCounter';
import HoverController from "./component/HoverController.js/HoverController"
import Data from "./component/StudentData/Data"
import ParentComp from './component/RenderProps/ParentComp';

function App() {
  return (
    <div className="App">
      {/* <ClickCounter name = "hello"/>
      <HoverController/> */}
      {/* <Data/> */}
      <ParentComp/>
    </div>
  );
}

export default App;
