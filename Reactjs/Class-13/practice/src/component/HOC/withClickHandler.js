import React, { Component } from 'react'

const withClickHandler = (ChildComp) => {
    class withClickHandlerClass extends Component {
        constructor() {
            super()
            this.state = {
                count: 0
            }
        }
        clickHandler = () => {
            this.setState({
                count: this.state.count + 1
            })
        }
        render() {
            return (
                <>
                    <ChildComp handler = {this.clickHandler} count = {this.state.count} {...this.props}/>
                </>
            )
        }
    }

    return withClickHandlerClass
    
}

export default withClickHandler