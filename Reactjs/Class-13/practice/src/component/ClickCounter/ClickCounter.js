import React, { Component } from 'react'
import withClickHandler from "../HOC/withClickHandler"

class ClickCounter extends Component {
    render() {
        const {handler, count} = this.props
        return (
            <>
                <button onClick = {handler}>Click Me {count} times</button>
            </>
        )
    }
}
export default withClickHandler(ClickCounter)

// export default ClickCounter
