import React, { Component } from 'react'

class ErrorBoundary extends Component {
    constructor() {
        super()
        this.state = {
            hasError: false
        }
    }
    static getDerivedStateFromError() {
        return {
            hasError: true
        }
    }
    render() {
        if(this.state.hasError){
            return <h1>There might have some error</h1>
        }else{
            return this.props.children
        }
    }
}

//export part
export default ErrorBoundary