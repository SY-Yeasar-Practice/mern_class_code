import React, { Component } from 'react'
import withClickHandler from '../HOC/withClickHandler.js'

class HoverController extends Component {
    render() {
        const {handler, count} = this.props
        return (
            <div>
                <button onMouseEnter = {handler}>Hover me {count} times</button>
            </div>
        )
    }
}

export default withClickHandler(HoverController)