import React, { Component } from 'react'

class RenderComp extends Component {
    constructor(){
        super()
        this.state = {
            count : 0
        }
    }
    incrementHandler = () => {
        this.setState ({
            count: this.state.count + 1
        })
    }
    render() {
        return (
            <div>
                {this.props.render(this.state.count, this.incrementHandler)}
            </div>
        )
    }
}

export default  RenderComp
