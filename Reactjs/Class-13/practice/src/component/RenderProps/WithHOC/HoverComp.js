import React, { Component } from 'react'
import withHandler from './withHandler'

class HoverComp extends Component {
    render() {
        return (
            <div>
                <h1 onMouseOut = {this.props.ClickHandler}>Hover me {this.props.count} times</h1>
            </div>
        )
    }
}

export default withHandler(HoverComp)
