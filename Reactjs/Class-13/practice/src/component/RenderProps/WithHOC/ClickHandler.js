import React, { Component } from 'react'
import withHandler from './withHandler'

class ClickHandler extends Component {
    render() {
        return (
            <div>
                <button onClick = {this.props.ClickHandler}>Click me {this.props.count}</button>
            </div>
        )
    }
}

export default withHandler(ClickHandler)