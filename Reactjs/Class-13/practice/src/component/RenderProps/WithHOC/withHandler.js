import React, {Component} from 'react'

const withHandler = (Comp) => {
    class HOCComponentClass extends Component {
        constructor(){
            super()
            this.state = {
                count : 0
            }
        }
        incrementHandler = () => {
            this.setState ({
                count: this.state.count + 1
            })
        }
        render() {
            return (
                <div>
                    <Comp count = {this.state.count} ClickHandler = {this.incrementHandler} {...this.props}/>
                </div>
            )
        }
    }
    
    return HOCComponentClass
}
export default withHandler