import React, { Component } from 'react'
import ClickHandler from './ClickHandler'
import HoverComp from './HoverComp'
import RenderComp from './RenderComp'
import ClickHandlerTwo from '../RenderProps/WithHOC/ClickHandler'
import HoverCompTwo from '../RenderProps/WithHOC/HoverComp'

class ParentComp extends Component {
    render() {
        return (
            <div>
                {/* with render props  */}
                <RenderComp render ={(count, handler) => (<ClickHandler count = {count} ClickHandler = {handler}/>)}/>
                <RenderComp render ={(count, handler) => (<HoverComp count = {count} ClickHandler = {handler}/>)}/>
                
                {/* with higher order component  */}
                <ClickHandlerTwo/>
                <HoverCompTwo/>
            </div>
        )
    }
}

export default  ParentComp