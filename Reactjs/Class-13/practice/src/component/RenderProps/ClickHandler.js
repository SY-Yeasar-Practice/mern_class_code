import React, { Component } from 'react'

class ClickHandler extends Component {
    render() {
        return (
            <div>
                <button onClick = {this.props.ClickHandler}>Click me {this.props.count}</button>
            </div>
        )
    }
}

export default ClickHandler