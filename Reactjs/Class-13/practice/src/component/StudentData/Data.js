import React, { Component } from 'react'
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary"
import ClickCounter from "../ClickCounter/ClickCounter"

 class Data extends Component {
    render() {
        return (
            <>
                <ErrorBoundary>
                      <ClickCounter/>
                </ErrorBoundary>
            </>
        )
    }
}
export default Data