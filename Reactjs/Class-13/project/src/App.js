import React, { Component } from 'react'
import './App.css';
import Home from "./Component/Home/Home"
import Loader from './Component/Loader/Loader';

class App extends Component {
  constructor() {
    super()
    this.state = {
      isLoad: false
    }
  }
  shouldComponentUpdate (nextState, nextProps) {
    if(nextProps.isLoad == this.state.isLoad){
      return false
    }else{
      return true
    }
  }

  componentDidMount () {
    setTimeout (() => {
      this.setState({
        isLoad: true
      })
    }, 1000)
  }
  render() {
    return (
      <div>
          {
            this.state.isLoad == true ? <Home/> : <Loader/>
          }
      </div>
    )
  }
}


export default App;
