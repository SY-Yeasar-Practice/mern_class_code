import React from 'react'
import CoverStyle from './CoverComp.module.css'

const CoverComp = ({personalInfo}) => {
    return (
        <div className = {`row`}>
            {/* description part  */}
            <div className = {`col-12 col-md-7 ${CoverStyle.coverWrapLeftSide}`}>
                <h1 className = {`${CoverStyle.coverNameTitle}`}>{personalInfo.firstName} {personalInfo.lastName}</h1> {/* Name of the user*/}

                {/* occupation part  */}
                <h5 className = {`${CoverStyle.coverOccupationPart}`}>{personalInfo.occupation}</h5>

                {/* description part  */}
                <p className = {`text-justify ${CoverStyle.coverDisPart}`}>I'm a {(personalInfo.occupation).toLowerCase()} specialised in frontend and backend development for complex scalable web apps.I write about software development on <a href="#" className = {`${CoverStyle.coverDisPartAnkorTag}`}>my blog</a>.Want to know how I may help your project?Check out my project <a href="#" className = {`${CoverStyle.coverDisPartAnkorTag}`} >portfolio</a> and <a href="#" className = {`${CoverStyle.coverDisPartAnkorTag}`} >online resume</a></p>

                {/* cover button one  View Portfolio */}
                <button className = {`btn ${CoverStyle.coverDisPartButton}  ${CoverStyle.coverDisButtonTextPart} ${CoverStyle.viewPortfolioButton} ${CoverStyle.buttonOutline} `}>
                    <i className={`fas fa-arrow-alt-circle-right ${CoverStyle.coverDisButtonFontAwesomeLogo}`}></i>
                    View Portfolio
                </button>

                {/* cover button two View Resume  */}
                <button className = {`btn ${CoverStyle.coverDisPartButton}  ${CoverStyle.coverDisButtonTextPart} ${CoverStyle.viewResumeButton} ${CoverStyle.buttonOutline}`} >
                    <i className={`far fa-file-alt ${CoverStyle.coverDisButtonFontAwesomeLogo} `}></i>
                    View Resume
                </button>
           </div>
           {/*cover image part  */}
           <div className = {`col-12 col-md-5 mt-4 mt-md-0`}>
                <img src={`image/${personalInfo.coverPic}`} alt="Cover Image" className = {`img-fluid h-100 w-100`} />
           </div>
        </div>
    )
}

export default CoverComp