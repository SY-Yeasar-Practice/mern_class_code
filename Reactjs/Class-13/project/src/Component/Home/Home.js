import React, { Component } from 'react'
import ProfileComp from "../ProfileComp/ProfileComp"
import Data from "../Data/data"
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import ProfileMenu from '../ProfileMenu/ProfileMenu'
import CoverComp from '../CoverComp/CoverComp'
import data from '../Data/data'
import SkillComp from '../SkillComp/SkillComp'
import HomeStyle from './Home.module.css'

class Home extends Component {
    render() {
        const menuItem =  ["About Me", "Portfolio", "Service & Pricing", "Resume", "Blog", "Contact", "More Page" ] //store the all menu items
        return (
            <div className = {`container-fluid`}>
               <div className = {`row`} style = {{height:'41rem'}}>
                   <div className = {`col-12 col-md-3 ${HomeStyle.background} p-4`}>
                        {/* left side part wrap */}
                        <div className = {`row`}>
                            {/* left side upper profile part  */}
                            <div  className = {`col-12 mb-3`} >
                                <ErrorBoundary>
                                    <ProfileComp {...Data}/>
                                </ErrorBoundary>
                            </div>
                            <div className = {`${HomeStyle.underlinePart}`}></div>
                            {/* left side down part wrap  */}
                            <div>
                                <ErrorBoundary>
                                    <ProfileMenu {...Data} menuItems = {menuItem}/>
                                </ErrorBoundary>
                            </div>
                            <div className = {`${HomeStyle.underlinePart} mt-3`}></div>
                        </div>
                   </div>
                   <div className = {`col-12 col-md-9`}>
                        {/* right side wrap  */}
                        <div className = {`container ${HomeStyle.rightSideWrap}`}>
                            {/* cover part  */}
                            <div>
                                <ErrorBoundary>
                                    <CoverComp {...Data}/>
                                </ErrorBoundary>
                            </div>

                            {/* What I fo part  */}
                            <div className = {`mt-5 mb-5`}>
                            <ErrorBoundary>
                                    <h2 className = {`${HomeStyle.whatIDoPart} h1 ${HomeStyle.dashPart}`}>What I do</h2>

                                    <p className = {`${HomeStyle.whatIDoPartParagraph}`}>I have more then {Data.workExperience.duration} {Data.workExperience.duration > 1 ? "year's" : "year"} experience building for clients all over the world. Below is a quick overview of my main technical skill sets and technologies I use. Want to find out more about my exprience? Check out my <a href="#" className = {`${HomeStyle.whatIDoPartParagraphAnkorPart}`}>online resume</a> and <a href="#" className = {`${HomeStyle.whatIDoPartParagraphAnkorPart}`} >project portfolio</a></p>
                            </ErrorBoundary>
                            </div>

                            {/* skill part  */}
                            <div>
                                <ErrorBoundary>
                                    <SkillComp {...Data}/>
                                </ErrorBoundary>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        )
    }
}

//export part
export default Home
