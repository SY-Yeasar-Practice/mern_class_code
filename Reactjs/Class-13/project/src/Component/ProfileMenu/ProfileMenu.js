import ProfileMenuStyle from './ProfileMenu.module.css'
import React, { Component } from 'react'

class ProfileMenu extends Component {
    constructor(){
        super()
        this.state = {
            active: "",
            index: 0
        }
    }

    menuItemsTag = (menuItems) =>  {
        const items = menuItems //store the parameter into another local variable
        let menuTag 
        switch (items) {
            case "About Me": {
                menuTag = "fas fa-address-book"
                break;
            }
            case "Portfolio": {
                menuTag = "fas fa-laptop-code"
                break;
            }
            case "Service & Pricing": {
                menuTag = "fas fa-briefcase"
                break;
            }
            case "Resume": {
                menuTag = "far fa-file-alt"
                break;
            }
            case "Blog": {
                menuTag = "fas fa-blog"
                break;
            }
            case "Contact": {
                menuTag = "fas fa-envelope-open"
                break;
            }
            case "More Page": {
                menuTag = "fas fa-cogs"
                break;
            }
        }
        return menuTag
    }
    activeHandler = (e, ind) => {
        e.preventDefault()
        this.setState({
            index: ind
        })
    }
    render() {
        return (
            <React.Fragment>
                <ul className = {``} >
                    {
                        this.props.menuItems.map((ele, ind) => {
                            const activeClass = this.state.index == ind ? ProfileMenuStyle.active: "" //active button part
                            return(
                                <li key = {ind} className = {`${ProfileMenuStyle.listItem}`} onClick = {(e) => this.activeHandler(e, ind)}>
                                    <i className={`${this.menuItemsTag(ele)} ${activeClass} ${ProfileMenuStyle.lisItemHover}`}></i>
                                    <a href="#" className = {`${ProfileMenuStyle.listAnkorTag} ${activeClass} ${ProfileMenuStyle.lisItemHover} `} >{ele}</a>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className = {`d-flex justify-content-center`}>
                    <button className = {`${ProfileMenuStyle.hireMeButton} btn`}><i className="far fa-paper-plane"></i> Hire Me</button>
                </div>
            </React.Fragment>
        )
    }
}


export default  ProfileMenu