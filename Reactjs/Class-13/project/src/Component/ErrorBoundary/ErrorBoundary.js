import React, { Component } from 'react'

class ErrorBoundary extends Component {
    constructor () {
        super()
        this.state = {
            hasError : false
        }
    }
    static getDerivedStateFromError (error) {
        return {
            hasError: true
        }
    }
    render() {
       const errorExist = !!this.state.hasError ? "There might have some error" : this.props.children
       return errorExist
    }
}

export default ErrorBoundary
