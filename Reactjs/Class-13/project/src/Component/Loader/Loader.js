import React from 'react'
import LoderCss from './Loader.module.css'

function Loader() {
    return (
        <div className = {`${LoderCss.mainLoader}`}>
            <img src="image/loader.gif" alt="" />
        </div>
    )
}

export default  Loader
