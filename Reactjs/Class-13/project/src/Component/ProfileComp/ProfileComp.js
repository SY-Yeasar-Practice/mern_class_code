import React from 'react'
import data from '../Data/data'
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import ProfileCompStyle from './ProfileComp.module.css'

const  ProfileComp = ({personalInfo, socialMedia}) =>  {
    return (
        <React.Fragment className = {`row`}>
            <React.Fragment className = {`col-12`}>
                <div className = {`text-center`}>
                    {/* title name  */}
                    <h3 className = {`${ProfileCompStyle.titleText}`}>{personalInfo.firstName} {personalInfo.lastName}</h3>
                    {/* profile image  */}
                    <div className = {`${ProfileCompStyle.profileImageWrap}`}>
                        <div className = {`${ProfileCompStyle.profilePicWrap}`}>
                            <img src={`image/${personalInfo.profileImage}`} alt="" className = {`img-fluid ${ProfileCompStyle.profilePic}`} />
                        </div>
                    </div>
                    {/* discription part  */}
                    <div className = {`${ProfileCompStyle.desciptionText}`}>
                        <p>Hi my name is {personalInfo.firstName} {personalInfo.lastName} and I'm a senior {personalInfo.occupation}. Welcome to my personal website!</p>
                    </div>
                    {/* social media part  */}
                    <div className = {`d-flex justify-content-center`}>
                        {
                            socialMedia.map((data, ind) => {
                                const serviceName = data.serviceName
                                return (
                                    <div key = {ind} className = {`${ProfileCompStyle.soialMediaIndiVisualPart}`}>
                                        <a href={`${data.link}`} target = "_blank" className = {`${ProfileCompStyle.linkPart}`}>{getTheFaFaIcon(serviceName)}</a>
                                    </div>
                                )
                            }) 
                        }
                    </div>
                </div>
            </React.Fragment>
        </React.Fragment>
    )
}

//get the fav icon according to the social media
function getTheFaFaIcon(serviceName) {
    const service = serviceName //store the service name
    let iconTag
    switch(service){
        case "facebook": {
            iconTag = <i class="fab fa-facebook"></i>
            break
        }
        case "twitter": {
            iconTag = <i class="fab fa-twitter"></i>
            break
        }
        case "gitHub": {
            iconTag = <i class="fab fa-github-alt"></i>
            break
        }
        case "facebook": {
            iconTag = <i class="fab fa-facebook"></i>
            break
        }
    }
    return iconTag
}

export default ProfileComp
