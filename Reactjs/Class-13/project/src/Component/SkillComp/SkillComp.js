import React from 'react'
import SkillStyle from './Skill.module.css'

//set the skill image 
const findSkillImage = (skill) => {
    const inputSkill = skill //store the skill into a variable
    let findSkillImage
    switch (inputSkill) {
        case "Angular, React & Vue":{
            findSkillImage = <>
                <img src="image/angular.png" alt="" className = {`img-fluid h-100 w-25`} />
                <img src="image/react.png" alt=""  className = {`img-fluid h-100 w-25`} />
                <img src="image/vue.png" alt=""  className = {`img-fluid h-100 w-25`} />
            </>
            break
        }
        case "Vanila JavaScript": {
            findSkillImage = <>
                <img src="image/js.png" alt="javaScript" className = {`h-100 w-25 img-fluid`} />
            </>
            break
        }
        case "Node js": {
            findSkillImage = <>
                <img src="image/nodejs.png" alt="nodeJs" className = {`h-100 w-25 img-fluid`} />
            </>
            break
        }
        case "Python & Django": {
            findSkillImage = <>
                <img src="image/python.png" alt="python" className = {`h-100 w-25 img-fluid`} />
            </>
            break
        }
        
    }
    return findSkillImage
}

const SkillComp = ({skill}) =>  {
    return (
        <div className = {`row`}>
            {
                skill.map((ele, ind) => {
                    return (
                        <div key = {ind} className = {`col-12 col-md-3 mt-3 mt-md-0`}>
                            {/* skill part  */}
                            <div>
                                 <div className = {`h-50 w-50 d-flex`}>
                                    {findSkillImage(ele)}
                                </div>
                               <div className = {`mt-2 ${SkillStyle.skillWrap}`}>
                                    <h4 className = {`${SkillStyle.skillTitle}`}>{ele}</h4>
                                    {/* disciption part  */}
                                    <p className = {`${SkillStyle.skillDisc}`}>List skill/technologies here, You can change the icon above to any of the 1500+ FontAweson 5 free icons available, Aenean commodo ligula eget dolor</p>
                               </div>
                            </div>
                        </div>
                    )
                }) 
            }
        </div>
    )
}

export default SkillComp