import React, { Component } from 'react'

class InstantInput extends Component {
    constructor(){
        super()
        this.state = {
            message : "",
            outPutMessage : React.createRef()
        }
    }

    //inputHandler
    inputHandler = (e) => {
        this.setState ({
            message: e.target.value
        })
    }

    //outPutMessageHandler
    // outPutMessageHandler = (e) => {
    //     this.setState ({
    //         outPutMessage : e.target.value
    //     })
    // }   

    //submitController
    submitController =  (e) => {
        e.preventDefault()
        const {outPutMessage} = this.state
        const value = outPutMessage.current.value
        console.log(value);
    }

    render() {
        return (
            <div>
                <div class="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Input Message</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name = "message" value = {this.state.message} onChange = {this.inputHandler}></textarea>
                </div>
                <div class="form-group">
                    <label htmlFor="exampleFormControlTextarea1">Output Message</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name = "outPutMessage" value = {this.state.message} ref = {this.state.outPutMessage}></textarea>
                </div>
                <button onClick = {this.submitController}>Submit</button>
            </div>
        )
    }
}

export default InstantInput
