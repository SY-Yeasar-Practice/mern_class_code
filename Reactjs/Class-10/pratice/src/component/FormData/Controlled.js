import React, { Component } from 'react'

class Controlled extends Component {
    constructor(){
        super()
        this.state = {
            email: "",
            password: "",
            showPassword: false
        }
    }

    //inputController
    inputController = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    //showPasswordHandler
    showPasswordHandler = (e) => {
        if(e.target.checked){
            this.setState({
                showPassword: true
            })
        }else{
            this.setState({
                showPassword: false
            })
        }
    }   

    //submitController
    submitController = (e) => {
        e.preventDefault()
        const {email, password} = this.state
        const dataStructure = {
            email,
            password
        }
        console.log(JSON.stringify(dataStructure));
    }
    
    render() {
        return (
            <form>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name ="email" value = {this.state.email} onChange = {this.inputController}></input>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type={this.state.showPassword ? "text" : "password"} className="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" value = {this.state.password} onChange = {this.inputController}></input>
                </div>
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1"  name = "showPassword" onChange = {this.showPasswordHandler}></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Show Password</label>
                </div>
                <button type="submit" className="btn btn-primary" onClick = {this.submitController}>Submit</button>
            </form>
        )
    }
}

export default Controlled
