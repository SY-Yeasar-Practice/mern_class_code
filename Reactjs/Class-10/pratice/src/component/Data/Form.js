import React, { Component } from 'react'

class Form extends Component {
    constructor(){
        super()
        this.state = {
            text: {
                myText: '',
                id: null
            },
            allText: []
        }
    }

    changeHandler = (event) => {
        this.setState({
            [event.target.name]: {
                myText: event.target.value,
                id: String(+new Date())
            }
        })
    }
    
    //submitHandler
    submitHandler = (e) => {
        e.preventDefault()
        this.setState({
            allText: [...this.state.allText ,  {
                    myText: this.state.text.myText,
                    id: this.state.text.id
                }]
        })
    }

    //removeHandler
    removeHandler = (e, id) => {
        e.preventDefault()
        const myData = this.state.allText.filter(ele => ele.id != id)
        this.setState({
            allText: myData
        })
    }

    render() {
        // console.log(this.state.allText);
        return (
            <div>
                <div>
                    <input type="text" name="text" id="" value = {this.state.text?.myText} onChange = {this.changeHandler}/>
                    <button onClick = {this.submitHandler}>Click me</button>
                </div>
                {this.state.allText.map(ele =>(
                    
                    <div key = {ele.id}>
                        <h1>{ele.myText}</h1>
                        <button onClick = {(e) =>{return this.removeHandler(e, ele.id)}}>Remove</button>
                    </div>
                ))}
            </div>
        )
    }
}

export default Form
