import logo from './logo.svg';
import './App.css';
import Controlled from './component/FormData/Controlled';
import Form from './component/Data/Form';
import InstantInput from './component/InstantInput/InstantInput';


function App() {
  return (
    <div className="">
      <div className="row">
        <div className="col-12 col-md-6">
          <Controlled/>
        </div>
        <div className="col-12 col-md-6">
           <Form/>
        </div>
        <div className = "col-12 ">
          <InstantInput/>
        </div>
      </div>
    </div>
  );
}

export default App;
