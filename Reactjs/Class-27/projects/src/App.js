import logo from './logo.svg';
import './App.css';
import { PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import MyDocument from './Component/MyDocument';

function App() {
  const data = {
  age: 25,
  doctorName: "Dr Nuruzzamn",
  patientName: "Sadmaney Yeasar",
  doctorDegree: ["MBBS", "FCPS", "FRCS"],
  hospitalName: "Dinajpur Medical College, Dinajpur",
  hospitalAddress : "Medical College Road, Dinajpur 5200",
  contactNumber: ['01521427881', '01761730030'],
  prescription: [
    {
      medicine: 'Napa',
      amount: '2/7',
      duration: '6 day'
    },
    {
      medicine: 'Alertrol',
      amount: '2/7',
      duration: '7 day'
    },
    {
      medicine: 'Antasida ',
      amount: '2/7',
      duration: '2 day'
    },
    {
      medicine: 'Napa Extra',
      amount: '2/7',
      duration: '5 day'
    },
  ]
}
  return (
    <div>
    <PDFDownloadLink document={<MyDocument data = {data}/>} fileName={`${+(new Date())}.pdf`}>
      {({ blob, url, loading, error }) =>
        loading ? `Loading` : `Download`
      }
    </PDFDownloadLink>
    
    <PDFViewer style = {{height: "100vh" , width: "100%"}}>
        <MyDocument data = {data}/>
    </PDFViewer>
  </div>
  );
}

export default App;
