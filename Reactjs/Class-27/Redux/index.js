const {createStore, applyMiddleware, compose} = require('redux');
const { addMobileAction, sellMobileAction, addLaptopAction, sellLaptopAction } = require('./Action/shopAction');
const rootReducer = require('./Reducer//rootReducer')
const logger = require('redux-logger')
const thunk = require('redux-thunk').default;
const { sentFetchReqAction } = require('./Action/fetchDataAction');

const composeEnhancers = compose;
const createLoggers = logger.createLogger()

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(createLoggers, thunk ))) //create the store

const {getState, dispatch, subscribe} = store

const unsubscribe = subscribe(() => console.log(`Update state:`, getState()))
// dispatch(addMobileAction(200))
dispatch(sentFetchReqAction(55))
unsubscribe()

