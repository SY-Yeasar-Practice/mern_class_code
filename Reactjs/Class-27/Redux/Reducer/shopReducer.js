const { BYE_NEW_MOBILE, ADD_NEW_MOBILE, BYE_NEW_LAPTOP, ADD_NEW_LAPTOP } = require("../Const/const")

const initialState = {
    mobileSet: 500,
    laptop: 1000
}

const shopReducer = (state = initialState, action) => {
    switch(action.type) {
        case BYE_NEW_MOBILE : {
            return {
                ...state,
                mobileSet: state.mobileSet - 1
            }
        }
        case ADD_NEW_MOBILE : {
            return {
                ...state,
                mobileSet: state.mobileSet + action.payload
            }
        }
        case BYE_NEW_LAPTOP:  {
            return {
                ...state,
                laptop: state.laptop - 1
            }
        }
        case ADD_NEW_LAPTOP : {
            return {
                ...state,
                laptop: state.laptop + action.payload
            }
        }
        default : {
            return state
        }
    }
}

module.exports = shopReducer