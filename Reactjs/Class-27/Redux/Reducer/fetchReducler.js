const { FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILURE } = require("../Const/const")

const initialState = {
    isLoading: true,
    data: [],
    error: ""
}

const fetchReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REQUEST : {
            return {
                ...state,
                isLoading: true
            }
        }
        case FETCH_SUCCESS : {
            return {
                ...state,
                isLoading: false,
                data: action.payload
            }
        }
        case FETCH_FAILURE: {
            return {
                ...state,
                isLoading: false,
                error: action.payload
            }
        }
        default : {
            return state
        }
    }
}


module.exports = fetchReducer