const shopReducer = require("../Reducer/shopReducer")
const fetchReducer = require("../Reducer/fetchReducler")
const {combineReducers} = require("redux")


const rootReducer = combineReducers({
    mobileShop: shopReducer,
    fetch: fetchReducer
})

module.exports = rootReducer