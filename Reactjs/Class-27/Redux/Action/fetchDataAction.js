const { FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILURE } = require("../Const/const")
const axios = require('axios')

const fetchRequestAction = () => {
    return {
        type: FETCH_REQUEST
    }
}

const fetchSuccessAction = (data) => {
    return {
        type: FETCH_SUCCESS,
        payload: data
    }
}

const fetchFailureAction = (data) => {
    return {
        type: FETCH_FAILURE,
        payload: data
    }
}

const sentFetchReqAction = (number) => {
    return async(dispatch, getState) => {
        try {
            dispatch(fetchRequestAction()) //start request from fetch
            const fetchResponse = await axios.get(`https://jsonplaceholder.typicode.com/posts/${number}`)
            const {data} = fetchResponse //store the fetch data
            if(data) {
                dispatch(fetchSuccessAction(data) //if you got the fetch data
               )
            }else {
               dispatch( fetchFailureAction(data.message))
            }
        }catch (err) {
            dispatch(fetchFailureAction(err.message))
        }
    }
}

module.exports = {
    fetchRequestAction,
    fetchSuccessAction,
    fetchFailureAction,
    sentFetchReqAction
}