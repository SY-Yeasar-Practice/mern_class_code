const {BYE_NEW_MOBILE,
    ADD_NEW_MOBILE,
    ADD_NEW_LAPTOP,
    BYE_NEW_LAPTOP} = require('../Const/const')

const sellMobileAction = () => {
   return {
       type: BYE_NEW_MOBILE
   }
}

const addMobileAction = (data) => {
    return {
        type: ADD_NEW_MOBILE,
        payload: data
    }
}

const addLaptopAction = (data) => {
    return {
        type: ADD_NEW_LAPTOP,
        payload: data
    }
}

const sellLaptopAction = () => {
    return {
        type: BYE_NEW_LAPTOP
    }
}

module.exports = {
    sellMobileAction,
    addMobileAction,
    addLaptopAction,
    sellLaptopAction
}