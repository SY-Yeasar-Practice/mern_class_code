import logo from './logo.svg';
import './App.css';
import { Form } from './Comp/CustomHooks/FormTwo/Form/Form';
// import Parent from './Comp/CustomHooks/FormOne/Parent/Parent';
// import FormWithClass from './UseRef/Form/FormWithClass';
// import UserRefWithForm from './UseRef/UserRefWithForm';

// import Parent from './Comp/ContextApi/Parent/Parent';

function App() {
  return (
    <div className="">
      {/* <Parent/> */}
      {/* <FormWithClass/> */}
      {/* <UserRefWithForm/> */}
      {/* <Parent/> */}
      <Form/>
    </div>
  );
}

export default App;
