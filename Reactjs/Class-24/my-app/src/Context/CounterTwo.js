import React from 'react'

const counterTwoContext = React.createContext()
const CounterTwoProvider = counterTwoContext.Provider
const CounterTwoConsumer = counterTwoContext.Consumer

export default counterTwoContext

export {
    CounterTwoProvider,
    CounterTwoConsumer
}