import React from 'react'
import { CounterConsumer } from '../../../Context/Counter'
import { CounterTwoConsumer } from '../../../Context/CounterTwo'

const ChildThree = () => {
     console.log(`I am rendered from child Three`);
    return (
        <div>
            
            <CounterConsumer>
                {
                    (value) => {
                        const {count, decrementHandler, incrementHandler, setCount} = value
                        return(
                            <>
                                <CounterTwoConsumer>
                                    {
                                        (valueTwo) => {
                                            const {countTwo, decrementHandler, incrementHandler, setCountTwo} = valueTwo
                                            return (
                                                <>
                                                    <h1>Count One: {count} </h1>
                                                    <h1>Count Two: {countTwo}</h1>
                                                    <button onClick = {(e) => setCount(count + 1)}>Increment</button>
                                                    <button onClick = {(e) => setCount(count - 1)}>Decrement</button>
                                                </>
                                            )
                                        }
                                    }
                                </CounterTwoConsumer>
                            </>
                        )
                    }
                }
            </CounterConsumer>
        </div>
    )
}

export default ChildThree
