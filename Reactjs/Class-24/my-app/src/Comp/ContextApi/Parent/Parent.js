import React, {useState} from 'react'
import { CounterProvider } from '../../../Context/Counter'
import { CounterTwoProvider } from '../../../Context/CounterTwo'
import ChildOne from '../ChildOne/ChildOne'
import ChildTwo from '../ChildTwo/ChildTwo'

const Parent = () => {
    const [count, setCount] = useState(0)
    const [countTwo, setCountTwo] = useState(5)

    const incrementHandler = (e) => {
        e.preventDefault()
        setCount(count + 1)
    }
    const incrementHandlerTwo = (e) => {
        e.preventDefault()
        setCountTwo(countTwo + 5)
    }

    const decrementHandler = (e) => {
        e.preventDefault()
        setCount(count - 1)
    }

    const decrementHandlerTwo = (e) => {
        e.preventDefault()
        setCountTwo(countTwo - 5)
    }

    const data = {
        count,
        decrementHandler,
        incrementHandler,
        setCount
    }

    const dataTwo = {
        countTwo,
        decrementHandlerTwo,
        incrementHandlerTwo,
        setCountTwo
    }

    // console.log(`I am from Parent`);
    return (
        <div>
            <h1>Count: {count}</h1>
            <CounterProvider value = {data}>
                <CounterTwoProvider value = {dataTwo}>
                    <ChildOne/>
                    <ChildTwo/>
                </CounterTwoProvider>
            </CounterProvider>
        </div>
    )
}

export default Parent
