import React, {useContext} from 'react'
import counterContext from '../../../Context/Counter'
import counterTwoContext from '../../../Context/CounterTwo'


const ChildTwo = () => {
    const counterOne = useContext(counterContext)
    const {count, setCount} = counterOne
    const counterTwo = useContext(counterTwoContext)
    return (
        <div>
            <button onClick={(e) => setCount(count + 1)}>Increment from child two</button>
            <button onClick = {(e) => setCount(count - 1 )}>Decrement from child two</button>
        </div>
    )
}

export default ChildTwo
