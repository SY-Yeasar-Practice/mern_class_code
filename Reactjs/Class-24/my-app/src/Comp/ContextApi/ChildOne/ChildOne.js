import React from 'react'
import ChildThree from '../ChildThree/ChildThree'

const ChildOne = () => {
    console.log(`I am rendered from child One`);
    return (
        <div>
            <ChildThree/>
        </div>
    )
}

export default ChildOne
