import React, {useState} from 'react'
import useFormInput from '../CreateHooks/useFormInput' 

const Parent = () => {
    // const [firstName, setFirstName] = useState("")
    // const [lastName, setLastName] = useState("")
    const [firstName, firstNameAttribute, firstNameReset] = useFormInput("")
    const [lastName, lastNameAttribute, lastNameReset] = useFormInput("")

    // submitHandler
    const submitHandler = (e) => {
        e.preventDefault()
        alert(`My name is ${firstName} ${lastName}`)
        firstNameReset()
        lastNameReset()
    }
    return (
        <div>
            <form action="/action_page.php">
                <label for="fname">First name:</label><br/>
                <input 
                type="text" 
                id="fname" 
                name="fname" 
                {...firstNameAttribute}/><br/>

                <label for="lname">Last name:</label><br/>
                <input 
                type="text" 
                id="lname" 
                name="lname" 
                // value={lastName}
                // onChange = {(e) => setLastName(e.target.value)}
                {...lastNameAttribute}/> 
                
                <br/>
                <input type="submit" value="Submit" onClick={(e) => submitHandler(e)}/>
            </form> 
        </div>
    )
}

export default Parent
