import React, {useState} from 'react'

const useFormInput = (initialState) => {
    const [value, setValue] = useState(initialState)

    const attributes = {
        value,
        onChange: (e) => setValue(e.target.value)
    }

    const reset = () => {
        setValue(initialState)
    }

    return [
        value,
        attributes,
        reset
    ]

}

export default useFormInput
