import React, {useState} from 'react'

const useFormInputTwo = (initialState) => {
    const [formData, setFormData] = useState(initialState)
    const {firstName, lastName} = formData
    const attributes = {
        onChange : (e) => setFormData({...formData, [e.target.name]: e.target.value}),
        value : firstName
    }

    const remove = () => {
        setFormData(initialState)
    }

    return [
        formData,
        attributes,
        remove
    ]
}

export default useFormInputTwo
