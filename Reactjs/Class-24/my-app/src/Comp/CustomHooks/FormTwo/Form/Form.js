import React, {useState} from 'react'
import useFormInputTwo from '../useFormInputTwo/useFormInputTwo'

export const Form = () => {
    const [formData, attribute, reset] = useFormInputTwo({
        firstName: "",
        lastName: ""
    })
    // submitHandler
    const submitHandler = (e) => {
        e.preventDefault()
        const {firstName, lastName} = formData
        alert(`My name is ${firstName} ${lastName}`)
    }

    return (
        <div>
             <form action="/action_page.php">
                <label for="fname">First name:</label><br/>
                <input 
                type="text" 
                id="fname" 
                name="fname" 
                // value = {formData.firstName}
                {...attribute}
                /><br/>

                <label for="lname">Last name:</label><br/>
                <input 
                type="text" 
                id="lname" 
                name="lname" 
                // value={lastName}
                // onChange = {(e) => setLastName(e.target.value)}
                // value = {formData.lastName}
                {...attribute}
                /> 
                
                <br/>
                <input type="submit" value="Submit"  onClick={(e) => submitHandler(e)}/>
            </form> 
        </div>
    )
}
