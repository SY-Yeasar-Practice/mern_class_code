import React, { Component } from 'react'


class FormWithClass extends Component {

    constructor() {
        super()
        this.firstName = React.createRef()
    }
   
    componentDidMount() {
       this.firstName.current.focus()
    }
    render() {
        return (
            <div>
                <input type="text" ref = {this.firstName}/>
            </div>
        )
    }
}

export default FormWithClass
