import React, {useState, useEffect, useRef}from 'react'

const UserRefWithForm = () => {
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [number, setNumber] = useState("")
    const [showHome, setShowHome ] = useState(false)
    const firstNameRef = useRef(null)
    const lastNameRef = useRef(null)
    const numberRef = useRef(null)

    const changeHandler = (e) => {
        e.preventDefault()
        const name = e.target.name
        if(name == "firstName") {
            setFirstName(e.target.value)
        }else if (name == "lastName") {
            setLastName(e.target.value)
        }else if (name == "number") {
            setNumber(e.target.value)
        }
    }

    useEffect(() => {
        if(firstName.length == 0 ) {
            firstNameRef.current.focus()
        }else if(firstName.length == 3) {
            lastNameRef.current.focus()
        }
    }, [firstName])

    useEffect(() => {
        if (firstName.length == 3 && lastName.length == 3) {
            numberRef.current.focus()
        }
    }, [lastName])

    useEffect(() => {
        if (firstName.length == 3 && lastName.length == 3 && number.length == 3) {
            setShowHome(true)
        }
    }, [number])

    return (
        <div>
            {
                showHome && <h1>Hello I am successfully done</h1>
            }
            <input 
            type="text" 
            style = {{marginRight: "5%"}} 
            value = {firstName}
            name = "firstName"
            onChange = {(e) => changeHandler(e)}
            maxLength= "3"
            ref = {firstNameRef}/>

            <input 
            type="text" 
            style = {{marginRight: "5%"}} 
            name = "lastName"
            value = {lastName}
            onChange = {(e) => changeHandler(e)}
            maxLength= "3"
            ref = {lastNameRef}/>

            <input 
            type="text" 
            style = {{marginRight: "5%"}}
            value = {number}
            name = "number"
            onChange = {(e) => changeHandler(e)}
            maxLength= "3"
            ref = {numberRef}/>

        </div>
    )
}

export default UserRefWithForm
