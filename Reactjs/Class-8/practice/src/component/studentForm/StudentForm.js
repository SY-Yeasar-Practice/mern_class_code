import React, {Component} from 'react'

class StudentForm extends Component{
    constructor(){
        super()
        this.state = {
            firstName: "", //text
            lastName: "", //text
            fatherName: "", //text
            motherName: "", //text
            permanentAddress: "", //text
            currentAddress: "", //text
            contactNumber:"", //text
            dateOfBirth:"", //text
            sex:"", //radio
            userId: "", //text
            email:"", //text
            userType:"" ,//section //text
            imageUpload: null ,//image upload //file
            imageUploadTwo : null, //demo image upload
            password : "", //password
            retypePassword : "" //retype password
        }
    }

    //take all data input handler
    dataChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value //store the input value dynamic way by input data name
        })
    }

    //take the image input handler
    imageUploadController = (event) => {
        this.setState({
            [event.target.name] : URL.createObjectURL(event.target.files[0])
        })
    }

    
    //form submission handler
    submissionHandelar = (event) => {
        event.preventDefault() //to stop the auto load during submission

        const {firstName, lastName, fatherName, motherName, permanentAddress, currentAddress, contactNumber, dateOfBirth, sex, userId, email, userType, imageUpload, password, retypePassword} = this.state //get the data from state value

        let data = {
            personalInfo: {
                name: {
                    FirstName: firstName,
                    LastName: lastName
                },
                contact: {
                    permanentAddress,
                    currentAddress,
                    mobileNo: contactNumber
                },
                profilePic: imageUpload,
                FatherName: fatherName,
                MotherName: motherName,
                email,
                dateOfBirth,
                sex
            },
            userId,
            userType: userType,
            password,
            retypePassword
        } //format of my model schema

        const dataInJsonFormat = JSON.stringify(data) //conver the data in to  json format
        console.log(dataInJsonFormat); //give the data in json format
    } 
    
    
    render(){
        return(
            <div>
                <form className = {`mb-5`}>
                    <div className="form-group">
                        {/* name*/}
                        <label htmlFor="exampleInputEmail1">Name</label>
                        <input type="text" className="form-control " id="exampleInputEmail1"  placeholder="First Name" name = "firstName" onChange = {this.dataChangeHandler} value = {this.state.firstName}></input>
                        <br />
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Last Name" name = "lastName" value = {this.state.lastName} onChange = {this.dataChangeHandler}></input>
                    </div>
                    {/* personalInfo  */}
                    <div className="form-group">
                        <br />
                        <label htmlFor="personal info">Personal Information</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Father Name" name = "fatherName" value = {this.state.fatherName} onChange = {this.dataChangeHandler}></input>

                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Mother Name" value = {this.state.motherName} name = "motherName" onChange = {this.dataChangeHandler}></input>

                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Permanent Address" name = "permanentAddress" value = {this.state.permanentAddress} onChange = {this.dataChangeHandler}></input>

                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Current Address" name = "currentAddress" value = {this.state.currentAddress} onChange = {this.dataChangeHandler} ></input>

                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Contact Number" name = "contactNumber" value = {this.state.contactNumber} onChange = {this.dataChangeHandler}></input>
                    </div>

                    <br /> 
                    <div className="form-group">
                        {/* date of birth  */}
                        <label htmlFor="dateOfBirth">Date of Birth</label>
                        <input type="date" className="form-control" id="exampleInputPassword1" placeholder="Date of Birth" name = "dateOfBirth" value = {this.state.dateOfBirth} onChange = {this.dataChangeHandler}></input>
                    </div>

                    <br />

                    {/* sex  */}
                    <label htmlFor="sex">Sex</label>
                    <div className="form-check">
                        {/* female  */}
                        <input className="form-check-input" type="radio" name="sex" id="exampleRadios1" value = "female" onChange = {this.dataChangeHandler}></input>
                        <label className="form-check-label" htmlFor="exampleRadios1">
                            Female
                        </label>
                    </div>
                    {/* male  */}
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="sex" id="exampleRadios2" value="male" onChange = {this.dataChangeHandler}></input>
                        <label className="form-check-label" htmlFor="exampleRadios2">
                            Male
                        </label>
                    </div>
                    
                    <br />

                    {/* user id  */}
                    <div className="form-group">
                        <label htmlFor="UserId">User Id</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="User ID" name = "userId" value = {this.state.userId} onChange = {this.dataChangeHandler}></input>
                    </div>
                    
                    <br />
                    {/* password  */
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name = "password" onChange = {this.dataChangeHandler} value = {this.state.password}></input>
                    </div>}

                    {/* retype password  */}
                    <div className="form-group">
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Retype Password" name = "retypePassword" value = {this.state.retypePassword} onChange = {this.dataChangeHandler}></input>
                    </div>
                    
                    <br />
                    {/* email  */}
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Email" name = "email" value = {this.state.email} onChange = {this.dataChangeHandler}></input>
                    </div>
                    <br />

                    {/* user type  */}
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">User Type</label>
                        <select class="form-control" id="exampleFormControlSelect1" name = "userType" value = {this.state.userType}  onChange = {this.dataChangeHandler} >
                            <option value = "student">Student</option>
                            <option value = "teacher">Teacher</option>
                        </select>
                    </div>

                    <br />
                    {/* image upload one */}
                    <label htmlFor="proPicUpload">Profile Picture</label>
                    <div className="input-group mb-3">
                        <div className="custom-file">
                            <input type="file" className="custom-file-input" id="inputGroupFile01" name = "imageUpload" onChange = {this.imageUploadController}/>
                            <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
                        </div>
                    </div>

                    <br />
                    {/* image upload two  */}
                    <label htmlFor="demoPicUpload">Demo Picture upload</label>
                    <div className="input-group mb-3">
                        <div className="custom-file">
                            <input type="file" className="custom-file-input" id="inputGroupFile01" name = "imageUploadTwo" onChange = {this.imageUploadController} />
                            <label className="custom-file-label" htmlFor="inputGroupFile01">Choose file</label>
                        </div>
                    </div>

                    
                    {/* submit button  */}
                    <button type="submit" className="btn btn-primary" onClick = {this.submissionHandelar}>Submit</button>
                </form>
            </div>
        )
    }
}

export default StudentForm