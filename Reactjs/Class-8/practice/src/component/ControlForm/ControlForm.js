import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ControlForm extends Component {
    constructor(){
        super()
        this.state = {
            name : '',
            password: '',
            hobby : [],
            sex : '',
            dateOfBirth: ''
        }
    }

    changeHandaler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandaller = (event) => {
        event.preventDefault() //to avoid the reload the page during the submission
        let {name, password } = this.state
        console.log(name, password);
    }


    hobbyHandaller = (event) => {
        if(event.target.checked){
            this.setState({
                hobby: [...this.state.hobby , event.target.value]
            })
        }else if(event.target.checked == false){
            let index = this.state.hobby.findIndex((val) => val == event.target.value )
            this.state.hobby.splice(index, 1)
        }
    }
    clickHandler = (event) => {
        event.preventDefault();
        const {name,password, hobby, sex, dateOfBirth} = this.state;
        const data = {
            name,
            sex,
            hobby,
            dateOfBirth,
            password
        }
        console.log(data);
    }

    sexHandaler = (event) => {
        if(event.target.checked){
            this.setState({
                sex: event.target.value
            })
        }
    }

    render() {
        return (
            <form>
                <br />
                {/* Name */}
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Name</label>
                    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Name" value = {this.state.name} onChange = {this.changeHandaler} name = "name"></input>
                </div>

                <br />
                {/* password  */}
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" value = {this.state.password} name = "password" onChange = {this.changeHandaler}></input>
                </div>

                <br />

                {/* hobby */}
                <label htmlFor="Hobby">Hobby</label>

                {/* Drawing */}
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" name = "badminton" value = "Drawing" onChange = {this.hobbyHandaller}></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Drawing</label>
                </div>
                {/* Programming */}
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" name = "programming" value = "Programming" onChange = {this.hobbyHandaller}></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Programming</label>
                </div>
                {/* Playing Badminton  */}
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" value = "Playing Badminton" name = "playingBadminton" onChange = {this.hobbyHandaller}></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Playing Badminton</label>
                </div>

                <br />
                
                {/* Sex  */}
                <label htmlFor="Sex">Sex</label>
                {/* Male  */}
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="sex" id="flexRadioDefault1" value = "male" onChange = {this.sexHandaler}></input>
                    <label className="form-check-label" htmlFor="Male">
                        Male
                    </label>
                </div>
                {/* Female  */}
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="sex" id="flexRadioDefault2" value ="female"  onChange = {this.sexHandaler} ></input>
                    <label className="form-check-label" htmlFor="Female">
                        Female
                    </label>
                </div>

                <br />
                <div className = "form-group">
                    <label htmlFor="BirthDate">Date of Birth </label>
                    <input type="date" className = {`form-controller`} name = "dateOfBirth" onChange = {this.changeHandaler} value = {this.state.dateOfBirth} />
                </div>
                {/* submit button  */}
                <button  className="btn btn-primary" onClick = {this.clickHandler}>Submit</button>
            </form>
            
        )
    }
}

export default ControlForm