import React, {Component} from 'react'

class Button extends Component {
    constructor(){
        super()
        this.state = {
            count: 0,
            timer: 0,
            timerStatus: false
        }
    }
    countDown = () => {
        if(this.state.count >= 10 || this.state.count == 'Done'){
            this.setState({
                count: "Done"
            })
            console.log(this.state.count);

        }else{
            this.setState({
                count: this.state.count + 1 
            })
        }

    }

    timerOn = () => {
         this.timerController = setInterval(() =>{
            this.setState({
                timer: this.state.timer + 1,
                timerStatus: true
            })
        }, 10)
        
    }
    buttonAble = () => {
        this.setState({
            timerStatus: false
        })
    }
    timerOff = () => {
        clearInterval(this.timerController)
        this.setState({
            timerStatus: false
        })
    }
    timeReset = () => {
        this.setState({
            timer: 0
        })
    }
    
    render(){
        const buttonDisable = (this.state.count == "Done" ) && "disabled"
        const timerButtonDisable = (this.state.timerStatus == true) ? "disabled" : ""
        const timerButtonDisableTwo = (this.state.timerStatus == false) ? "disabled" : ""
        return(
            <div>
                <h1>{this.state.count}</h1>
                <button className = {`btn btn-success ${buttonDisable}`} onClick = {this.countDown}>Click me</button>
                <h1>Timer: {this.state.timer}</h1>
                <button className = {`btn btn-danger ${timerButtonDisable}`} onClick = {this.timerOn}>Start</button>
                <button className = {`btn btn-danger ${timerButtonDisableTwo} `} onClick = {this.timerOff}>Stop</button> 
                <button className = {`btn btn-success`} onClick = {this.timeReset}>Reset</button> 
            </div>
           
        )
    }
}

export default Button