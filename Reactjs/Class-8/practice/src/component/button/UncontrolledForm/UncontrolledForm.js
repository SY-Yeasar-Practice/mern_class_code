import React, {Component} from 'react'

class UncontrolledForm extends Component{
    constructor(){
        super()
        this.email = React.createRef()
        this.bangladeshCheck = React.createRef()
        this.indiaCheck = React.createRef()
        this.pakistanCheck = React.createRef()
        this.allChecked = []
        this.password = React.createRef()
        this.argentinaRadio = React.createRef()
        this.brasilRadio = React.createRef()
    }

    submitHandler = (event, name) => {
        event.preventDefault()
        let {email, bangladeshCheck, indiaCheck, pakistanCheck, allChecked, password, argentinaRadio , brasilRadio, radioValue} = this
        let emailValue = email.current.value

        bangladeshCheck.current.checked && allChecked.push(bangladeshCheck.current.value)
        indiaCheck.current.checked && allChecked.push(indiaCheck.current.value)
        pakistanCheck.current.checked && allChecked.push(pakistanCheck.current.value)

        let passwordValue = password.current.value

        argentinaRadio.current.checked && (this.radioValue = argentinaRadio.current.value)
        brasilRadio.current.checked && (this.radioValue = brasilRadio.current.value)

        console.log(emailValue, name, allChecked, passwordValue, radioValue);
        console.log(argentinaRadio.current.checked);

        
    }

    render(){
        return(
            <form>
                <br />
                {/* input the email */}
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" ref = {this.email}></input>
                </div>
                <br />
                {/* input password */}
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" ref = {this.password}></input>
                </div>
                <br />
                {/* input check part */}
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" value = "Bangladesh" ref ={this.bangladeshCheck} ></input>
                    <label className="form-check-label" htmlFor="exampleCheck1" >Bangladesh</label>
                </div>
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" value = "India" ref ={this.indiaCheck} ></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">India</label>
                </div>
                <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" value = "Pakistan" ref ={this.pakistanCheck} ></input>
                    <label className="form-check-label" htmlFor="exampleCheck1">Pakistan</label>
                </div>

                <br />

                {/* radio button */}
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="Argentina" checked ref = {this.argentinaRadio} ></input>
                    <label className="form-check-label" htmlFor="exampleRadios1">
                        Argentina
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="Brasil" ref = {this.brasilRadio}></input>
                    <label className="form-check-label" htmlFor="exampleRadios2">
                        Brasil
                    </label>
                </div>
                <br />
                <button type="submit" className="btn btn-primary" onClick = {(event) => {this.submitHandler(event, "shopnil")}}>Submit</button>
            </form>
        )
    }
}

export default UncontrolledForm