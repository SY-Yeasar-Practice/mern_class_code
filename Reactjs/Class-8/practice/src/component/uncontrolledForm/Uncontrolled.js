import React, { Component } from 'react'


class Uncontrolled extends Component {
    constructor(){
        super()
        //store all ref from input field
        this.name = React.createRef() // --------- (2)
        this.sex = React.createRef()
        this.country = React.createRef()
        this.hobby = []
        this.playing = React.createRef()
        this.programming = React.createRef()
    } //------------ (3)

   
    //submit handler
    submitHandler = (e) => {
        e.preventDefault() //ignore auto page load during submission ------------(6)
        let {name, sex, country, hobby, programming, playing} = this //get the ref from input  --------(7)
        let nameInput = name.current.value , sexInput = sex.current.value, countryInput = country.current.value, hobbyInput = hobby //store the value what we got from ref -------- (8)
        
        //hobby select part
        if(playing.current.checked){ //playing part
             let isAvailble = hobby.find(data => data == playing.current.value) 
             if(!isAvailble){
                hobby.push(playing.current.value)
             } 
        }else if(playing.current.checked == false){
            let index = hobby.findIndex(ele => ele == playing.current.value)
            hobby.splice(index, 1)
        } //------------- (9)
        if(programming.current.checked){ //programming part
             let isAvailble = hobby.find(data => data == programming.current.value) 
             if(!isAvailble){
                hobby.push(programming.current.value)
             }
        }else if(programming.current.checked == false){
            let index = hobby.findIndex(ele => ele == programming.current.value)
            hobby.splice(index, 1)
        } // ------------- (10)

        console.log(`name = ${nameInput} sex = ${sexInput} country = ${countryInput} hobby = ${hobbyInput}`);
        
    } //----------------- (5)
    render() {
        return (
            <div>
                <form>
                    {/* name  */}
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" ref = {this.name}></input> {/*---------- (1) */}
                    </div>

                    {/* sex  */}
                    <label htmlFor="sex">Sex</label>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="male" ref = {this.sex}></input>
                        <label className="form-check-label" htmlFor="exampleRadios1">
                            Male
                        </label>
                        </div>
                        <div className="form-check">
                        <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="female" ref = {this.sex}></input>
                        <label className="form-check-label" htmlFor="exampleRadios2">
                            Female
                        </label>
                    </div>

                    {/* country  */}
                     <div className="form-group">
                        <label htmlFor="exampleFormControlSelect1">Example select</label>
                        <select className="form-control" id="exampleFormControlSelect1" ref = {this.country}>
                            <option value = "bangladesh">Bangladesh</option>
                            <option value = 'india'>India</option>
                        </select>
                    </div>

                    {/* hobby */}
                    <label htmlFor="hobby">Hobby</label>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" ref = {this.programming} value = "programming"></input>
                        <label className="form-check-label" htmlFor="exampleCheck1">Programming</label>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" ref = {this.playing} value = "playing"></input>
                        <label className="form-check-label" htmlFor="exampleCheck1">Playing</label>
                    </div>

                    {/* submit button  */}
                    <button type="submit" className="btn btn-primary" onClick = {this.submitHandler}>Submit</button> {/* --------------- (4) */}
                    </form>
            </div>
        )
    }
}

export default Uncontrolled
