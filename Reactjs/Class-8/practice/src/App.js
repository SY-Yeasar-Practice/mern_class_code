import logo from './logo.svg';
import './App.css';
import Button from './component/button/button';
import UncontrolledForm from './component/button/UncontrolledForm/UncontrolledForm';
import ControlForm from './component/ControlForm/ControlForm';
import StudentForm from './component/studentForm/StudentForm';
import Uncontrolled from './component/uncontrolledForm/Uncontrolled';

function App() {
  return (
    <div className={`container-fluid `}>
        <div className={`container`} >
          <div className = {`row mt-4 `}>
              <div className = {`col-12 col-md-4`}><Button/></div>
              {/* <UncontrolledForm/> */}
              {/* <ControlForm/> */}
              <br />
              {/* <div className = {`col-12 col-md-6 mt-4 mt-md-0`} ><StudentForm/></div> */}
              <div className = {`col-12 col-md-6 mt-4 mt-md-0`} >
                <Uncontrolled/>
              </div>
              
          </div>
        </div>
    </div>
  );
}

export default App;
