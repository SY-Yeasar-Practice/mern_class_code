import logo from './logo.svg';
import './App.css';
import ImageUploader from './utils/ImageUploader';
import Form from './Component/Form/Form';
import NewForm from './Component/NewForm/NewForm';
import MultipleFileUpload from './utils/MultipleFileUpload';

function App() {
  return (
    <div className="container">
      {/* <ImageUploader/> */}
      {/* <Form/> */}
      <NewForm/>
      {/* <MultipleFileUpload/> */}
    </div>
  );
}

export default App;
