import React, { Component } from 'react'
import defaultImage from '../assert/download.png'

class ImageUploader extends Component {
    constructor() {
        super()
        this.state = {
            file: null,
            preview: defaultImage
        }
    }
    
    // changeHandler
    changeHandler = (e) => {
        e.preventDefault()
        const reader = new FileReader() //create the instance of a reader
        let myNewFile ;//will store all new file here

        if(e.target.files[0]) {
            myNewFile = e.target.files[0] //if there have new file then store it 
        }else{
            myNewFile = this.state.file //else store the exist one
        }
        reader.onloadend = () => {
            this.setState ({
                file: myNewFile, //store the new file into the state value
                preview: reader.result
            },
            () => {
                this.props.imageChangeHandler(myNewFile)
            })
        }
        reader.readAsDataURL(myNewFile)
        
    }

    render() {
        return (
            <div>
                <img src={this.state.preview} alt="" style = {{height: "20%", width: '20%'}} /> <br />
                <input type="file" name="imageUploader" id="" onChange = {this.changeHandler} />
            </div>
        )
    }
}

export default ImageUploader
