import React, { Component } from 'react'

class SingleImageUploader extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            file: null,
            preview: this.props.defaultImg //set the default preview image
        }
    }
    
    // imageHandler
    imageHandler = (e) => {
        e.preventDefault()
        const myImageFile = e.target.files[0] //store the file
        if(e.target.files[0]){
            const reader = new FileReader() //create the reader instance
            reader.onloadend = () => {
                if(myImageFile){
                    this.setState({
                        file: myImageFile,
                        preview: reader.result //this will give a base64 format data
                    }, //set the new state data with the new fle 
                    () => {
                        this.props.imageUploadHandler(myImageFile) //sent the file for upload 
                    }) 
                }else{
                    this.setState({
                        file: this.state.file,
                        preview: this.props.defaultImg 
                    })
                }
            }
            reader.readAsDataURL(myImageFile)
        }
    }

    render() {
        return (
            <React.Fragment>
                <input class="form-control form-control-sm" id="formFileSm" type="file" onChange = {(e) => this.imageHandler(e)} ></input>
                <img src={this.state.preview} alt="file" className = {`mt-3`} style = {{height:'100px', width: '123px'}} />
            </React.Fragment>
        )
    }
}

export default SingleImageUploader