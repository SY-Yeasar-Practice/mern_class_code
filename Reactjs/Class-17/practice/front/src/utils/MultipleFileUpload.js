import React, { Component } from 'react'
import style from './MutipleFileUploaderStyle.module.css'

class MultipleFileUpload extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            preview : [],
            // file: []
        }
    }

    // changeHandler
    changeHandler = (e) => {
        e.preventDefault()
        let rawFileData = [...e.target.files] //store the raw file data
        let storeData = [] //store the export data in it 
        rawFileData.map(rawData => {
            const reader = new FileReader() //create the file reader
            reader.onloadend = () => {
                storeData.push(rawData)
                this.setState({
                    preview: [...this.state.preview, reader.result], //here reader.result make the base 64 data of the data what we have to sent to server
                    // file: [...this.state.file, rawData] //update the file
                })//update the state value and show the data
            } //load the reader and sent it to form handler and change the state value
            // this.props.fileExportHandler(rawFileData)
            reader.readAsDataURL(rawData) //create a binary data of the data
        })
        this.props.fileExportHandler(rawFileData)
    }
    
    //deleteHandler 
    deleteHandler = (e, index) => {
        e.preventDefault()
        const {preview} = this.state
        preview.splice(index, 1)
        this.setState({
            preview
        })
    }

    render() {
        console.log("Previous preview", this.state.preview);
        return (
            <div class="mb-3">
                <input multiple className="form-control form-control-sm mb-3" id="formFileSm" type="file" onChange = {this.changeHandler}/>
                {
                    this.state.preview.length == 0
                    ?
                    <img src={`${this.props.defaultImg}`}  style = {{height: "100px", width: '123px'}} alt ="image"  />
                    :
                    this.state.preview.map((data, ind) => {
                        return(
                            <div key = {ind} className ={`d-inline-block ${style.parentDiv}`} >
                                <img  src={`${data}`}  style = {{height: "100px", width: '123px'}} alt ="image"  />
                                <button className = {`${style.btnPart}`} onClick = {(e) => this.deleteHandler(e, ind)} ><i class="fas fa-times"></i></button>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default MultipleFileUpload
