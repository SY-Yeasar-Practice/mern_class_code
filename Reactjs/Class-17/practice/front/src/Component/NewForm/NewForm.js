import React, { Component } from 'react'
import SingleImageUploader from '../../utils/SingleImageUploader'
import defaultImg from '../../assert/download.png'
import axios from 'axios'
import MultipleFileUpload from '../../utils/MultipleFileUpload'

class NewForm extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            email: '',
            password: '',
            sex: '',
            image:'',
            multipleImage: []

        }
    }
    
    // profileImageHandler 
    profileImageHandler = (file) => {
        const {size} = file //get the data from file
        const reader = new FileReader() 
        reader.onloadend = () => {
            this.setState({
                image: {
                    base64: reader.result, //store the base data of the file
                    size
                }
            })
        }
        reader.readAsDataURL(file)
    }

    //other data handler
    formDataHandler = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    // multipleImageHandler
    multipleImageHandler = (file) => {
        const myFile = file //store the file into another variable
        myFile.map(rawData => {
            const reader = new FileReader() //create the reader instance what read the file data
            reader.onloadend = () => {
                const data = {
                    base64: reader.result, //store the base64 data 
                    size: rawData.size
                } //create the data format
                this.setState({
                    multipleImage: [...this.state.multipleImage, data]
                })
            }
            reader.readAsDataURL(rawData) //create a binary url data format of the provided data
        })
    }
    
    // submitHandler
    submitHandler = (e) => {
        e.preventDefault()
        const {email, password, sex, image, multipleImage} = this.state //get the data from state
        const data = {
            email,
            password,
            sex,
            image,
            multipleImage
        }
        axios.post(`http://localhost:3030/user/create`, data) //sent the data to server
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
    }
    render() {
        return (
            <div>
                <form>
                    {/* email  */}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" onChange = {(e) => this.formDataHandler(e)} name = "email" value = {this.state.email}></input>
                    </div>

                    {/* password  */}
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"  onChange = {(e) => this.formDataHandler(e)} name = "password" value = {this.state.password}></input>
                    </div>

                    {/* Sex  */}
                    <label htmlFor="sex">Sex</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sex" id="flexRadioDefault1"  onChange = {(e) => this.formDataHandler(e)}  value = "male"></input>
                        <label class="form-check-label" for="flexRadioDefault1">
                            Male
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sex"  value = "female" id="flexRadioDefault2"   onChange = {(e) => this.formDataHandler(e)} ></input>
                        <label class="form-check-label" for="flexRadioDefault2">
                            Female
                        </label>
                    </div>

                    {/* profile Image */}
                    <div class="mb-3">
                        <label for="formFileSm" class="form-label">Profile Image</label>
                        <SingleImageUploader defaultImg = {defaultImg} imageUploadHandler = {this.profileImageHandler}/>
                    </div>

                    {/* multiple file upload  */}
                     <div class="mb-3">
                        <label for="formFileSm" class="form-label">Multiple Image</label>
                        <MultipleFileUpload defaultImg = {defaultImg} fileExportHandler = {this.multipleImageHandler}/>
                    </div>

                    {/* submit */}
                    <button type="submit" class="btn btn-primary" onClick = {(e) => this.submitHandler(e)}>Submit</button>
                </form>
            </div>
        )
    }
}


export default NewForm