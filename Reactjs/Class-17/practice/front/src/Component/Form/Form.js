import React, { Component } from 'react'
import ImageUploader from '../../utils/ImageUploader'
import axios from 'axios'

class Form extends Component {
    constructor () {
        super()
        this.state = {
            userName: '',
            password: '',
            image: ''
        }
    }

    // imageHandler
    imageHandler = (file) => {
        const reader = new FileReader()
        reader.onloadend = () => {
            this.setState ({
                image: {
                    base64: reader.result,
                    file
                }
            })
        }
        reader.readAsDataURL(file)
    }

    // dataChangeHandler
    dataChangeHandler = (e) => {
        this.setState ({
            [e.target.name] : e.target.value //set the value dynamic way
        })
    }

    //submit handler 
    submitHandler = (e) => {
        e.preventDefault()
        const {userName, password, image} = this.state //get the data from constructor
        const data = {
            userName,
            password,
            image
        }
        
        console.log(data);

        //send the data into the server
        axios.post('http://localhost:3030/user/create', data)
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
    }
    render() {
        return (
            <React.Fragment>
                <form action="/action_page.php">
                    {/* User Name  */}
                    <label for="fname">User Name</label> <br />
                    <input type="text" name="userName" id="" value = {this.state.userName} onChange = {this.dataChangeHandler}/>
                    
                    <br />
                    <br />

                    {/* password  */}
                    <label htmlFor="lname">Password</label> <br />
                    <input type="password" id="" name="password" value = {this.state.password} onChange = {this.dataChangeHandler} />
                    
                    <br />
                    <br />

    
                    <ImageUploader imageChangeHandler = {this.imageHandler}/>

                    <br />
                    <br />
                    <button type="submit" value="Submit" onClick = {this.submitHandler} >Submit</button>
                </form> 
            </React.Fragment>
        )
    }
}

export default  Form