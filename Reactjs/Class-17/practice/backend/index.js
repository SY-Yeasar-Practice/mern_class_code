const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv').config()
const cors = require('cors')
const userRoute = require('./src/route/demoUser')

const PORT = process.env.PORT || 8080 //get the por from dot env
const mongoURL = process.env.URL //get the mongo url from dot env

//body parser part
app.use(express.json({limit: '500mb'}));
app.use(express.urlencoded({limit: '500mb', extended: true}));
app.use(cors()) //to get the data from a https request just valid it
app.use(express.static('public')) //make public folder a static folder


//connected to the server
app.listen(PORT, () => {console.log(`Server is running on ${PORT}`);})

//connect to the data base
mongoose.connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(() => {
    console.log(`Server is connected to the database`);
})
.catch(err => {
    console.log(err);
})

app.get('/', (req, res) => {
    res.send('Hello I am from root')
})

//others api route
app.use('/user', userRoute)

app.get('*', (req, res) => {
    res.status(404).send("<h1>404 Page not found</h1>")
})