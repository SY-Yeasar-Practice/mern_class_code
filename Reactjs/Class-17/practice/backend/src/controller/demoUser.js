//require part
const express = require('express')
const bcrypt = require('bcrypt')
const fs = require('fs')
const User = require('../model/demoUser')
const {singleImageUploader:uploader} = require("../utils/imageUploader")
const {multipleImageFileUploader:multipleUploader} = require("../utils/mulipleFileUploader")

//create a user
const createUserController = async (req, res) => {
    try{
        const {password, image, multipleImage} = req.body //get the data from body
        const requireExtension = ["jpg", "jpeg"]
        const uploadTheImage = uploader(image, requireExtension) //it will return a object that contain all status
        const multipleUpload = multipleUploader(multipleImage, requireExtension)
        const {extensionValidation:multipleExtensionValidation, exportData} = multipleUpload
        const {fileAddStatus, extensionValidation, fileUrl } = uploadTheImage
        if(extensionValidation){ //validation part of image check the extension
            //multiple image upload part
             if(multipleExtensionValidation == false){
                res.status(401).json({
                    message:"multiple file should be jpeg and jpg"
                })
            }else{
                const multiPleData = [] //to store the multiple data into database formate
                exportData.map(data => {
                    multiPleData.push(data.dataUrl) //store the data url here
                })
                const hashed = await bcrypt.hash(password, 10) //hash the password
                if(hashed) {
                    const createUser = new User({
                        ...req.body,
                        password: hashed,
                        image: fileUrl,
                        mulitpleImage: multiPleData
                    })
                    const saveData = await createUser.save()
                    if(saveData) {
                        res.status(201).json({
                            message: "User has created successfully",
                            saveData
                        })
                    }else{
                        res.status(400).json({
                            message: "User creation failed"
                        })
                    }
                }else{
                    res.json({
                        message: "Password hashed time problem"
                    })
                }
            }
        }else{
            res.status(401).json({
                message: `require extension must be jpg and jpeg`,
                extensionValidation: false
            })
        }
        
        
        
    }catch(err){
        res.json({
            err
        })
        console.log(err);
    }
}

module.exports = {
    createUserController
}