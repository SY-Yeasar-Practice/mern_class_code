const express = require('express')
const route = express.Router()
const {createUserController} = require('../controller/demoUser')

route.post('/create', createUserController)

//export part
module.exports = route