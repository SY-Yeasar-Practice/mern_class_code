import React, { Component } from 'react'

class CommonFunciton extends Component {
    constructor () {
        super()
        this.state = {
            count: 0
        }
    }
    counterHandler = () => {
        this.setState ({
            count: this.state.count + 1
        })
    }
    render() {
        console.log("I am from common function");
        return (
            <div>
                {this.props.render(this.counterHandler, this.state.count)}
            </div>
        )
    }
}

export default CommonFunciton