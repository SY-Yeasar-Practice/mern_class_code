import React, { Component } from 'react'

class CounterOne extends Component {
    render() {
        console.log("I am from counter one");
        return (
            <div>
                <h1>Count {this.props.count}</h1>
                <button onClick = {this.props.handler}>Click me</button>
            </div>
        )
    }
}

export default  CounterOne