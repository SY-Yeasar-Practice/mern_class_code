import React, { Component } from 'react'
import CommonFunciton from './CommonFunciton'
import CounterOne from './CounterOne'

class RenderParentComp extends Component {
    render() {
        return (
            <div>
                <CommonFunciton render = {(handler, countValue) => (<CounterOne handler = {handler} count = {countValue}/>)}/>
           </div>
        )
    }
}
export default  RenderParentComp