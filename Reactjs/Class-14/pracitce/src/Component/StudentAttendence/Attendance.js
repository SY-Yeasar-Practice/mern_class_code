import React, { Component } from 'react'
import {StudentConsumer} from '../ContextAPi/Context/StudentDataContext'

class Attendance extends Component {
    constructor (props) {
        super()
        this.state = {
            presentStudent : [],
            myData : props.data
        }
    }
    
    //changeHandler
    changeHandler = (e) => {
        if(e.target.checked == true){
            this.setState({
                presentStudent: [...this.state.presentStudent, {
                    studentId: e.target.value
                }]
            })
        }else if(e.target.checked == false){
            const findTheIdIndex = this.state.presentStudent.findIndex(data => data == e.target.value)
            this.state.presentStudent.splice(findTheIdIndex, 1)
        }
    }
    
    //submitHandler
    submitHandler = (e) => {
        e.preventDefault() 
        const mainData = this.props.data //get the main data
        mainData[0].personalInfo.name.FirstName = "hello"
        const res = mainData[0].personalInfo.name.FirstName 
        console.log(this.state.myData);
    }

    render() {
        return (
            <div>
                <h1 style  ={{textAlign : "center"}}>Take the Attendance on {new Date().toLocaleTimeString()}</h1>
                <StudentConsumer>
                    {
                       (data) => (
                            this.props.data.map(data => {
                                return (
                                    <React.Fragment key = {data._id}>
                                        <input type="checkbox" id="vehicle1" name="attendance" value={data.userId} onChange = {this.changeHandler}></input>
                                        <h1 style = {{display: "inline"}}>{data.personalInfo.name.FirstName} {data.personalInfo.name.LastName} </h1>
                                        <br />
                                    </React.Fragment>
                                )
                            })
                       )
                    }
                </StudentConsumer>
                <button onClick = {this.submitHandler}>Submit Attendance</button>
            </div>
        )
    }
}
export default  Attendance