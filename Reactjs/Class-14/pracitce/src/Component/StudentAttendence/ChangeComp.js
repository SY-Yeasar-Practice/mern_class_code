import React, { Component } from 'react'
import {StudentConsumer} from '../ContextAPi/Context/StudentDataContext'

class ChangeComp extends Component {
    render() {
        return (
            <>
                <StudentConsumer>
                    {
                        (data) =>(
                            this.props.data.map(data => (
                                    <div style = {{border: "1px solid red"}} key = {data._id}>
                                        <h1 >Name: {data.personalInfo.name.FirstName } {data.personalInfo.name.LastName }</h1>
                                        <h2>Total Class: {data.academicInfo.attendanceRecord.totalClass}</h2>
                                        <h2>Present: {data.academicInfo.attendanceRecord.present}</h2>
                                        <h2>Absent: {data.academicInfo.attendanceRecord.absent}</h2>
                                    </div>
                            ))
                        )
                    }
                </StudentConsumer>
            </>
        )
    }
}
export default  ChangeComp