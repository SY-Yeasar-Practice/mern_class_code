import React, { Component } from 'react'
import studentData from './Data'
import Attendance from './Attendance'
import ChangeComp from './ChangeComp'
import {StudentProvider} from '../ContextAPi/Context/StudentDataContext'

class StudentComp extends Component {
    constructor() {
        super()
        this.state = {
            studentData: studentData,
        }
        this.attendance =  React.createRef()
    }
    render() { 
        console.log(this.attendance);
        return (
            <div>
                <StudentProvider value = {this.state.studentData}>
                    <Attendance data = {this.state.studentData} ref = {this.attendance} />
                    <ChangeComp data = {this.state.studentData}/>
                </StudentProvider>
            </div>
        )
    }
}

export default StudentComp
