const studentData = [
    {
        _id : "60b3a828c95c981f40f7e75b",
        personalInfo : {
            name: {
                FirstName : "Mr",
                LastName : "Hasan"
            },
            contact : {
                permanentAddress : "Nimnagar Balubari Dinajpur",
                currentAddress : "Nimnagar Balubari Dinajpur",
                mobileNo : "01701557770"
            },
            profilePic : "1622386727932_femaleStudent.png",
            FatherName : "Shah Rezaur Rahman",
            MotherName : "Samina Momtaz",
            email : "nihan123@gmail.com",
            sex : "female"
        },
        academicInfo : {
            attendanceRecord : {
                totalClass : 0,
                present : 0,
                absent : 0,
                record : []
            },
            isActive : true,
            isDeleted : false,
            syllabus : "",
            class : "One",
            examDetails : []
        },
        recoveryToken : "",
        userId : "011122008",
        userType : "student",
        password : "$2b$10$3GKOvGOjDZUmriAXnbTBaOTNojbNwECHa8gqQxPfH3HaJKgBQcNtG",
    },
    
    {
        _id : "60b3a884c95c981f40f7e75e",
        personalInfo : {
            name : {
                FirstName : "Mr",
                LastName : "Rohim"
            },
            contact : {
                permanentAddress : "Nimnagar Balubari Dinajpur",
                currentAddress : "Nimnagar Balubari Dinajpur",
                mobileNo : "01701557770"
            },
            profilePic : "1622386820235_maleStudent.jpg",
            FatherName : "Shah Rezaur Rahman",
            MotherName : "Samina Momtaz",
            email : "nihan1523@gmail.com",
            sex : "female"
        },
        academicInfo : {
            attendanceRecord : {
                totalClass : 0,
                present : 0,
                absent : 0,
                record: []
            },
            isActive : true,
            isDeleted : false,
            syllabus : "",
            class : "One",
            examDetails : []
        },
        recoveryToken : "",
        userId : "011132008",
        userType : "student",
        password : "$2b$10$HWK633XlCVUPcM4gjWY/d.qkyaQxkfBBJgk.uRSsmFg1GxX5DaI1.",
    },
    {
        _id : "60b3a828c95c181f40f7e75b",
        personalInfo : {
            name: {
                FirstName : "Mr",
                LastName : "Rahman"
            },
            contact : {
                permanentAddress : "Nimnagar Balubari Dinajpur",
                currentAddress : "Nimnagar Balubari Dinajpur",
                mobileNo : "01701557770"
            },
            profilePic : "1622386727932_femaleStudent.png",
            FatherName : "Shah Rezaur Rahman",
            MotherName : "Samina Momtaz",
            email : "nihan123@gmail.com",
            sex : "female"
        },
        academicInfo : {
            attendanceRecord : {
                totalClass : 0,
                present : 0,
                absent : 0,
                record : []
            },
            isActive : true,
            isDeleted : false,
            syllabus : "",
            class : "One",
            examDetails : []
        },
        recoveryToken : "",
        userId : "011122058",
        userType : "student",
        password : "$2b$10$3GKOvGOjDZUmriAXnbTBaOTNojbNwECHa8gqQxPfH3HaJKgBQcNtG",
    }
]

export default studentData