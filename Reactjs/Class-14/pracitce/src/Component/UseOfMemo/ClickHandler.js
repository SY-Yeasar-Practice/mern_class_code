import React, { Component, PureComponent } from 'react'
import ClickHandlerMemo from './ClickHandlerMemo'
import { AgeConsumer } from '../ContextAPi/Context/AgeContext'

class ClickHandler extends Component {
    constructor () {
        super()
        this.state = {
            count: 0
        }
    }
    clickHandler = () => {
        this.setState ({
            // count: this.state.count + 1
            count: 15
        })
    }
    render() {
        return (
            <div>
                <h1>Count {this.state.count}</h1>
                <button onClick = {this.clickHandler}>Click me from ClickHandler</button>

                <ClickHandlerMemo handler = {this.clickHandler} count = {this.state.count}/>
                <AgeConsumer>
                    {
                        (age) => (
                            <h1>I am {age} years old</h1>
                        )
                    }
                </AgeConsumer>
            </div>
        )
    }
}

// export default  React.memo(ClickHandler)
export default ClickHandler
