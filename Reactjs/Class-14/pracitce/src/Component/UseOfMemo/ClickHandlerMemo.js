import React from 'react'

function ClickHandlerMemo({handler, count}) {
    console.log(`Hello I am from CLick Handler Memo Comp`);
    return (
        <div>
            <h1>Hello I am from CLick Handler Memo Comp</h1>
            <h2>Count: {count}</h2>
            <button onClick = {handler}>CLick me from Click Handler Memo</button>
        </div>
    )
}

export default React.memo(ClickHandlerMemo)
// export default ClickHandlerMemo