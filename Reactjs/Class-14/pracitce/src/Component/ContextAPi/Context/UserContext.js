import React from 'react'

//create the context
const UserContext = React.createContext()

//create the provider
const UserProvider = UserContext.Provider

//create the consumer
const UserConsumer = UserContext.Consumer

export {
    UserConsumer,
    UserProvider
}