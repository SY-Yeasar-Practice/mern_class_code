import React, {createContext} from 'react'

const StudentDataContext = createContext()
const StudentProvider = StudentDataContext.Provider
const StudentConsumer = StudentDataContext.Consumer

export {
    StudentConsumer,
    StudentProvider
}