import React from 'react'

const AgeContext = React.createContext() //create the context
const AgeProvider = AgeContext.Provider //create the provider
const AgeConsumer = AgeContext.Consumer //create the Consumer

export {
    AgeProvider,
    AgeConsumer
}