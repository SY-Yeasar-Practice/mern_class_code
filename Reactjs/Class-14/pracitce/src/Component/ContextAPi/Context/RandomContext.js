import { createContext } from "react"

const RandomContext = createContext()
const RandomProvider = RandomContext.Provider
const RandomConsumer = RandomContext.Consumer

export {
    RandomProvider,
    RandomConsumer
}