import React, { Component } from 'react'
import { UserConsumer } from './Context/UserContext'
import { AgeConsumer } from './Context/AgeContext'
import { RandomConsumer } from './Context/RandomContext'

class CompThree extends Component {
    render() {
        return (
            <div>
                <UserConsumer>
                    {
                        (name) =>(
                            <RandomConsumer>
                                {
                                    (isLoggedin) => (
                                        <>
                                            <h1>I am {name}</h1>
                                            <h2>{isLoggedin ? `Wellcome ${name}` : 'Loading...'  }</h2>
                                        </>
                                    )
                                }
                            </RandomConsumer>
                            
                        )
                    }
                </UserConsumer>

                <AgeConsumer>
                    {
                        (age) => (
                            <h1>I am from Comp three I am {age} years old</h1>
                        )
                    }
                </AgeConsumer>
            </div>
        )
    }
}

export default CompThree