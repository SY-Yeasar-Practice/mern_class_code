import React, { Component } from 'react'
import CompTwo from './CompTwo'
import { UserConsumer } from './Context/UserContext'
import { AgeConsumer } from './Context/AgeContext'
import { RandomProvider } from './Context/RandomContext'

class CompOne extends Component {
    constructor() {
        super()
        this.state = {
            isLoggedIn : true
        }
    }
    render() {
        return (
            <div>
                <UserConsumer>
                    {
                        (name) =>(
                            <AgeConsumer>
                                {
                                    (age) => (
                                        <>
                                            <RandomProvider value = {this.state.isLoggedIn}>
                                                <CompTwo/>
                                                <h1>I am {name} from comp One I am {age} years old</h1>
                                            </RandomProvider>
                                        </>
                                    )
                                }   
                            </AgeConsumer>
                        )
                    }
                </UserConsumer>
            </div>
        )
    }
}

export default CompOne