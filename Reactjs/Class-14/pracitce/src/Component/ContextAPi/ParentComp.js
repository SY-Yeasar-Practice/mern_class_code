import React, { Component } from 'react'
import CompOne from './CompOne'


class ParentComp extends Component {
    render() {
        return (
            <div>
               <CompOne />
            </div>
        )
    }
}

export default ParentComp