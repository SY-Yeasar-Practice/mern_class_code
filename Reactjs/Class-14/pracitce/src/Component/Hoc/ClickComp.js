import React from 'react'
import HocComp from './HocComp'

function ClickComp({handler, count}) {
    return (
        <div>
            <h1>Count {count}</h1>
            <button onClick = {handler}>Click me</button>
        </div>
    )
}

export default HocComp(ClickComp)