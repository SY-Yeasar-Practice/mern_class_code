import React, { Component } from 'react'

const HocComp = (InputComponent) => {
    class HocCompClass extends Component {
        constructor () {
            super()
            this.state = {
                count : 0
            }
        }
        countHandler =  () => {
            this.setState({
                count: this.state.count + 1
            })
        }
        render() {
            return (
                <div>
                    <InputComponent handler = {this.countHandler} {...this.props} count = {this.state.count}/>
                </div>
            )
        }
    }

    return HocCompClass
    
}

export default HocComp