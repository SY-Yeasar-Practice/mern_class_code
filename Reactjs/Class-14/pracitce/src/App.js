import logo from './logo.svg';
import './App.css';
import ParentComp from './Component/ContextAPi/ParentComp';
import React, {Component} from 'react';
import { UserProvider } from './Component/ContextAPi/Context/UserContext';
import { AgeProvider } from './Component/ContextAPi/Context/AgeContext';
import RenderParentComp from './Component/RenderProps/RenderParentComp';
import ClickComp from './Component/Hoc/ClickComp';
import ClickHandler from './Component/UseOfMemo/ClickHandler';
import CompOne from './Component/ContextAPi/CompOne';
import StudentComp from './Component/StudentAttendence/StudentComp';


class App extends Component {
  constructor () {
      super()
      this.state = {
          name: "Ridom",
          age: 25
      }
  }
  render() {
    return (
      <div>
        <AgeProvider value = {this.state.age}>
          <UserProvider value = {this.state.name}>
            {/* <ParentComp/>
            <RenderParentComp/>
            <ClickComp/>
            <ClickHandler/> */}
            {/* <CompOne/> */}
            <StudentComp/>
          </UserProvider>
        </AgeProvider>
      </div>
    )
  }
}


export default App;
