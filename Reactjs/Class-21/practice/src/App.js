import logo from './logo.svg';
import './App.css';
// import ClassParent from './Component/Parent/ClassParent';
// import Parent from './Component/Functional/Parent/Parent';
// import CompA from './Component/Functional/ComponentA/PracticeReducer';
import Parent from './Component/Functional/UseReducerWithContext/Parent';
import CompA from './Component/Functional/FetchAPIWithReducer/CompA';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle'
import LoginForm from './Component/LoggInSystem/LoginForm/LoginForm';
import LoginPage from './Component/LoggInSystem/Login/LoginPage';

function App() {
  return (
    <div className="container">
      {/* <ClassParent/>
      <Parent/> */}
      {/* <CompA/> */}
      {/* <Parent/> */}
      {/* <CompA/> */}
      {/* <LoginForm/> */}
      <LoginPage/>
    </div>
  );
}

export default App;
