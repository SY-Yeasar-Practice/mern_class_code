import React, { Component } from 'react'
import ClassChild from '../Child/ClassChild'
import ClassChildTwo from '../Child/ClassChildTwo'
import CounterRender from '../Render/CounterRender'
import ClassRenderCounter from '../Child/ClassRenderCounter'

class ClassParent extends Component {
    render() {
        return (
            <div>
               <div>
                    <h1>Counter using Higher order component</h1>
                    <ClassChild/>
                    <ClassChildTwo/>
               </div>

               <div>
                    <h1>Counter using Render Props</h1>
                    <CounterRender render = {(count, handler) => <ClassRenderCounter count = {count} handler = {handler}/>}/>
               </div>
            </div>
        )
    }
}

export default ClassParent