import React, { Component } from 'react'

class CounterRender extends Component {
        constructor(props) {
                super()
                this.state = {
                    count: 0
                }
            }

        countHandler = (instruction) => {
            switch (instruction) {
                case "increment": {
                    this.setState({count: this.state.count + 1})
                    break
                }
                case "decrement": {
                    this.state.count > 0 && this.setState({count: this.state.count - 1})
                    break
                }
                case "reset" : {
                    this.setState({count: 0})
                    break
                }
                default: {
                    this.setState({count: 0})
                    break
                }
            }
        }

        render() {
            const {count} = this.state
            return (
                <div>
                    {
                        this.props.render(count , this.countHandler)
                    }
                </div>
            )
        }
}

export default CounterRender