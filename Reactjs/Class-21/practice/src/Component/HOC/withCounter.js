import React, { Component } from 'react'

const withCounter = (Child) => {
    class HocClass extends Component {
        constructor(props) {
            super()
            this.state = {
                count: 0
            }
        }

        countHandler = (instruction) => {
            switch (instruction) {
                case "increment": {
                    this.setState({count: this.state.count + 1})
                    break
                }
                case "decrement": {
                    this.state.count > 0 && this.setState({count: this.state.count - 1})
                    break
                }
                case "reset" : {
                    this.setState({count: 0})
                    break
                }
                default: {
                    this.setState({count: 0})
                    break
                }
            }
        }

        render() {
            const {count} = this.state //get the state value here
            return (
                <React.Fragment>
                    <Child {...this.props} count = {count} handler = {this.countHandler} />
                </React.Fragment>
            )
        }

    }
    return HocClass
}

export default withCounter


