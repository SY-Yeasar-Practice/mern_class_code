import React, {useContext} from 'react'
import { LoggedInUser } from '../Context/LoggedInUserContext'
import MyDetailsStyle from './MyDetails.module.css'

const MyDetails = () => {
    const user = useContext(LoggedInUser) //get the user data here
    const {firstName, lastName, profileImage} = user.newProfileData.data.isValidUser.personalInfo //get the personal data 
    // const {} = 
    return (
        <div>
            <h1>I am from My Details Comp</h1>
            <h2>My name is {firstName} {lastName}</h2>
            <h2>My profile image is: </h2>
            <img src={profileImage} alt="" className  ={`${MyDetailsStyle.myImage}`} />
        </div>
    )
}

export default MyDetails
