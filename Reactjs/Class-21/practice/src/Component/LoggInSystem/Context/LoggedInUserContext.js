import React, {useContext} from 'react'

const LoggedInUser = React.createContext()
const LoggedInUserProvider = LoggedInUser.Provider

export {
    LoggedInUser,
    LoggedInUserProvider,
}