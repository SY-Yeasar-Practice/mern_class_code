import React, {useReducer} from 'react'
import SinglePdfFileUpload from '../utils/SinglePdfFileUpload'
import { Worker } from '@react-pdf-viewer/core';
import { Viewer } from '@react-pdf-viewer/core';
// Plugins
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout'; // install this library
// Import the styles
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';

const fileInitialStatus = {
    isUpload: false,
    isError: false,
    file: '',
    newFile: ""
}
const pdfFileReducer = (state, action) => {
    switch (action.type) {
        case "Success" : {
            return {
                ...state,
                isUpload: true,
                isError: false
            }
        }
        case "Failed" : {
            return {
                ...state,
                isUpload: false,
                isError: true
            }
        }
        case "Add": {
            return {
                ...state,
                file: action.file
            }
        }
        case "Upload": {
            return {
                ...state,
                newFile: action.file
            }
        }
    }
}
const ShowPdf = () => {
    const [newPdfFileStatus, setPdfFileStatus] = useReducer(pdfFileReducer, fileInitialStatus)

    const pdfFileHandler = (data) => {
        const myData = data //get the data
        if(data) {
            const reader = new FileReader() 
            reader.onloadend = () => {
                setPdfFileStatus({
                    type: "Add",
                    file: reader.result
                })
            }
            reader.readAsDataURL(myData)
        }
    }
    // Create new plugin instance
    const defaultLayoutPluginInstance = defaultLayoutPlugin();
    // pdfUploadHandler
    const pdfUploadHandler = async(e) => {
        try{
            e.preventDefault() 
            const {file} = newPdfFileStatus //get the upload file 
            setPdfFileStatus({
                type: "Upload",
                file
            })

        }catch(err){
            setPdfFileStatus({
                type: "Failed"
            })
        }
    }
    const {newFile} = newPdfFileStatus
    return (
        <div className="row">
            <div className="col-12 col-lg-5">
               <div>
                    <SinglePdfFileUpload fileData = {(data) => pdfFileHandler(data)}/>
               </div>
                <div>
                    <button 
                    className = {`btn btn-danger`}
                    onClick = {(e) => pdfUploadHandler(e)}>Upload</button>
                </div>
            </div>
            <div className="col-12 col-lg-7" style = {{overflow: "scroll", height: "400px"}} >
                {/* pdf view preview */}
                <div>
                    {
                        !!newFile &&
                        <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js" >
                            <Viewer
                            fileUrl={newFile} 
                            plugins = {[
                                defaultLayoutPluginInstance
                            ]}
                            >
                            </Viewer>
                        </Worker>
                    }
                    
                </div>
            </div>
            
        </div>
    )
}

export default ShowPdf
