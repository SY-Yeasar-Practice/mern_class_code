import React, {useReducer, useEffect, useState} from 'react'
import loginFormStyle from './LoginForm.module.css'

const loginFormData = {
    email: '',
    password: '',
}
const loginFormChangeHandler = (state, action) => {
    switch (action.type) {
        case 'email': {
            return {
                ...state,
                email: action.value
            }
        }
        case 'password' : {
            return {
                ...state,
                password: action.value
            }
        }
        default: {
            return state
        }
    }
}

const LoginForm = ({loginData, error}) => {
    const [newLoginFormData, setLoginFormData] = useReducer(loginFormChangeHandler, loginFormData)
    const loggedIntHandler = (e) => {
        e.preventDefault()
        loginData(newLoginFormData)
    }
    console.log({errorOfLoginForm: error});
    return (
       <React.Fragment>
                <h1>Login Form</h1>
                <form>
                    <div className="mb-3">
                        <label for="exampleInputEmail1" 
                        className="form-label">Email address</label>
                        <input type="email" 
                        className="form-control" 
                        id="exampleInputEmail1" 
                        aria-describedby="emailHelp"
                        onChange = {(e) => setLoginFormData({type: e.target.name, value: e.target.value})}
                        name = "email"
                        value = {`${newLoginFormData.email}`}/>
                    </div>

                    <div className="mb-3">
                        <label for="exampleInputPassword1" 
                        className="form-label">Password</label>
                        <input 
                        type="password" 
                        className="form-control" 
                        id="exampleInputPassword1"
                        onChange = {(e) => setLoginFormData({type: e.target.name, value: e.target.value}) }
                        name = "password"
                        value = {`${newLoginFormData.password}`}/>
                    </div>
                    <button type="submit" 
                    className="btn btn-primary"
                    onClick = {(e) => loggedIntHandler(e)}>Login</button>
                    {
                        error ?
                        <p className = {`${loginFormStyle.loginTimeError}`}>Login Failed</p>
                        :
                        ""
                    }
                </form>
       </React.Fragment>
    )
}

export default LoginForm
