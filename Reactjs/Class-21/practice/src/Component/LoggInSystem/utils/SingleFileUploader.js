import React, {useReducer} from 'react'
import defaultImage from '../assert/default.png'
import UploaderStyle from './SIngFileUploader.module.css'

const fileUploadData = {
    file: '',
    image: defaultImage
}

const uploadStateChangeHandler = (state, action) => {
    switch (action.type) {
        case 'File': {
            return {
                ...state,
                file: action.file
            }
        }
        case 'image': {
            return {
                ...state,
                image: action.img
            }
        }
        case 'Success': {
            return {
                file: action.file,
                image: action.img
            }
        }
        case 'Failed': {
            return {
                file: '',
                image: defaultImage
            }
        }
        case "removePicture": {
            return {
                ...state,
                image: defaultImage
            }
        }
    }
}
const SingleFileUploader = ({data}) => {
    const [newFileUploadData, setFileUploadData] = useReducer(uploadStateChangeHandler, fileUploadData)

    // uploadHandler
    const uploadHandler = (e) => {
        e.preventDefault() 
        const myFile = e.target.files[0] //get the file 
        if(myFile) {
            const reader = new FileReader() 
            reader.onloadend = () => {
                setFileUploadData({
                    type: "Success",
                    file: myFile,
                    img: reader.result
                })
            }
            reader.readAsDataURL(myFile) //get the base 64 data
            data(myFile)
        }else{
            setFileUploadData({
                type: "Failed"
            })
        }
    }
    const imageRemoveHandler = (e) => {
        e.preventDefault()
        setFileUploadData({
            type: "removePicture"
        })

    }
    // console.log(newFileUploadData);
    return (
        <div className = {`mb-2`}>
            <div className="mb-3 ">
                <label for="formFile" className="form-label">Profile Image</label>
                <input 
                className="form-control" 
                type="file" 
                id="formFile"
                onChange = {(e) => uploadHandler(e)}/>
            </div>
            {/* preview image wrapper  */}
           <div className= {`${UploaderStyle.proPicWrap}`} >
                <img src= {newFileUploadData.image} alt="" className = {`${UploaderStyle.proPic}`} />
                <i className= {`${UploaderStyle.crossSign} fas fa-times`} onClick = {(e) => imageRemoveHandler(e)}></i>
           </div>
        </div>
    )
}

export default SingleFileUploader
