import React, {useReducer, useState} from 'react'
import defaultPdf from '../assert/default.pdf'
import { Worker } from '@react-pdf-viewer/core';
import { Viewer } from '@react-pdf-viewer/core';
// Plugins
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout'; // install this library
// Import the styles
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';


const SinglePdfFileUpload = ({fileData}) => {
    const [myFile, setMyfile] = useState("")
      // Create new plugin instance
    const defaultLayoutPluginInstance = defaultLayoutPlugin();
    // onchangeFileHandler
    const onchangeFileHandler = (e) => {
        const file = e.target.files[0]
        // console.log(file);
        if(file) {
            const reader = new FileReader()
            reader.onloadend = () => {
                setMyfile(reader.result)
            }
            reader.readAsDataURL(file)
            fileData(file)
        }else {
            setMyfile(null)
        }
    }
    return (
        <div>
            <div className="mb-3">
                <div className = {`mb-3`}>
                    <label for="formFile" className="form-label">PDF file</label>
                    <input className="form-control" 
                    type="file" 
                    id="formFile"
                    onChange = {(e) => onchangeFileHandler(e)}/>
                </div>

                {/* preview Wrapper */}
                <div className = "row">
                    <div className = "col-6 ">
                        <div style={{border: '1px solid rgba(0, 0, 0, 0.3)', height: '250px',       width: '100%'}} className="d-flex justify-content-center align-items-center " >
                            {
                                !!myFile && 
                                <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js" >
                                    <Viewer 
                                    fileUrl= {myFile} 
                                    />;
                                </Worker>
                            }
                            {
                                !myFile&&
                                <>No pdf file selected</>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SinglePdfFileUpload
