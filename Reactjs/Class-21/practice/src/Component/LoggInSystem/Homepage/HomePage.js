import React, {useEffect, useReducer} from 'react'
import axios from 'axios'
import LoginPage from '../Login/LoginPage'
import { LoggedInUserProvider } from '../Context/LoggedInUserContext'
import ProfilePage from '../ProfilePage/ProfilePage'
import MyDetails from '../MyDetails/MyDetails'
import ShowPdf from '../ShowPdf/ShowPdf'

const profileData = {
    data: '',
    error: false,
    isLoggedOut: false,
    isLoading: true
} //set the default state value
const profileDataSetHandler = (state, action) => {
    switch (action.type) {
        case 'Success' : {
            return {
                ...state,
                data: action.value,
                error: false,
                isLoggedOut: false,
                isLoading: false
            }
        }
        case 'Error' : {
            return {
                ...state,
                data: '',
                error: true,
                isLoggedOut: false,
                isLoading: true
            }
        }
        case 'LoggedOut' : {
            return {
                ...state,
                isLoggedOut: true
            }
        }
        default : {
            return  state
        }
    }
} //create a reducer function for change the state
const HomePage = () => {
    const [newProfileData, setProfileData ] = useReducer(profileDataSetHandler, profileData) //get a new state with data

    useEffect(async () => {
        try {
            const token = localStorage.getItem('auth-token') //get the token
            const option = {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "Authorization": token
                }
            } //set the header
            const getProfileData = await axios.get("http://localhost:3030/user/show/profile", option)
            const {status, data} = getProfileData
            if(status == 200 && data) { //if find the user profile data then set the state value
                setProfileData({
                    type: 'Success',
                    value: data
                })
            }else { //if there have a error during the fetching and failed to get the user profile data
                setProfileData({
                    type: 'Error'
                })
            }
        }catch (e) {
            setProfileData({
                type: 'Error'
            })
        }
    }, [])  //fetch the data from a api and get the user profile of logged in user
    
    
    const loggedOutHandler = async(e) => {
        e.preventDefault();
        localStorage.removeItem('auth-token');
        setProfileData({type: "LoggedOut"})
    } //logged Out handlerHandler

    return (
        <React.Fragment>
            {
                newProfileData.isLoggedOut 
                ?
                <LoginPage/>
                :
                <>
                    {
                        newProfileData.isLoading 
                        ?
                        <h1>...Loading</h1>
                        :
                        <LoggedInUserProvider value = {{newProfileData}}>
                           <div className = {`row`}>
                                <div className = {`col-12 col-md-6`}>
                                    <ProfilePage/>
                                </div>

                                <div className = {`col-12 col-md-6`} >
                                    <MyDetails/>
                                </div>
                                <div className = {`col-12`} >
                                    <ShowPdf/>
                                </div>
                           </div>
                        </LoggedInUserProvider>
                    }
                    <button className = {`mt-3`} onClick = {(e) => loggedOutHandler(e)}>Logged Out</button>
                </>
            }
        </React.Fragment>
    )
}

export default HomePage
