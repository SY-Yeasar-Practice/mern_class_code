import React , {useEffect, useContext, useState} from 'react'
import { LoggedInUser } from '../Context/LoggedInUserContext'
import ProfilePageStyle from './ProfilePage.module.css'
import SingleFileUploader from '../utils/SingleFileUploader'
import axios from 'axios'

const ProfilePage = () => {
    const userData = useContext(LoggedInUser) //get the data of logged in user 
    const {personalInfo, userId} = userData.newProfileData.data.isValidUser //get the data from context api of the user 
    const {firstName, lastName, profileImage} = personalInfo
    const [imageFile, setImageFile ] = useState("")
    const [uploadStatus, setUploadStatus ] = useState(false)

    // uploadHandler
    const uploadHandler = async(e) => {
        try{
            // e.preventDefault()
            const token = localStorage.getItem('auth-token')
            const option = {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "Authorization": token
                }
            } //set the header
            const sentFile = {
                profileImage: imageFile
            } //get the image file 
            console.log(sentFile)
            const sentData = await axios.post("http://localhost:3030/user/update/profile", sentFile, option)
            const {data, status} = sentData //get the responser from api
            const {updateStatus} = data
            if(updateStatus && status == 202) {
                setUploadStatus(true)
                window.location.reload()
            }else if(status == 400 || status == 402 || !updateStatus){
                setUploadStatus(false)
            }
        }catch(err) {
            setUploadStatus(false)
        }
    }

    // imageFileHandler
    const imageFileHandler = async (data) => {
        try{
            const imageData = data
            if(imageData) {
                const reader = new FileReader()
                reader.onloadend = () => {
                    const data = {
                        base64: reader.result,
                        size: imageData.size
                    }
                    setImageFile(data)
                }
                reader.readAsDataURL(imageData)
            }
            
        }catch(err){
            console.log("Error in Image")
        }
    }
    return (
        <div>
            <h1>Hello I am {firstName} {lastName}</h1>
            <img src={profileImage} alt="Profile Image" className = {`${ProfilePageStyle
            .profileImage}`}/>
            
            <div>
                <SingleFileUploader data = {imageFileHandler}/>
                <button className="btn btn-primary" onClick = {(e) => uploadHandler(e)}>Upload</button>
            </div>
        </div>
    )
}

export default ProfilePage
