import React, {useState, useReducer, useEffect} from 'react'
import LoginForm from '../LoginForm/LoginForm'
import axios from 'axios'
import HomePage from '../Homepage/HomePage'

//set the login info result initial state
const loginInfo = {
    isLoggedIn: false,
    data: '',
    error: false
}

//create the login change handler reducer function
const loginChangeHandler = (state, action) => {
    switch (action.type) {
        case 'Success' : {
            return {
                ...state,
                isLoggedIn: true,
                data: action.data,
                error: false
            }
        }
        case 'Failed' : {
            return {
                ...state,
                isLoggedIn: false,
                data: '',
                error: true
            }
        }
    }
}

const LoginPage = () => {
    // const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [newLoginInfo, setLoginInfo ] = useReducer(loginChangeHandler, loginInfo) //get the new login info
    const loginHandler = async (data) => {
        try {
            const myLoginFormInputData = data //store the login form input data here
            const getData = await axios.post("http://localhost:3030/user/login", myLoginFormInputData)
            const {data:dataOfAPI, status} = getData //get the data from api
            const {token} = dataOfAPI //get the toke from api
            if(token && status == 202) {
                localStorage.setItem('auth-token', token) //set the token into local storage
                setLoginInfo({
                    type: 'Success',
                    data: ''
                })
            }else {
                setLoginInfo({
                    type: "Failed",
                    data : ''
                })
            }
        }catch (err) {
            setLoginInfo({
                type: "Failed",
                data : ''
            })
        }
    }
    //controlled the logged in seasson and keep login if the user is not loggedOut
    useEffect(() => {
        const token = localStorage.getItem('auth-token') //get the token from 
        if(token) {
            setLoginInfo({type: "Success"})
        }else {}
    }, [])
    
    return (
        <React.Fragment>
             {
                newLoginInfo.isLoggedIn  
                ?
                <HomePage/>
                :
                <LoginForm loginData = {loginHandler} error = {newLoginInfo.error}/>
             }
        </React.Fragment>
    )
}

export default LoginPage
