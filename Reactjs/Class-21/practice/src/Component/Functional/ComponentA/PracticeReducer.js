import React, {useReducer, useState} from 'react'

//make a counter with use State
// const PracticeReducer = () => {
//     const [count, setCount] = useState(0)
//     return (
//         <div>
//             <h1>Make a counter with useState() hooks </h1>
//             <h1>Count: {count}</h1>
//             <button onClick={() =>setCount(count + 1)} >Increase</button>
//             <button onClick={() =>setCount(count - 1 )} >Decrease</button>
//             <button onClick={() =>setCount(0)} >Reset</button>
//         </div>
//     )
// }

// export default PracticeReducer


//make a counter using useReducer()
// const initialState = 0
// const reducer = (state, action) => {
//     switch (action)  {
//         case 'increment': {
//             return state + 1
//         }
//         case 'decrement': {
//             return state - 1
//         }
//         case 'reset' : {
//             return initialState
//         }
//         default: {
//             return state
//         }
//     }
// }

// const PracticeReducer = () => {
//     const [newState, dispatch] = useReducer(reducer, initialState)
//     return (
//         <div>
//             <h1>Make a counter with useReducer() hooks </h1>
//             <h1>Count: {newState}</h1>
//             <button onClick = {(e) => dispatch('increment')} >Increase</button>
//             <button onClick = {(e) => dispatch('decrement')} >Decrease</button>
//             <button onClick = {(e) => dispatch('reset')} >Reset</button>
//         </div>
//     )
// }

// export default PracticeReducer


//make multiple counter with useState() hooks

// const PracticeReducer = () => {
//     const [count, setCount] = useState({
//         counterOne: 0,
//         counterTwo: 10
//     })
//     console.log(count)
//     return (
//         <div>
//             <h1>Make multiple counter with useState() hooks </h1>

//             <h1>Counter One: {count.counterOne}</h1>
//             <button onClick={() =>setCount({...count, counterOne: count.counterOne + 1})} >Increase</button>
//             <button onClick={() =>setCount({...count, counterOne: count.counterOne + 1})} >Decrease</button>
//             <button onClick={() =>setCount({...count, counterOne: 0})} >Reset</button>


//             <h1>Counter Two: {count.counterTwo}</h1>
//             <button onClick={() =>setCount({...count, counterTwo: count.counterTwo + 10})} >Increase</button>
//             <button onClick={() =>setCount({...count, counterTwo: count.counterTwo - 10 })} >Decrease</button>
//             <button onClick={() =>setCount({...count, counterTwo: 0})} >Reset</button>
//         </div>
//     )
// }

// export default PracticeReducer


//make multiple counter with useReducer() hooks 
// const initialState = {
//     counterOne: 0,
//     counterTwo: 10
// }
// const reducer = (state, action) => {
//     switch (action.type) {
//         case 'increment' : {
//             return{
//                 ...state,
//                 counterOne: state.counterOne + action.value
//             }
//         }
//         case 'decrement' : {
//             return {
//                 ...state,
//                 counterOne: state.counterOne - action.value
//             }
//         }
//         case 'reset' : {
//             return {
//                 ...state,
//                 counterTwo: 0
//             }
//         }
//         case 'incrementTwo' : {
//             return{
//                 ...state,
//                 counterTwo: state.counterTwo + action.value
//             }
//         }
//         case 'decrementTwo' : {
//             return {
//                 ...state,
//                 counterTwo: state.counterTwo - action.value
//             }
//         }
//         case 'resetTwo' : {
//             return {
//                 ...state,
//                 counterTwo: 0
//             }
//         }
//     }
// }
// const PracticeReducer = () => {
//     const [count, setCount] = useReducer(reducer, initialState)
//     return (
//         <div>
//             {/* counter one */}
//             <h1>Make a counter with useReduce() hooks </h1>
//             <h1>Counter One: {count.counterOne}</h1>
//             <button onClick={(e) =>setCount({type: 'increment', value: 5})} >Increase</button>
//             <button onClick={(e) =>setCount({type: "decrement", value: 5})} >Decrease</button>
//             <button onClick={(e) =>setCount({type: "reset"})} >Reset</button>

//             {/* counter two */}
//             <h1>Counter Two: {count.counterTwo}</h1>
//             <button onClick={(e) =>setCount({type: 'incrementTwo', value: 2})} >Increase</button>
//             <button onClick={(e) =>setCount({type: "decrementTwo", value: 2})} >Decrease</button>
//             <button onClick={(e) =>setCount({type: "resetTwo"})} >Reset</button>
//         </div>
//     )
// }

// export default PracticeReducer

// const initialStateOne = 0
// const initialStateTwo = 10
// const reducer = (state, action) => {
//     switch (action.type) {
//         case 'increment' : {
//             return state + action.value
//         }
//         case 'decrement' : {
//             return state - action.value
//         }
//         case 'reset' : {
//             return 0
//         }
//         default : {
//             return state
//         }
//     }
// }

//  const PracticeReducer = () => {
//         const [count, dispatch] = useReducer(reducer, initialStateOne)
//         const [countTwo, dispatchTwo] = useReducer(reducer, initialStateTwo)
//         return (
//             <div>
//                 <h1>Make a counter with useReducer() hooks </h1>
//                 <h1>Counter One: {count}</h1>
//                 <button onClick = {(e) => dispatch({type:'increment', value: 1})} >Increase</button>
//                 <button onClick = {(e) => dispatch({type:'decrement', value: 1})} >Decrease</button>
//                 <button onClick = {(e) =>dispatch({type:'reset'})} >Reset</button>

//                 <h1>Counter One: {countTwo}</h1>
//                 <button onClick = {(e) => dispatchTwo({type:'increment', value: 10})} >Increase</button>
//                 <button onClick = {(e) => dispatchTwo({type:'decrement', value: 10})} >Decrease</button>
//                 <button onClick = {(e) =>dispatchTwo({type:'reset'})} >Reset</button>
//             </div>
//         )
// }

// export default PracticeReducer