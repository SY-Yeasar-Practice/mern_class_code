import React, {useReducer, useState, useContext, createContext} from 'react'
import { ChildOne } from './ChildOne'

export const DataContext = createContext()
const DataProvider = DataContext.Provider
const initialState = 0

const reducer = (state, action) => {
    switch (action.type) {
        case 'increment':{
            return state + action.value
        }
        case 'decrement': {
            return state  - action.value
        }
        case 'reset' : {
            return initialState
        }
        default : {
            return state
        }
    }
}

const Parent = () => {
    const [count, dispatch] = useReducer(reducer, initialState)
    const sendData = {
        data: count,
        setCount: dispatch
    }
    return (
        <div>
            <h1>Count: {count}</h1>
            <DataProvider value = {sendData}>
                <ChildOne/>
            </DataProvider>
        </div>
    )
}

export default Parent
