import React, {useContext} from 'react'
import { DataContext } from './Parent'

const ChildTwo = () => {
    const getData = useContext(DataContext)
    const {data, setCount} = getData
    return (
        <div>
            <h1>I am from Child Two</h1>
            <h1>Counter: {data}</h1>
            <button onClick={(e) => setCount({type: 'increment', value : 1})} >Increase</button>
            <button onClick={(e) => setCount({type: 'decrement', value : 1})} >Decrease</button>
            <button onClick={(e) => setCount({type: 'reset'})} >Reset</button>
        </div>
    )
}

export  default ChildTwo