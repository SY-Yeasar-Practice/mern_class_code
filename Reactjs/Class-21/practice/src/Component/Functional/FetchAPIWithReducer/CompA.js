import React, {useReducer, useEffect } from 'react'
import axios from 'axios'
import CompB from './CompB'

const Login = {
    error: '',
    data: '',
    isLoggedIn: false
}
const changeFunction = (data, action) => {
    // console.log(action);
    switch (action.type) {
        case 'success': {
            return {
                data: action.data,
                isLoggedIn: true,
                error: ''
            }
        }
        case 'error' : {
            return {
                data: {},
                isLoggedIn: false,
                error: action.error
            }
        }
        default : {
            return data
        }
    }
}
export const DataContext = React.createContext()
const DataProvider = DataContext.Provider

const CompA = () => {
    const [loggedInData, setData] = useReducer(changeFunction, Login)
    // console.log(loggedInData);
    useEffect( async () => {
        const getData = await axios.get('https://jsonplaceholder.typicode.com/posts')
        if(getData) {
            setData({
                type: 'success',
                data: getData.data
            })
        }else{
            setData({
                type: 'error',
                error: 'fetch time error'
            })
        }
    }, [])
    return (
        <div>
            {
                loggedInData.isLoggedIn 
                ?
                <div>
                    {/* {
                        loggedInData.data.map(data => {
                            return (
                               <div>
                                    <h1 key = {data.id}>{data.title}</h1>
                               </div>
                            )
                        })
                    } */}
                    <DataProvider value = {loggedInData}>
                        <CompB/>
                    </DataProvider>
                </div>
                :
                <h1>....Loading</h1>
            }
        </div>
    )
}

export default CompA
