import React, {useContext} from 'react'
import { DataContext } from './CompA'

const CompC = () => {
    const loggedInData = useContext(DataContext)
    return (
        <div>
            <h1>I am from Comp C</h1>
            {
                loggedInData.data.map(data => {
                    return(
                        <h1 key = {data.id}>{data.title}</h1>
                    )
                })
            }
        </div>
    )
}

export default CompC
