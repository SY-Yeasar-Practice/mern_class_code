import React from 'react'
import FilterCss from './Filter.module.css'

const Filter = ({data}) => {
    return(
        <div>
            <div className = {`text-center mt-2 mt-md-5`}>
                <h1>Catagory</h1>
            </div>
            <div  className = {`mt-md-5 text-center`}>
                 {
                    data.map((ele, ind) => {
                        return <div className = {`${FilterCss.distanceBetweenTwoFilterOption} `}>
                                    <p key = {ind} className = {`d-inline`}>{ele}</p>
                                    <input type="checkbox" aria-label="Checkbox for following text input" className = {`${FilterCss.checkButton}`} ></input>
                                </div>
                    })
                }
            </div>
        </div>
    )
}

export default Filter