
let data = [
    {
        title: "Hypocrite World",
        writter : "Mr X",
        price: "250",
        img: "hypocriteWorld.jpg"
    },
    {
        title: "Nora Rarrett",
        writter : "Mr X",
        price: "450",
        img: "noraRarrett.jpg"
    },
    {
        title: "The Inspiration Code",
        writter : "Mr X",
        price: "150",
        img: "theInspirationCode.jpg"
    },
    {
        title: "Nora Rarrett",
        writter : "My Book Cover",
        price: "220",
        img: "book-covers-big-2019101610.jpg"
    }
]

export default data