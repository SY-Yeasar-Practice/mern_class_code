import React from 'react'
import Book from '../book/Book'
import Filter from '../filter/Filter'
import Header from '../header/Header'
import FilterData from '../data/bookDataFilter'
import FilterCss from '../filter/Filter.module.css'
import HeaderStyle from '../header/Header.module.css'
import MainStyle from './Main.module.css'
import BookData from "../data/bookData"

const MainFile = () => {
    return (
        <div className = {`container-fluid ${MainStyle.mainBackground}`}>
            <div className = {`container`}>
                <div className = {`text-center  ${HeaderStyle.background} p-3`}>
                    <Header/>
                </div>
                <div className = {`container`}>
                    <div className = {`row`}>
                    <div className = {`col-12 col-md-5 ${FilterCss.mainBackGround}`} >
                        <Filter data = {FilterData}/>
                    </div>
                    <div className = {`col-12 col-md-7`}>
                        <Book BookData = {BookData}/>
                    </div>
                </div>
            </div>
            </div>
        </div>
    )
}

export default MainFile
