import React from "react"
import BookCss from "./Book.module.css"

const Book = ({BookData}) => {
    return(
        <div>  
            {
                BookData.map((data, ind) =>{
                    const sectionStyle = ind % 2 == 0 ? "oddSection" : "evenSection" //execute the css class according to the index number whether it is odd or even number
                    return(
                        <div key = {ind} className = {`row ${BookCss[sectionStyle]} p-4`} >
                            {/* image part */}
                            <div className = {`col-12 col-md-5 p-3`}>
                                <img  src= {`images/${data.img}`} alt={`${data.title} image`} className = {`img-fluid ${BookCss.bookImgSize}`} />
                            </div>
                            {/* data part */}
                            <div className = {`col-12 col-md-7 ${BookCss.bookTitle} text-center text-md-right`}>
                                <h4>Tittle: {data.title}</h4>
                                <h4>Tittle: {data.writter}</h4>
                                <h4>Tittle: {data.price} taka</h4>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default Book